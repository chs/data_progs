#! /usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

from .uri import EMAIL_CREDENTIALS


def readCredentials(fname):
    if Path(fname).is_file():
        with open(fname) as f:
            out = [l.strip() for l in f.readlines()]
        return out
    return ["nobody", "nopass"]


SENDER = "david.schaefer@ufz.de"
RECIPIENTS = [
    SENDER,
    # "corinna.rebmann@ufz.de",
    "robert.wiesen@ufz.de",
    "patrick.schmidt@ufz.de",
    "alexander.hinz@ufz.de",
]
CREDENTIALS = readCredentials(EMAIL_CREDENTIALS)
