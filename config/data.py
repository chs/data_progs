#! /usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import date

NODATA = -9999
MINDATE = date(1900, 1, 1)
MAXDATE = date(2100, 12, 31)
