#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
from pathlib import Path

ROOT = os.path.abspath("/mnt/ygruppen/chs-data")
FTPROOT = "files.ufz.de"
KEYFILE = "/home/chs/.ssh/id_rsa"

XLSFILE = os.path.join(ROOT, "CHS-measurements.xlsx")
CONFIGPATH = Path(__file__).absolute().parents[1] / "config"
SECRETSPATH = Path(__file__).absolute().parents[1] / "secrets"
CACHEPATH = Path(ROOT) / "cache"

STATIONSFILE = str(CONFIGPATH / "stations.csv")
DEVICESSFILE = str(CONFIGPATH / "devices.csv")

EMAIL_CREDENTIALS = str(SECRETSPATH / ".email_credentials")
DMP_CREDENTIALS = str(SECRETSPATH / ".dmp_credentials")

LOGFILE = str(Path(ROOT, "log", "daily.log"))
USAGEPATH = str(Path(ROOT, "site_usage"))

PLOTPATH = str(Path(ROOT, "Plots"))
