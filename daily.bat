REM mount Y:
net use Y: \\intranet.ufz.de\dfs

REM activate virtual environment
call c:\Users\schaefed\AppData\Local\Continuum\anaconda3\Scripts\activate data_progs_new

set ppath=C:\Users\schaefed\SourceCode\Python\data_progs
cd %ppath%

REM git checkout master
python -m transfer_level0_level1.do_data_download --ndays 1
python -m transfer_level0_level1.do_check_download --ndays 1 --station HH GB HD

rem python -m transfer_level0_level1.do_transfer_level0_level1 --ndays 180
python -m transfer_level0_level1.do_level1_flagging --start_date 2020-01-01
python -m plotting.do_plot_current_level1 --ndays 7

rem python -m transfer_level0_level1.do_transfer_level0_level1_soilnet --ndays 180
rem python -m transfer_level0_level1.do_transfer_level0_level1_ICOS --ndays 180
rem python -m transfer_level0_level1.do_level1_flagging_soilnet --ndays 180
rem python -m plotting.do_plot_current_level1_soilnet --ndays 15

rem is this --autopath thingy really necessary
python -m plotting.do_plot_current_level1_intersite --ndays 7 --autopath
python -m plotting.do_plot_current_level1_intersite --ndays 30 --autopath

rem fix the script and include again
rem python -m level1.do_write_meteo --station HH HD GB --ndays 1
python -m level1.do_concatenate_year --start_date 2020-01-01
rem python -m level1.do_concatenate_year_soilnet --ndays 60
rem python -m level1.do_aggregate_soilnet --ndays 60
rem python -m plotting.do_plot_level1_soilnet --ndays 60


python -m update_zip_archive.update_zip_archives_eddy_external_zipper Y:\Gruppen\chs-data\HohesHolz\Eddydata\ 1
python -m update_zip_archive.update_zip_archives_eddy_external_zipper Y:\Gruppen\chs-data\Wulferstedt\Eddydata\ 1
python -m update_zip_archive.update_zip_archives_eddy_external_zipper Y:\Gruppen\chs-data\Hordorf\Eddydata\ 1


python -m tools.do_check_usage
python -m tools.do_check_log
