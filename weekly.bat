net use Y: \\intranet.ufz.de\dfs

REM activate virtual environment
call c:\Users\schaefed\AppData\Local\Continuum\anaconda3\Scripts\activate data_progs_new

set ppath=C:\Users\schaefed\SourceCode\Python\data_progs
cd %ppath%

rem python -m transfer_level0_level1.do_transfer_level0_level1 --ndays 400
rem python -m transfer_level0_level1.do_level1_flagging --ndays 400
python -m level1.do_concatenate_year --doall

rem derived stuff
python -m level1.do_write_meteo --station HH GB HD --ndays 400
python -m level1.do_aggregate_logger --freq 30min --ndays 400
python -m level1.do_aggregate_logger --freq 1D --ndays 400
python -m level1.do_dendro_calculation --ndays 400 --station HH --logger BC1 BC2 BC3 T5
python -m tools.do_monthly_archives

python -m tools.do_check_log

