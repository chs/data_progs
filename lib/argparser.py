#! /usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from datetime import datetime, date, timedelta


def _parseDate(datestring):
    return datetime.strptime(datestring, "%Y-%m-%d").date()


class ChainParser(argparse.ArgumentParser):
    def addArgument(self, longname, **kwargs):
        option = longname.lstrip("-")
        if option not in self.noopts:
            kwargs["default"] = self.defaults.get(option)
            super().add_argument(longname, **kwargs)

    def __init__(self, description, defaults=None, noopts=None):
        super(ChainParser, self).__init__(description=description)

        noopts = noopts or ()
        defaults = defaults or {}
        self.noopts = set(noopts)
        self.defaults = {**{"doall": False, "debug": False}, **defaults}
        if self.noopts - self.defaults.keys():
            raise RuntimeError("default value needed for all noopt values")

        self.addArgument("--station", nargs="*", help="process given station only")

        self.addArgument("--device", nargs="*", help="process given logger only")

        self.addArgument(
            "--tag", nargs="*", help="process all devices with given data tag"
        )

        self.addArgument(
            "--ndays", type=int, help="Number of most recent days to process"
        )

        self.addArgument(
            "--doall",
            action="store_true",
            help="Process everything (this may take a while...)",
        )

        self.addArgument(
            "--start-date", type=_parseDate, help="Processing start date [yyyy-mm-dd]"
        )

        self.addArgument(
            "--end-date", type=_parseDate, help="Processing end date [yyyy-mm-dd]"
        )

        self.addArgument(
            "--debug",
            action="store_true",
            help=("run in debug mode (i.e. verbose output, no email notification"),
        )

        self.addArgument(
            "--fail",
            action="store_true",
            default=False,
            help=("exit immediatly on errors"),
        )

    def parseArgs(self):
        args = super(ChainParser, self).parse_args()
        if args.ndays:
            # option 'ndays weaker than explit 'start_date'/'end_date'
            if args.end_date is None:
                args.end_date = date.today()
            if args.start_date is None:
                args.start_date = args.end_date - timedelta(days=args.ndays)
        if args.doall:
            # option 'doall' stronger than the other time limiting options
            args.start_date = None
            args.end_date = None
        # add the noopts
        for key in self.noopts:
            setattr(args, key, self.defaults[key])
        return args


def parseArguments(description, defaults=None, noopts=None):
    parser = ChainParser(description, defaults, noopts)
    return parser.parseArgs()
