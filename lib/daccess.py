#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import string
import warnings
from datetime import datetime
from io import StringIO
from pathlib import Path

import numpy as np
import pandas as pd

from .tools import _tupelize
from config.data import NODATA
from config.uri import XLSFILE
from .pdutils import filterBy, setIndexLevel
from .tools import firstOfDay, firstOfYear, firstOfMonth


class ConfigFields(object):
    DEVICE = "logger"
    DBNAME = "headerout (DB)"
    VARNAME = "headerout"
    DATE = "Date Time"
    START_DATE = "date start"
    END_DATE = "date end"
    EDDYPRO = "Eddypro Label"
    STARTDATE = "date start"
    ENDDATE = "date end"
    UNIT = "units"
    HEADNAME = "headerout (final)"
    DEPTH = "depth/height (m)"
    MIN = "Min"
    MAX = "Max"
    FLAG1 = "max percentage missing data for aggregation resulting in flag 1"
    FLAG2 = "max percentage missing data for aggregation resulting in flag 2"
    AGGREGATION_TYPE = "AGGREGATION_TYPE"
    DEPENDENCIES = "derived from/influenced by var"
    STORAGE = "storage meta data variable name"


CONFIG_FIELDMAP = {
    "SoilNet Node": "logger",
    "logger/data acquisition": "logger",
    "RECORD [RN]": "record",
    "Record": "record",
    "Installation date + time": "date start",
    "neu am 13.4.22 CR: derived from/influenced by": "derived/influenced variables",
}


# this work needs to continue
class IndexFields(object):
    VARNAME = "varnames"
    TAG = "tags"
    HEADER = "headers"
    DATE = ConfigFields.DATE
    FLAG = "flag"
    DATA = "data"


Fields = ConfigFields


def readExcel(sheets):
    dfs = []
    for sheet in _tupelize(sheets):
        data = pd.read_excel(XLSFILE, sheet)
        data = data.rename(columns=CONFIG_FIELDMAP)
        dfs.append(data)

    if dfs:
        out = pd.concat(dfs, sort=True)
        grouper = out.groupby(by=ConfigFields.VARNAME)
        # remove the duplicates without a registered DBNAME (necessary e.g. HH-T3/Tpot01)
        out = grouper.apply(lambda df: df.sort_values([ConfigFields.DBNAME]).iloc[0])
        return out
    return pd.DataFrame()


def splitTable(df, index_level=None):
    # split given df into data and flags

    if isinstance(df.columns, pd.MultiIndex) and "tags" in df.columns.names:
        data = df.xs("data", level="tags", axis=1)
        flags = df.xs("flag", level="tags", axis=1)
    else:
        tmp = reindexTable(df)
        data = tmp.xs("data", level="tags", axis=1)
        flags = tmp.xs("flag", level="tags", axis=1)

    if index_level is not None:
        data.columns = data.columns.get_level_values(index_level)
        flags.columns = flags.columns.get_level_values(index_level)

    assert len(data.columns) == len(flags.columns)

    return data, flags


def _readTableFile(fname, sep, usecols, filename, dtype):
    try:
        df = pd.read_csv(
            fname,
            sep=sep,
            parse_dates=[0],
            index_col=0,
            usecols=usecols,
            na_values=NODATA,
        )
    except pd.errors.EmptyDataError:
        df = pd.DataFrame()
    if filename:
        df["filename"] = fname
    if dtype is not None:
        df = df.astype(dtype)
    df.columns.rename(IndexFields.HEADER, inplace=True)
    return df


def readTableFiles(fnames, filename=False, dtype=None, variables=None, sep=","):
    def usecols(col):
        for vname in _tupelize(variables):
            if ("date" in col.lower()) or (vname in col):
                return True

    usecols = usecols if variables is not None else None
    fnames = sorted(_tupelize(fnames))
    dfs = []
    for fname in fnames:
        df = _readTableFile(fname, sep, usecols, filename, dtype)
        if not df.empty:
            dfs.append(df)
    return dfs


def readTable(fnames, filename=False, variables=None):
    """
    Arguments:
    ----------
    fnames : String or sequence of strings

    Purpose:
    --------
    Read all given level1 files into a pandas DataFrame and clean the result:

    """
    dfs = readTableFiles(fnames, filename, variables=variables)

    if not dfs:
        return pd.DataFrame()

    tmp = []
    for df in dfs:
        # we may loose flag-columns, if multiple variables have the
        # same name and only different units. to prevent that, we
        # temporarily create unique columns names
        if df.empty:
            continue
        df = reindexTable(df)
        cols = df.columns.to_frame()
        unique = []
        for key in cols.index.get_level_values("varnames").unique():
            col = cols.loc[key]
            try:
                # let's use the header of data to make flag unique
                col.loc["flag", "varnames"] = col.loc["data", "headers"].iloc[0]
            except KeyError:
                pass
            unique.extend((col["varnames"] + "," + col["headers"]).tolist())

        df.columns = unique
        tmp.append(df)

    out = pd.concat(tmp, sort=False)
    out.columns = [c.split(",")[1] for c in out.columns]
    out.columns.name = IndexFields.HEADER
    return out.dropna(axis="index", how="all")


# NOTE: check all fill=True positions - should be done
def prepareTable(
    df,  # type: pd.DataFrame
    fillna=True,  # type: bool
    reindex=False,  # type: bool
    index_level=None,  # type: Optional[Union[str, int]]
    dtypes=(
        float,
        int,
    ),  # type: Sequence[Union[type, str, np.dtype], Union[type, str, np.dtype]]
    nbits=None,  # type: Optional[int]
    drop_record=True,  # type: bool
    drop_empty=False,
    drop_duplicated_timestamps=True,
    freq=None,  # type: Optional[str]
):
    if isinstance(df.columns, pd.MultiIndex):
        df.columns = df.columns.get_level_values(IndexFields.HEADER)

    df.columns.rename(IndexFields.HEADER, inplace=True)
    df.index.rename(IndexFields.DATE, inplace=True)

    if not df.empty:
        if drop_duplicated_timestamps:
            df = squeezeTimestamps(df)

        if freq:
            index = pd.date_range(df.index.min(), df.index.max(), freq=freq)
            # if we don't specify fill_value and fillna=False,
            # we might end up with both NODATA and np.nan
            # indicating missing data in the same data frame
            df = df.reindex(index, fill_value=NODATA)

    # remove duplicated and therefore renamed columns and write their values
    # to the appropiate position. That means that later files overwrite earlier
    out = df.filter(regex=r"(\]|f)$")
    for col in set(df.columns) - set(out.columns):
        right = df[col].replace(NODATA, np.nan).dropna()
        out.loc[right.index, col.split(".")[0]] = right

    if drop_record is True:
        out.drop(["Record", "RECORD [RN]"], axis=1, inplace=True, errors="ignore")

    if drop_empty is True:
        out = dropEmptyVariables(out)

    if reindex is True and len(out.columns):
        out = reindexTable(out, index_level=index_level)

    if not df.empty:
        out = setTypes(out, dtype=dtypes[0], ftype=dtypes[1], nbits=nbits)

    if fillna is True:
        out.fillna(NODATA, inplace=True)
    else:
        # if we don't want to fillna, set all NODATA values to np.nan
        out.replace(NODATA, np.nan, inplace=True)

    # we have special rounding needs
    out = out.round(4)
    precip = [
        c
        for c in out.columns
        if "precip" in c and not "ombro" in c and not c.endswith("_f")
    ]
    if precip:
        out[precip] = out[precip].round(1)

    return out


def columnKeys(col):
    if col.endswith("_f"):
        return col.replace("_f", ""), IndexFields.FLAG
    elif col.lower().startswith("record"):
        return col, "record"
    return col.split(" ")[0], IndexFields.DATA


def setColumns(df, level):
    out = reindexTable(df)
    return setIndexLevel(out, axis=1, level=level)


def reindexTable(df, index_level=None):
    """
    Set a triple columm index with:
    1. varname without unit of flag suffix
    2. ["data", "flag"] for easy access
    3. the original column names
    """

    out = df.copy(deep=False)

    headers = out.columns.get_level_values(IndexFields.HEADER)

    level0, level1 = zip(*map(columnKeys, headers))

    out.columns = pd.MultiIndex.from_tuples(
        zip(level0, level1, headers),
        names=[IndexFields.VARNAME, IndexFields.TAG, IndexFields.HEADER],
    )

    if index_level is not None:
        index_levels = _tupelize(index_level)
        indices = [out.columns.get_level_values(i) for i in index_levels]
        out.columns = pd.MultiIndex.from_arrays(indices)

    return out


def setTypes(
    df,  # type: pd.DataFrame
    dtype=None,  # type: Optional[Union[str, np.dtype, type]]
    ftype=None,  # type: Optional[Union[str, np.dtype, type]]
    nbits=None,  # type: Optional[int]
):  # type: (...) -> None
    # (DataFrame, data_dtype, flag_dtype, nbits) -> None

    def _asType(df, dtype):
        if np.issubdtype(dtype, np.integer):
            df = df.fillna(NODATA)
        return df.astype(dtype)

    # set defaults
    dtype = np.dtype(dtype or float)
    ftype = np.dtype(ftype or int)

    # nbits overwrites explicit dtype and ftype settings
    if nbits in (16, 32, 64):
        dtype = np.dtype("float{:}".format(nbits))
        ftype = np.dtype("int{:}".format(nbits))

    out = setColumns(df, "tags")
    out["data"] = _asType(out["data"], dtype)
    if "flag" in out.columns:
        out["flag"] = _asType(out["flag"], ftype)
    if "record" in out:
        out["record"] = _asType(out["record"], ftype)

    out.columns = df.columns
    return out


def filterVariables(data, regex):
    out = filterBy(
        reindexTable(data.copy(deep=False)), level=2, regex=regex, into_level=0
    )
    return out


def writeTable(fname, df, make_path=False, format="csv"):
    # type: (str, pd.DataFrame, bool) -> None
    # Purpose: write df to fname, optionally create missing directories

    if format not in ("csv", "parquet"):
        raise ValueError(f"unsupported file format '{format}'")

    if isinstance(df.columns, pd.MultiIndex):
        df.columns = df.columns.get_level_values(level="headers")

    if make_path:
        Path(fname).parent.mkdir(parents=True, exist_ok=True)

    fname = str(fname)
    out = setTypes(df.fillna(NODATA))

    # we have special rounding needs
    out = out.round(4)
    precip = [
        c
        for c in out.columns
        if "precip" in c and not "ombro" in c and not c.endswith("_f")
    ]
    if precip:
        out[precip] = out[precip].round(1)

    # to get rid of the name attribute
    out.columns = df.columns.tolist()
    out = out.fillna(NODATA)
    if format == "csv":
        # NOTE: converting the index to strings first, is waaaay faster
        out.index = out.index.strftime("%Y-%m-%d %H:%M:%S")
        out.to_csv(fname, sep=",", index_label="Date Time")
    else:
        out.to_parquet(fname)


def _toDatetime(dates, formats):
    for f in formats:
        try:
            return pd.to_datetime(dates, format=f)
        except ValueError:
            pass
    raise ValueError(f"time data does not match any of the given formats: '{formats}'")


def _readSoilnetL0File(fname):
    badchars = re.escape(re.sub(r"[_.:]", "", string.punctuation))
    data = []
    with open(fname, "r") as f:
        for line in f:
            if re.search(badchars, line):
                continue
            data.append(line)
    return "".join(data)


def readLoggerL0(fnames, fail=True):
    # TODO: rename function
    dfs = []
    for fname in fnames:
        try:
            df = pd.read_csv(
                fname, sep=",", engine="c", header=[1, 2, 3], na_values="NAN"
            )
        except pd.errors.ParserError:
            if fail:
                raise
            warnings.warn(f"failed to parse file: {fname}")
            continue
        df = df.sort_index(axis="columns")
        dfs.append(df)

    df = pd.concat(dfs)

    level0 = df.columns.get_level_values(0)
    level1 = df.columns.get_level_values(1).to_series()
    level1[level1.str.startswith("Unnamed")] = ""
    df.columns = pd.Index(level0 + " [" + level1 + "]", name=IndexFields.HEADER)
    df.index = pd.Index(pd.to_datetime(df.pop("TIMESTAMP [TS]")), name=IndexFields.DATE)
    # TODO: remove when the transition to SaQC is completed
    df = df.groupby(df.index).last()
    return df


def _readSoilnetL0Files(fnames, device_key):
    header = [
        "date",
        "time",
        "box",
        "Voltage",
        "Moist1",
        "Temp1",
        "Moist2",
        "Temp2",
        "Moist3",
        "Temp3",
        "Moist4",
        "Temp4",
        "Moist5",
        "Temp5",
        "Moist6",
        "Temp6",
    ]

    fnames = sorted(_tupelize(fnames))
    dfs = []

    for fname in fnames:
        content = _readSoilnetL0File(fname)
        df = pd.read_csv(
            StringIO(content),
            sep=" ",
            header=None,
            engine="c",
        )

        df.rename(columns=dict(zip(range(len(header)), header)), inplace=True)
        df = df[df["box"].str[-2:] == device_key[-2:]]
        df = df.loc[:, header].astype({c: np.int32 for c in header if c[-1].isdigit()})
        df["date"] = pd.to_datetime(
            df["date"].str.cat(df.pop("time"), sep=" "),
            format="%d.%m.%Y %H:%M:%S",
            errors="coerce",  # mark parsing erros as NaT
        )
        df = df.dropna(subset=["date"])
        df.columns.rename("headers", inplace=True)
        dfs.append(df)

    if dfs:
        return pd.concat(dfs)
    return pd.DataFrame()


def readSoilnetL0(fnames, device_key, index=True):
    df = _readSoilnetL0Files(fnames, device_key)
    df = df.filter(items=[c for c in df.columns if isinstance(c, str)])
    if index and not df.empty:
        df = df.set_index("date")
    return df


def readCaldata(path):
    pattern = re.compile(r"([0-9]+).csv$")
    fdict = {"fname": [], "start": []}
    for f in sorted(os.listdir(path)):
        m = pattern.search(f)
        if m:
            fdict["fname"].append(os.path.join(path, f))
            fdict["start"].append(datetime.strptime(m.group(1), "%Y%m%d%H%M"))

    calfiles = pd.DataFrame(data=fdict).sort_values(by="start", ascending=False)
    calfiles["end"] = ([datetime.today()] + list(calfiles["start"]))[: len(calfiles)]
    return calfiles


def _readSoilnetV3L0Files(fnames, device_key):
    header = [
        "date",
        "time",
        "ID",
        "Voltage",
        "counter",
        "qc_wifi",
        "dummy_rain",
        "Moist1_raw",
        "Moist1_perm",
        "Moist1",
        "Temp1",
        "sens1_volt",
        "Moist3_raw",
        "Moist3_perm",
        "Moist3",
        "Temp3",
        "sens3_volt",
        "Moist4_raw",
        "Moist4_perm",
        "Moist4",
        "Temp4",
        "sens4_volt",
        "Moist5_raw",
        "Moist5_perm",
        "Moist5",
        "Temp5",
        "sens5_volt",
        "Moist6_raw",
        "Moist6_perm",
        "Moist6",
        "Temp6",
        "sens6_volt",
        "Moist7_raw",
        "Moist7_perm",
        "Moist7",
        "Temp7",
        "sens7_volt",
    ]

    fnames = sorted(_tupelize(fnames))
    dfs = []

    for fname in fnames:
        content = _readSoilnetL0File(fname)
        df = pd.read_csv(
            StringIO(content),
            sep=" ",
            header=None,
            engine="c",
        )

        df.rename(columns=dict(zip(range(len(header)), header)), inplace=True)
        df = df[df["ID"].str[-2:] == device_key[3:5]]
        df = df.loc[:, header]
        df["date"] = pd.to_datetime(
            df["date"].str.cat(df.pop("time"), sep=" "),
            format="%d.%m.%Y %H:%M:%S",
            errors="coerce",  # mark parsing erros as NaT
        )
        df = df.dropna(subset=["date"])
        df.columns.rename("headers")
        dfs.append(df)

    if dfs:
        return pd.concat(dfs)
    return pd.DataFrame()


def readSoilnetL0V3(fnames, device_key, index=True):
    df = _readSoilnetV3L0Files(fnames, device_key)
    df = df.filter(items=[c for c in df.columns if isinstance(c, str)])
    if index and not df.empty:
        df = df.set_index("date")
    return df


def readCaldataICOS(path):
    # Extract fname, start date & box by fname.
    fdict = {"fname": [], "start": [], "box": []}
    date_pattern = re.compile(r"([0-9]+).csv$")
    box_pattern = re.compile(r"ICOS(..)")
    for f in os.listdir(path):
        m = date_pattern.search(f)
        if m:
            fdict["fname"].append(os.path.join(path, f))
            fdict["start"].append(datetime.strptime(m.group(1), "%Y%m%d%H%M"))
            fdict["box"].append(box_pattern.search(f).group(1))
    # Get end date as last occurrence grouped by box.
    cal = pd.DataFrame(data=fdict).sort_values(by=["box", "start"])
    cal["end"] = cal["start"][1:].tolist() + [datetime.today()]
    cal.loc[cal.groupby("box")["end"].tail(1).index, "end"] = datetime.today()
    return cal


def getICOSHeader(cal, box, mindate):
    # Find matching fname from cal file.
    headerfile = cal.loc[
        (cal["start"] <= mindate) & (cal["end"] > mindate) & (cal["box"] == box)
    ].iloc[0, 0]
    header = pd.read_csv(headerfile, usecols=["Variable", "Headerout"])
    header.set_index("Variable", inplace=True)
    header = header["Headerout"].str.replace("Box.._", "").to_dict()

    return header


def readSoilnetL0ICOS(fnames, calpath, device_key):
    cal = readCaldataICOS(calpath)
    pattern = re.compile(r"[0-9]+_L.(.)")
    fnames = sorted(_tupelize(fnames))
    dfs = []

    for fname in fnames:
        box = "4" + pattern.search(fname).group(1)
        if box != device_key[-2:]:
            continue
        content = _readSoilnetL0File(fname)
        df = pd.read_csv(
            StringIO(content),
            engine="c",
            sep=",",
            header=None,
            parse_dates=[0],
            date_format="%Y%m%d%H%M",
            # date_parser=lambda d: pd.to_datetime(d, format="%Y%m%d%H%M"),
        )
        if df.empty:
            continue

        box = "4" + pattern.search(fname).group(1)
        header = getICOSHeader(cal, box, df[0].min())
        df = (
            df.rename(columns=header, errors="ignore")
            .set_index("Date Time")
            .apply(pd.to_numeric, errors="coerce", downcast="float")
            .replace([np.inf, -np.inf], np.nan)
        )
        df["box"] = box
        dfs.append(df)

    df = pd.concat(dfs, sort=False).fillna(NODATA)
    # NOTE: df might contain corrupted dates
    df = df[df.index.notnull()]

    return df


def splitSoilnet(df):
    moist = df.filter(regex=".*_Moist[0-9]+(_f|\s+\[.*\])")
    temp = df.filter(regex=".*_Temp[0-9]+(_f|\s+\[.*\])")
    return moist, temp


def squeezeTimestamps(df):
    return df.replace(NODATA, np.nan).groupby(by=df.index).last().fillna(NODATA)


def squeezeColumns(df):
    out = {}
    for colname in df:
        col = df[colname].squeeze()
        if isinstance(col, pd.DataFrame):
            col = df[colname].fillna(method="ffill", axis="columns").iloc[:, -1]
        out[colname] = col
    return pd.DataFrame(out)


def dropEmptyVariables(df):
    out = reindexTable(df)
    data = out.xs("data", level=1, axis=1)
    dcols = data.columns.get_level_values(IndexFields.VARNAME)[
        ((data == NODATA) | data.isnull()).all()
    ]
    if len(dcols):
        out = out.drop(dcols, axis=1, level=dcols.name)
    colvals = [out.columns.get_level_values(l) for l in df.columns.names]
    # preserve the MultiIndex if necessary
    if isinstance(df.columns, pd.MultiIndex):
        out.columns = pd.MultiIndex.from_arrays(colvals)
    else:
        out.columns = colvals[0]
    return out


def iterDataFreq(df, freq, drop_empty=True):
    # Purpose:
    # iterate the grouped data frame, the grouping is based on
    # the given frequency on a datetime index

    freqs = {"yearly": "AS", "daily": "D", "monthly": "MS"}
    funcs = {"AS": firstOfYear, "D": firstOfDay, "MS": firstOfMonth}

    if df.index.empty:
        return

    if not isinstance(df.index, pd.DatetimeIndex):
        raise TypeError("DataFrame index should by a DatetimeIndex")

    freq = freqs.get(freq, freq)
    func = funcs[freq]

    for date, data in df.groupby(by=pd.Grouper(freq=freq, closed="right")):
        if drop_empty is True:
            data = dropEmptyVariables(data)
        if not data.empty:
            yield func(date), data


def limitData(df, config, inplace=False, nodata=np.nan):
    # Limit the data to the range given in config
    # Arguments:
    #     df: the data to limit
    #     config: the CHS_measurements data for the device the data belongs to
    #     inplace: change data in place (all references to df see the changes)
    #     nodata: data to replace the values out of bounds

    df = df.copy(deep=(not inplace))
    df = reindexTable(df)
    for varname in df.columns.get_level_values(IndexFields.VARNAME):
        limits = config[config[ConfigFields.VARNAME] == varname]
        if not limits.empty:
            lower = float(limits[ConfigFields.MIN])
            upper = float(limits[ConfigFields.MAX])
            data = df[(varname, "data")]
            idx = (data < lower) | (data >= upper)
            data[idx] = nodata
            df[(varname, "data")] = data
    return df
