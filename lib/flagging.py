#! /usr/bin/env python
# -*- coding: utf-8 -*-

from math import isnan
from typing import Union

import numpy as np
import numba as nb
import pandas as pd

from .tools import _numpyfy
from config.data import NODATA


np.seterr(divide="ignore", invalid="ignore")


def _prepFlags(flags):
    out = _numpyfy(flags)
    out[~np.isfinite(out)] = NODATA
    return out


def broadcastMany(*args):
    arrays = [np.atleast_1d(a) for a in args]
    target_ndim = max(arr.ndim for arr in arrays)
    out = []
    for arr in arrays:
        out.append(arr[(slice(None),) + (None,) * (target_ndim - arr.ndim)])
    target_shape = np.broadcast(*out).shape
    return tuple(np.broadcast_to(arr, target_shape) for arr in out)


def setFlags(
    flags,  # type: Union[pd.Series, pd.DataFrame, np.array]
    pos,  # type: Union[int, pd.Series, pd.DataFrame, np.array]
    values,  # type: Union[int, pd.Series, pd.DataFrame, np.array]
):  # type: (...) -> np.array
    # Purpose: Set (values > 0) at position 'pos' in 'flags'

    # NOTE:
    #     if not integers, flags, pos and values need to have the
    #     same shape or align on the first dimension

    flags, pos, values = broadcastMany(flags, pos, values)

    out = flags.astype(np.float64)
    # initialize previously uninitialized flags
    out[out <= 0] = 9

    # right-pad 'flags' with zeros, to assure the
    # desired flag position is available
    ndigits = np.floor(np.log10(out)).astype(int)
    idx = (ndigits < pos) & (values >= 0)  # 'values' < 0 shall be skipped
    out[idx] *= 10 ** (pos[idx] - ndigits[idx])
    ndigits = np.log10(out).astype(int)

    out[idx] += 10 ** (ndigits[idx] - pos[idx]) * values[idx]

    return out.astype(int)


@nb.jit(nopython=True, cache=True)
def _broadcastTo(array, shape):
    # create an broadcasted view into array

    for i, v in enumerate(list(shape)[-array.ndim :]):
        if v != array.shape[i]:
            raise ValueError("operands could not be broadcast together")

    out = np.zeros(shape, dtype=array.dtype).ravel()
    out[-array.size :] = array.ravel()
    return out.reshape(shape)


@nb.jit(nopython=True)
def digits(number):
    """
    find the largest digit in number,
    optinally skipping 'skip' number of digits
    """
    out = []
    while number:
        digit = number % 10
        digit = number % 10
        out.append(digit)
        number //= 10
    return out[::-1]


def getMaxflags(flags, exclude=0):
    flagmax = np.max(np.array(flags))
    exclude = set(np.array(exclude).ravel())

    if flagmax == NODATA or isnan(flagmax):
        # flags not initialized yet
        return np.full_like(flags, -2)
    if flagmax == 9:
        # intialized but not set
        return np.full_like(flags, 0)

    out = np.full_like(flags, -2)
    ndigits = int(np.ceil(np.log10(flagmax)))

    for pos in range(ndigits):
        if pos not in exclude:
            out = np.maximum(out, getFlags(flags, pos))

    return out


def getFlags(flags, pos):
    # type: (Union[pd.Series, pd.DataFrame, np.array], int) -> np.array
    # NOTE: rename getFlags -> getFlag

    flags = _prepFlags(flags)
    pos = np.broadcast_to(np.atleast_1d(pos), flags.shape)

    out = np.full_like(flags, 0)
    out[flags <= 0] = -2
    ndigits = np.floor(np.log10(flags)).astype(int)

    idx = np.where(ndigits >= pos)
    out[idx] = flags[idx] // 10 ** (ndigits[idx] - pos[idx]) % 10

    return out
