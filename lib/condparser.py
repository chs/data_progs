#! /usr/bin/env python
# -*- coding: utf-8 -*-

import ast
import copy
import operator as op


import pandas as pd

from lib.flagging import getMaxflags


# supported operators
OPERATORS = {
    ast.Add: op.add,
    ast.Sub: op.sub,
    ast.Mult: op.mul,
    ast.Div: op.truediv,
    ast.Pow: op.pow,
    ast.USub: op.neg,
    ast.NotEq: op.ne,
    ast.Eq: op.eq,
    ast.Gt: op.gt,
    ast.GtE: op.ge,
    ast.Lt: op.lt,
    ast.LtE: op.le,
    ast.BitAnd: op.and_,
    ast.BitOr: op.or_,
    ast.BitXor: op.xor,
}

FUNCTIONS = {"abs": (abs, "data"), "maxflag": (getMaxflags, "flag")}


def setKey(d, key, value):
    # type: (Dict, Any, Any) -> Dict  # NOTE: bad hint, the key needs to be immutable
    out = copy.copy(d)
    out[key] = value
    return out


def evalFunction(expr, context):
    return _eval(ast.parse(expr, mode="eval").body, context)


def _eval(node, context):
    # type: (ast.Node, Dict) -> None
    # the context dictionary should provide the data frame for the device
    # being processed and any additional variables (e.g. NODTA, this)

    if isinstance(node, ast.Num):  # <number>
        return node.n

    elif isinstance(node, ast.UnaryOp):
        return OPERATORS[type(node.op)](_eval(node.operand, context))

    elif isinstance(node, ast.BinOp):
        return OPERATORS[type(node.op)](
            _eval(node.left, context), _eval(node.right, context)
        )

    elif isinstance(node, ast.Compare):
        # NOTE: chained comparison not supported yet
        op = OPERATORS[node.ops[0].__class__]
        return op(_eval(node.left, context), _eval(node.comparators[0], context))

    elif isinstance(node, ast.Call):
        # functions out of math are allowed
        # kwargs not supported yet
        func, access = FUNCTIONS[node.func.id]
        context = setKey(context, "access", access)
        args = [_eval(n, context) for n in node.args]
        return func(*args)

    elif isinstance(node, ast.Name):  # <variable>
        # get the value, this is starting to become ugly
        this = context["this"]
        # limit data to the dates of this
        data = context["data"].loc[this.index]

        if node.id in context:
            # at the moment only "this"
            name = context[node.id].name
            # this.name is most likely hidden within the lower levels of data.columns
            node.id = [col[0] for col in data.columns if name in col][0]

        tag = context.get("access", "data")
        var = data[node.id]
        out = var[tag]
        if tag == "data":
            out = out.mask(getMaxflags(var["flag"]) == 2)

        # NOTE: flags not taken into account yet, this needs to change
        if isinstance(out, pd.Series):
            # working on numpy arrays to save resources
            out = out.values.ravel()
        elif isinstance(out, pd.DataFrame):
            assert len(out.columns) == 1
            out = out.values.ravel()
        return out

    else:
        raise TypeError(node)
