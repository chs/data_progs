#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re
from datetime import datetime, date, timedelta
from itertools import groupby
from pathlib import Path
from typing import List

import pandas as pd
import numpy as np

from .daccess import ConfigFields as CF
from config.data import MINDATE, MAXDATE, NODATA
from config.uri import (
    ROOT,
    FTPROOT,
    CACHEPATH,
)
from .daccess import (
    prepareTable,
    readLoggerL0,
    readSoilnetL0,
    readSoilnetL0V3,
    readSoilnetL0ICOS,
    readExcel,
    _toDatetime,
    readCaldata,
    setTypes,
    IndexFields,
)
from .ftputils import (
    sftpConnection,
    getFileListSFTP,
    remoteDirectorySFTP,
)
from .tools import (
    _tupelize,
    toDate,
    _stringFname,
    _stringPname,
)


class Device(object):
    _l0suffix = ".dat"  # likely to change in subclasses
    _l1suffix = ".level1.csv"  # should be fix

    _TRANSFER_CONFIG = "Transfer_Level0_{key}.txt"
    _MANFLAG_CONFIG = "Manual_Flags_Logger_{device_key}.csv"  # should be fix

    _l0subfolder = "[0-9][0-9][0-9][0-9]"
    _l1subfolder = "[0-9][0-9][0-9][0-9]"

    # shall we check the download (do_check_download)?
    check = True

    def __init__(self, start_date=None, end_date=None, **kwargs):
        self._start_date = start_date
        self._end_date = end_date
        for k, v in kwargs.items():
            setattr(self, k, v)
        self._config = None

    def __str__(self):
        return "Device({:} - {:})".format(self.station_key, self.logger_key)

    __repr__ = __str__

    def getStation(self):
        from .faccess import getStations

        return tuple(
            getStations(
                station=self.station_key,
                start_date=self.start_date,
                end_date=self.end_date,
            )
        )[0]

    @property
    def config(self):
        if self._config is None:
            self._config = readExcel(self.xls_sheets).dropna(
                subset=[CF.DEVICE, CF.DBNAME]
            )
        return self._config

    @property
    def localpath(self):
        return str(Path(self.stationpath, self.local_path))

    @property
    def stationpath(self):
        return str(Path(ROOT, self.station_path))

    @property
    def start_date(self):
        if self._start_date is None:
            self._start_date = self._getStartDate()
        return (
            self._start_date
            if self._start_date is None
            else pd.Timestamp(self._start_date)
        )

    @property
    def end_date(self):
        if self._end_date is None:
            self._end_date = self._getEndDate()
        return (
            self._end_date if self._end_date is None else pd.Timestamp(self._end_date)
        )

    @property
    def timestep(self):
        return self.sampling_frequency

    @property
    def manflag_file(self):
        return str(
            Path(self.path, self._MANFLAG_CONFIG.format(device_key=self.logger_key))
        )

    @property
    def l0path(self):
        return str(Path(self.localpath, self.level0_path))

    @property
    def derivedpath(self):
        return str(Path(self.localpath, self.derived_path))

    @property
    def archivepath(self):
        return str(Path(self.stationpath, self.archive_path))

    @property
    def l1path(self):
        return str(Path(self.localpath, self.level1_path))

    @property
    def path(self):
        return self.localpath

    @property
    def calpath(self):
        return str(Path(self.localpath, self.cal_path))

    def getConfigFiles(self, filter=True):
        out = readCaldata(self.calpath)
        if filter is True:
            out[out["end"] > pd.to_datetime(self.start_date)]
        return out

    def _getStartDate(self, key=None, **kwargs):
        key = key if key is not None else self.logger_key
        try:
            xls = self.readExcel(**kwargs)
            out = xls.set_index(CF.DEVICE).loc[key, CF.STARTDATE].dropna().min()
            return out
        except (ValueError, KeyError):
            return MINDATE

    def _getEndDate(self, key=None, **kwargs):
        key = key if key is not None else self.logger_key
        try:
            xls = self.readExcel(**kwargs)
            end_dates = xls.set_index(CF.DEVICE).loc[key, CF.ENDDATE]
            if pd.isnull(end_dates).any():
                return date.today()
            return end_dates.max()
        except (ValueError, KeyError):
            return MAXDATE

    def getManualFlags(self):
        formats = {"%d.%m.%Y %H:%M:%S", "%d.%m.%Y %H:%M"}

        df = pd.read_csv(self.manflag_file, comment="#", encoding="latin")
        try:
            df["start"] = _toDatetime(df["start"], formats)
            df["end"] = _toDatetime(df["end"], formats)
        except ValueError as e:
            raise ValueError("invalid date format in manual flag file") from e

        config = self.readExcel().set_index(CF.VARNAME)
        now = pd.Timestamp.now()
        df = df.set_index("var_id").fillna(np.nan)
        df.index = df.index.str.split(" ").str[0]

        # fill start/end dates with valid values
        for var in df.index:
            if var in config.index:
                df.loc[[var], "start"] = df.loc[[var], "start"].fillna(
                    config.loc[var, CF.START_DATE]
                )
                df.loc[[var], "start"] = df.loc[[var], "start"].fillna(now)
                df.loc[[var], "end"] = df.loc[[var], "end"].fillna(
                    config.loc[var, CF.END_DATE]
                )
                df.loc[[var], "end"] = df.loc[[var], "end"].fillna(now)
        return df

    def readDerived(self, tag: str):
        path = Path(CACHEPATH)
        fname = tuple(path.glob(f"*{self.logger_key}-{tag}*.parquet"))[0]
        return pd.read_parquet(fname)

    def getTransferConfig(self):
        return str(Path(self.path, self._TRANSFER_CONFIG.format(key=self.logger_key)))

    def readExcel(self, filter=True):
        # type: () -> pd.DataFrame
        """
        Purpose:
            Return the logger relevant data from config.uri.XLSFILE
        """
        out = self.config
        if filter is True:
            out = out[out[CF.DEVICE] == self.logger_key]
        return out

    def getRemoteFiles(self, pattern=None):
        # type: () -> List[List[str, int]]
        #             List[List[fname, fsize]]
        fpattern = ["*" + s for s in _tupelize(pattern or self._l0suffix)]
        with sftpConnection(FTPROOT, self.sftp_user) as sftp:
            with remoteDirectorySFTP(sftp, self.remote_path):
                flist = getFileListSFTP(sftp, fpattern, size=True, recurse=True)
        return flist

    def getL0Files(self, *args, **kwargs):
        def _buildSuffixPatterns(paths, suffixes):
            out = []
            for path in _tupelize(paths):
                for suffix in _tupelize(suffixes):
                    out.append(str(Path(path, "*" + suffix)))
            return out

        patterns = _buildSuffixPatterns(self._l0subfolder, self._l0suffix)

        out = self._iterLXFiles(path=self.l0path, pattern=patterns, *args, **kwargs)
        return out

    def getL1Files(self, *args, **kwargs):
        def _buildSuffixPatterns(path, suffixes):
            out = []
            for suffix in _tupelize(suffixes):
                out.append(str(Path(path, "*" + suffix)))
                # yield str(Path(path, "*" + suffix))
            return out

        patterns = _buildSuffixPatterns(self._l1subfolder, self._l1suffix)

        return self._iterLXFiles(path=self.l1path, pattern=patterns, *args, **kwargs)

    def _iterLXFiles(
        self, path, pattern, start_date=None, end_date=None, dateFunc=None
    ):
        def _globMultiple(path, patterns):
            out = []
            for pat in patterns:
                out.extend(path.glob(pat))
            return out

        path = Path(path)
        patterns = _tupelize(pattern)

        dateFunc = dateFunc or self.getFnameDate
        start_date = pd.Timestamp(
            pd.to_datetime([start_date, self.start_date, MINDATE]).dropna()[0]
        )
        end_date = pd.Timestamp(
            pd.to_datetime([end_date or self.end_date, MAXDATE]).dropna()[0]
        )
        path = Path(path)

        out = []
        for f in sorted(_globMultiple(path, patterns), key=dateFunc):
            fd = pd.Timestamp(dateFunc(f))
            if fd >= start_date:
                if fd <= end_date:
                    out.append(str(f))
                else:
                    break
        return out

    def translateRemoteToL0(self, fname):
        remote_fname = Path(fname)
        date = self.getFnameDate(fname)
        local_fname = Path(
            self.l0path, str(date.year) if date else "", remote_fname.name
        )
        return str(local_fname)

    def getFnameDate(self, direntry):
        fname = _stringFname(direntry)
        for pattern, fmt in self._fname_date_dict.items():
            try:
                datestring = pattern.search(fname).group()
            except AttributeError:
                continue
            return datetime.strptime(datestring, fmt)
        raise ValueError("Could not extract date from '{:}'".format(fname))

    def _getPath(self, level):
        return Path(
            CACHEPATH, f"{self.station_key}-{self.logger_key}-level{level}.parquet"
        )

    def _prepSlicingDates(self, index, start_date=None, end_date=None):
        # NOTE
        # we only accpet datetime.dates as slicing arguments, to anyhow stick to the
        # date conventions (datasets start with timestamp '00:10:00' and end at '00:00:00')
        # we need to do some extra work
        start_date = (
            pd.to_datetime([start_date, self.start_date, index[0]])
            .max()
            .to_pydatetime()
        )
        end_date = (
            pd.to_datetime([end_date, self.end_date, index[-1]]).min().to_pydatetime()
        )
        return start_date, end_date

    def _getLevelData(
        self,
        level,
        start_date=None,
        end_date=None,
        fill=False,
        columns=None,
        *args,
        **kwargs,
    ):
        try:
            data = pd.read_parquet(
                self._getPath(level=level), columns=columns
            ).sort_index()
        except FileNotFoundError:
            return pd.DataFrame()
        if data.empty:
            return data

        start_date, end_date = self._prepSlicingDates(data.index, start_date, end_date)
        data = data.loc[start_date:end_date].fillna(NODATA)
        freq = self.timestep if fill is True else (fill or None)
        out = prepareTable(data, freq=freq, *args, **kwargs)
        return out

    def getL0Data(self, start_date=None, end_date=None, fail=True, *args, **kwargs):
        start_date = toDate(start_date or self.start_date)
        end_date = toDate(end_date or self.end_date)

        # the requested dates might be stored in the surrounding files,
        # thats why we slightly increase the number of files we read
        tdelta = timedelta(days=5)
        files = self.getL0Files(
            start_date=start_date - tdelta, end_date=end_date + tdelta
        )
        df = readLoggerL0(files, fail=fail)
        return prepareTable(df.sort_index().loc[start_date:end_date], *args, **kwargs)

    def getL1Data(
        self, start_date=None, end_date=None, fill=False, columns=None, *args, **kwargs
    ):
        data = self._getLevelData(
            level=1, start_date=start_date, end_date=end_date, *args, **kwargs
        )
        # NOTE: maybe we could generalize frequency changes through the config/devices.py ?
        if self.logger_key == "T4":
            split_date = "2018-06-14 10:00:00"
            data = pd.concat(
                [
                    prepareTable(data.loc[:split_date], freq="10Min", *args, **kwargs),
                    prepareTable(data.loc[split_date:], freq="5Min", *args, **kwargs),
                ]
            )
            data.columns.name = IndexFields.HEADER

        return data

    def getL2Data(
        self, start_date=None, end_date=None, fill=False, columns=None, *args, **kwargs
    ):
        return self._getLevelData(
            level=2, start_date=start_date, end_date=end_date, *args, **kwargs
        )

    def _setLevelData(self, df, level):
        out = setTypes(df.fillna(NODATA))
        out.to_parquet(self._getPath(level=level))

    def setL1Data(self, df):
        self._setLevelData(df, level=1)

    def setL2Data(self, df):
        self._setLevelData(df, level=2)


class SoilnetDeviceBase(Device):
    _MANFLAG_CONFIG = "Manual_Flags_Soilnet_{station_key}.csv"

    @property
    def manflag_file(self):
        return str(
            Path(self.path, self._MANFLAG_CONFIG.format(station_key=self.station_key))
        )

    def getL1Data(self, start_date=None, end_date=None, fill=False, *args, **kwargs):
        data = self._getLevelData(
            level=1, start_date=start_date, end_date=end_date, *args, **kwargs
        )
        if IndexFields.TAG in data.columns.names:
            data = data.droplevel(IndexFields.TAG, axis="columns")
        return data

    def _getStartDate(self, **kwargs):
        # key = int(self.logger_key.replace("Box", ""))
        key = self.logger_key.replace("Box", "")
        return super()._getStartDate(key, **kwargs)

    def _getEndDate(self, **kwargs):
        # key = int(self.logger_key.replace("Box", ""))
        key = self.logger_key.replace("Box", "")
        return super()._getEndDate(key, **kwargs)

    def setL1Data(self, df):
        out = setTypes(df.fillna(NODATA))
        out.to_parquet(self._getPath(level=1))

    def readExcel(self, filter=True):
        # type: () -> pd.DataFrame
        """
        Purpose:
            Return the logger relevant data from config.uri.XLSFILE
        """
        out = self.config
        if filter is True:
            device_names = out[CF.DEVICE].astype(int).map("Box{:02}".format)
            out = out[device_names == self.logger_key]
        return out


class SoilnetDevice(SoilnetDeviceBase):
    _fname_date_dict = {re.compile(r"\d{8}"): "%Y%m%d"}
    _l0suffix = ".dat"

    def __init__(self, **kwargs):
        super(SoilnetDevice, self).__init__(**kwargs)
        self.xls_sheets = self.xls_sheets.split(",")

    def _getStartDate(self):
        return super(SoilnetDevice, self)._getStartDate(filter=False)

    def _getEndDate(self):
        return super(SoilnetDevice, self)._getEndDate(filter=False)

    def _getDate(self, content):
        pattern = re.compile(r"\d{2}\.\d{2}\.\d{4}\s+\d{2}:\d{2}:\d{2}")
        fmt = "%d.%m.%Y %H:%M:%S"
        datestrings = pattern.findall(content)
        dates = [datetime.strptime(d, fmt) for d in datestrings]
        return max(dates)

    def translateRemoteToL0(self, remote_fname, fname_part="SoilNet_Data_{:}_{:}.dat"):
        with sftpConnection(FTPROOT, self.sftp_user) as sftp:
            with sftp.open(str(Path(self.remote_path, remote_fname)), mode="r") as f:
                length = min(f.stat().st_size, 2000)
                f.seek(-length, 2)
                content = f.read()
                fdate = self._getDate(str(content))

        fname = fname_part.format(self.station_key, datetime.strftime(fdate, "%Y%m%d"))
        local_fname = Path(self.l0path, str(fdate.year), fname)
        return str(local_fname)

    def getL0Data(self, *args, **kwargs):
        out = readSoilnetL0(self.getL0Files(), self.logger_key, *args, **kwargs)
        return out.sort_index().loc[self.start_date : self.end_date]

    def getRemoteFiles(self, pattern="SNDTRO_.asc"):
        return super().getRemoteFiles(pattern=pattern)


class SoilnetICOSDevice(SoilnetDeviceBase):
    _l0suffix = ".csv"
    _fname_date_dict = {re.compile(r"\d{8}"): "%Y%m%d"}

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def getL0Files(self, *args, **kwargs):
        """
        There is file 'test.asc' in the rawdata directory,
        that gets updated every day, skip that.
        """

        def _buildSuffixPatterns(path, suffixes):
            out = []
            for suffix in _tupelize(suffixes):
                out.append(str(Path(path, "DE-*" + suffix)))
                # yield str(Path(path, "DE-*" + suffix))
            return out

        patterns = _buildSuffixPatterns(self._l0subfolder, self._l0suffix)

        return self._iterLXFiles(path=self.l0path, pattern=patterns, *args, **kwargs)

    def getL0Data(self, *args, **kwargs):
        out = readSoilnetL0ICOS(
            self.getL0Files(), self.calpath, self.logger_key, *args, **kwargs
        ).sort_index()
        return out.loc[
            max(self.start_date, out.index[0]) : min(self.end_date, out.index[-1])
        ]


class SoilnetV3Device(SoilnetDevice):
    _l0suffix = ".dat"
    _fname_date_dict = {re.compile(r"\d{8}"): "%Y%m%d"}

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def translateRemoteToL0(self, remote_fname):
        return super().translateRemoteToL0(
            remote_fname=remote_fname, fname_part="{:}_soilnet-v3_{:}.dat"
        )

    def getRemoteFiles(self):
        return super().getRemoteFiles(pattern=None)

    def getL0Data(self, *args, **kwargs):
        out = readSoilnetL0V3(self.getL0Files(), self.logger_key, *args, **kwargs)
        return out.sort_index().loc[self.start_date : self.end_date]


class SoilrespirationDevice(Device):
    _l0suffix = ".81x"
    check = False

    def __init__(self, **kwargs):
        super(SoilrespirationDevice, self).__init__(**kwargs)

    def _getDate(self, content):
        pattern = re.compile(r"\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}")
        fmt = "%Y-%m-%d %H:%M:%S"
        datestrings = pattern.findall(content)
        dates = [datetime.strptime(d, fmt) for d in datestrings]
        return max(dates)

    def _getRemoteFnameDate(self, fname):
        with sftpConnection(FTPROOT, self.sftp_user) as sftp:
            with sftp.open(str(Path(self.remote_path, fname)), mode="r") as f:
                length = min(f.stat().st_size, 2000)
                f.seek(-length, 2)
                content = f.read()
                fdate = self._getDate(str(content))
        return fdate

    def _getLocalFnameDate(self, fname):
        direntry = _stringPname(direntry)
        with open(direntry, "rb") as f:
            # we don't need the entire file,
            # the last 2000 bytes are enough
            f.seek(-2000, 2)
            content = f.read()
        return self._getDate(content.decode("utf-8"))

    def getFnameDate(self, fname):
        # every filename is created by a human operator -> a really bad mess
        # extract the dates from the file content
        if Path(fname).is_file():
            return self._getLocalFnameDate(fname)
        return self._getRemoteFnameDate(fname)


class MeteoDevice(Device):
    _date_parts = slice(-4, None)
    _fname_date_dict = {re.compile(r"\d{4}_\d{2}_\d{2}_\d{4}"): "%Y_%m_%d_%H%M"}

    def __init__(self, **kwargs):
        super(MeteoDevice, self).__init__(**kwargs)


class PhenoDevice(Device):
    _l0suffix = "[0-9].jpg"
    _fname_date_dict = {
        re.compile(r"\d{4}_\d{2}_\d{2}_\d{2}_\d{2}_\d{2}"): "%Y_%m_%d_%H_%M_%S",
        re.compile(r"\d{14}"): "%Y%m%d%H%M%S",
    }
    _archivefname = "{station}_PhenoPhoto_{first}-{last}.zip"

    def __init__(self, **kwargs):
        super(PhenoDevice, self).__init__(**kwargs)

    def iterArchiveGroups(self, fnames=None):
        fnames = sorted(fnames or self.getL0Files())
        dates = sorted(self.getFnameDate(f) for f in fnames)
        archivefname = self._archivefname.format(
            station=self.station_key,
            year=dates[0].year,
            first=dates[0].strftime("%Y%m%d"),
            last=dates[-1].strftime("%Y%m%d"),
        )
        yield archivefname, fnames


class EddyICOSDevice(Device):
    _l0folders = {".ghg": "raw", ".zip": "results", ".txt": "summaries"}
    _archivefname = "{station}{year}_ICOS_Eddy_{sub}_DOY{first}-DOY{last}.zip"

    _fname_date_dict = {
        re.compile(r"^\d{4}-\d{2}-\d{2}T\d{6}"): "%Y-%m-%dT%H%M%S",
        re.compile(r"^\d{4}-\d{2}-\d{2}"): "%Y-%m-%d",
    }

    def __init__(self, **kwargs):
        super(EddyICOSDevice, self).__init__(**kwargs)
        self._l0suffix = self._l0folders.keys()
        self._l0subfolder = (
            "{:}/[0-9][0-9][0-9][0-9]".format(f) for f in self._l0folders.values()
        )

    def iterArchiveGroups(self, fnames=None):
        folders = set(["raw", "results"])
        groups = groupby(
            sorted(fnames or self.getL0Files()),
            key=lambda f: str(Path(f).relative_to(self.localpath).parents[1]),
        )
        for k, g in groups:
            if k in folders:
                fnames = tuple(g)
                dates = sorted(self.getFnameDate(f) for f in fnames)
                archivefname = self._archivefname.format(
                    station=self.station_key,
                    year=dates[0].year,
                    sub=k,
                    first=dates[0].strftime("%j"),
                    last=dates[-1].strftime("%j"),
                )
                yield archivefname, fnames

    def translateRemoteToL0(self, fname):
        remote_fname = Path(fname)
        date = self.getFnameDate(fname)
        local_fname = Path(
            self.l0path,
            self._l0folders[remote_fname.suffix],
            str(date.year),
            remote_fname.name,
        )
        return str(local_fname)


class EddyBCDevice(EddyICOSDevice):
    _l0folders = {
        "biomet.data": "biomet",
        "biomet.metadata": "biomet",
        "daqm.log": "biomet",
        "daqm.zip": "biomet",
        ".ghg": "raw",
        ".zip": "results",
        ".txt": "summaries",
        ".metadata": "metadata",
        ".data": "metadata",
    }

    _archivefname = "{station}{year}_BC_Eddy_{sub}_DOY{first}-DOY{last}.zip"

    def _getFolder(self, fname):
        for pattern, folder in self._l0folders.items():
            if fname.endswith(pattern):
                return folder
        raise ValueError(f"No target directory founf for {fname}")

    def translateRemoteToL0(self, fname):
        remote_fname = Path(fname)
        date = self.getFnameDate(fname)
        local_fname = Path(
            self.l0path,
            self._getFolder(remote_fname.name),
            str(date.year),
            remote_fname.name,
        )
        return str(local_fname)


class EddyDevice(Device):
    _l0suffix = (".zip", ".slt", ".dia", ".log", ".flx", ".csv", ".csu", ".csr", ".cfg")
    _l0subfolder = "Raw-Zip_[0-9][0-9][0-9][0-9]"

    _archivefname = "{station}{year}_Eddy-Data_DOY{first}-DOY{last}.zip"
    _fname_date_dict = {
        re.compile(r"\d{4}_\d{3}"): "%Y_%j",
        re.compile(r"\d{7}"): "%Y%j",
    }

    def iterArchiveGroups(self, fnames=None):
        fnames = sorted(fnames or self.getL0Files())
        dates = sorted(self.getFnameDate(f) for f in fnames)
        archivefname = self._archivefname.format(
            station=self.station_key,
            year=dates[0].year,
            first=dates[0].strftime("%j"),
            last=dates[-1].strftime("%j"),
        )
        yield archivefname, fnames

    def __init__(self, **kwargs):
        super(EddyDevice, self).__init__(**kwargs)

    def getL0Files(self, *args, **kwargs):
        return self._iterLXFiles(
            path=self.l0path,
            pattern=str(Path(self._l0subfolder, "*.zip")),
            *args,
            **kwargs,
        )

    def _readProcessedLists(self):
        out = []
        for fname in Path(self.path).glob("**/processed_rawdata.txt"):
            with open(str(fname), "r") as f:
                out.extend([l.strip() for l in f.readlines()[1:]])
        return out

    def getRemoteFiles(self):
        # Returns [[fname, fsize, fdate],]
        skip_files = set(self._readProcessedLists())
        flist = super().getRemoteFiles()
        out = []
        for ftuple in flist:
            if Path(ftuple[0]).name in skip_files:
                continue
            out.append(ftuple)
        return out

    def translateRemoteToL0(self, fname):
        date = self.getFnameDate(fname)
        _raw_folders = {
            ".zip": "Raw-Zip_{:}".format(date.year),
            ".slt": "Rawdata{:}".format(date.year),
            ".dia": "Rawdata{:}".format(date.year),
            ".log": "Log-Files",
            ".flx": "Flx-Files",
            ".csv": "Flx-Files",
            ".csu": "Flx-Files",
            ".csr": "Flx-Files",
            ".cfg": "Config-Files",
        }

        remote_fname = Path(fname)
        local_fname = Path(
            self.l0path, _raw_folders[remote_fname.suffix], remote_fname.name
        )
        return str(local_fname)
