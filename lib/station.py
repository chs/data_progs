#! /usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
from typing import Any, Sequence

import pandas as pd

from .tools import _tupelize
from .daccess import ConfigFields as CF
from .daccess import readExcel, readTable, prepareTable
from .pdutils import concatOrdered
from config.uri import ROOT


class Station(object):
    def __init__(self, data, start_date=pd.NaT, end_date=pd.NaT):
        """
        data: config/data.csv content
        """
        self._setKeys(data)
        self.start_date = start_date or self._getStartDate()
        self.end_date = end_date or self._getEndDate()

    def _setKeys(self, data):
        for key in data.columns:
            values = data[key].drop_duplicates()
            if len(values) == 1:
                values = values.iloc[0]
            setattr(self, key, values)

    @property
    def name(self):
        return self.station_name

    @property
    def path(self):
        path = self.station_path
        return str(Path(ROOT, path))

    @property
    def soilnet_path(self):
        paths = set()
        for device in self.getDevices(tag=("soilnet", "soilnet_icos")):
            paths.add(device.path)
        if len(paths) == 0:
            raise RuntimeError("No soilnet root path found")
        if len(paths) > 1:
            raise RuntimeError("Multiple soilnet root paths found")
        return paths.pop()

    def _getStartDate(self):
        xls = self.readExcel()
        # not all sheets have an startdate field (i.e. soilnet GB)
        try:
            return xls[CF.STARTDATE].dropna().min()
        except KeyError:
            return pd.NaT

    def _getEndDate(self):
        xls = self.readExcel()
        # not all sheets have an enddate field (i.e. soilnet GB)
        try:
            end_dates = xls[CF.ENDDATE]
            if end_dates.isna().any():
                return pd.Timestamp.today()
            return end_dates.max()
        except KeyError:
            return pd.NaT

    def __str__(self):
        return "Station({:})".format(self.station_key)

    __repr__ = __str__

    def readExcel(self):
        sheets = set()
        # for sht in self._data["xls_sheets"]:
        for sht in _tupelize(self.xls_sheets):
            if sht:
                sheets = sheets | set(sht.split(","))
        return readExcel(sheets)

    def getDevice(self, device_key):
        key = self.logger_key[self.logger_key == device_key]
        if len(key) > 1:
            raise RuntimeError(
                "Ambiguous device key. Found {:}".format(key["logger_key"].tolist())
            )
        return tuple(self.getDevices(device_key))[0]

    def getDevices(self, devices=None, start_date=None, end_date=None, **kwargs):
        from .faccess import getDevices

        devices = _tupelize(devices) if devices else _tupelize(self.logger_key)
        return getDevices(
            station=self.station_key,
            logger=devices,
            start_date=start_date or self.start_date,
            end_date=end_date or self.end_date,
            **kwargs
        )

    def getL1Data(self, devices=None, start_date=None, end_date=None, *args, **kwargs):
        dfs = []
        for dev in self.getDevices(devices, start_date, end_date):
            dfs.append(dev.getL1Data(*args, **kwargs))

        out = concatOrdered(dfs)
        # NOTE: That's the way to drop duplicates
        return out.groupby(out.index).last().dropna(axis="index", how="all")

    def getDerivedData(
        self,
        var: str,
        start_date: pd.Timestamp = None,
        end_date: pd.Timestamp = None,
        variables: Sequence[str] = None,
        *args: Any,
        **kwargs: Any
    ) -> pd.DataFrame:
        try:
            out = self._getDerivedDataStation(
                var, start_date, end_date, variables, *args, **kwargs
            )
            if not out.empty:
                return out
        except ValueError:
            pass

        try:
            out = self._getDerivedDataDevices(
                var, start_date, end_date, variables, *args, **kwargs
            )
            if not out.empty:
                return out
        except ValueError:
            pass

        raise AttributeError(
            "No derived property '{:}' found for station '{:}'".format(
                var, self.station_key
            )
        )

    def _getDerivedDataStation(
        self,
        var: str,
        start_date: pd.Timestamp = None,
        end_date: pd.Timestamp = None,
        variables: Sequence[str] = None,
        *args: Any,
        **kwargs: Any
    ) -> pd.DataFrame:
        path = Path(self.path, "derived", var)
        data = readTable(path.glob("*.csv"), variables=variables)
        data = data[start_date or self.start_date : end_date or self.end_date]
        return prepareTable(data, *args, **kwargs)

    def _getDerivedDataDevices(
        self,
        var: str,
        start_date: pd.Timestamp = None,
        end_date: pd.Timestamp = None,
        variables: Sequence[str] = None,
        *args: Any,
        **kwargs: Any
    ) -> pd.DataFrame:
        config = self.readExcel()
        if variables is not None:
            variables = _tupelize(variables)
            config = config[config[CF.VARNAME].str.contains("|".join(variables))]
        config = config.loc[:, [CF.VARNAME, CF.DEVICE]]

        dfs = []
        for device, device_config in config.groupby(CF.DEVICE):
            dev = self.getDevice(device)
            data = dev.getDerivedData(
                var, start_date, end_date, device_config[CF.VARNAME], *args, **kwargs
            )
            dfs.append(data)

        if dfs:
            return dfs[0].join(dfs[1:], how="outer", sort=True)

        raise AttributeError
