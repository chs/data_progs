#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
File system access functions. In the future all access to logger data files
on disk should be routed through this module.

PURPOSE:
--------
The idea is to abstract the interaction with the data storage away, as this
opens up the possibility to change/optimize the storage scheme, without the
hazzle to go through every file/script in the process chain.

STATUS:
-------
At the moment the provided functionality reflects the author's needs up to
the last commit. There are most likely a number of convience functions
missing.

TODO:
-----
Some test for the most important functions are urgently needed

CHANGELOG:
----------
Written: David Schaefer, Feb 2017
"""

from collections import OrderedDict

import pandas as pd

from config.uri import STATIONSFILE, DEVICESSFILE, DMP_CREDENTIALS
from .tools import _tupelize
from .devices import (
    SoilnetDevice,
    SoilnetICOSDevice,
    SoilnetV3Device,
    MeteoDevice,
    PhenoDevice,
    EddyDevice,
    EddyICOSDevice,
    EddyBCDevice,
    SoilrespirationDevice,
    Device,
)


def _readStationsFile():
    stations = pd.read_csv(STATIONSFILE, na_filter=False, comment="#")
    try:
        dmp_creds = pd.read_csv(DMP_CREDENTIALS, na_filter=False, comment="#")
        stations = stations.merge(dmp_creds)
    except FileNotFoundError:
        pass
    return stations


def _readPathFile(fields=None):
    stations = _readStationsFile()
    devices = pd.read_csv(DEVICESSFILE, na_filter=False, comment="#")
    data = pd.merge(stations, devices, on="station_key", how="inner").drop_duplicates()
    if fields is None:
        return data
    return data[fields]


def _createDevice(**kwargs):
    if kwargs["data_tag"] == "soilnet":
        return SoilnetDevice(**kwargs)
    elif kwargs["data_tag"] == "soilnet_icos":
        return SoilnetICOSDevice(**kwargs)
    elif kwargs["data_tag"] == "soilnet_V3":
        return SoilnetV3Device(**kwargs)
    elif kwargs["data_tag"] == "meteo":
        return MeteoDevice(**kwargs)
    elif kwargs["data_tag"] == "pheno":
        return PhenoDevice(**kwargs)
    elif kwargs["data_tag"] == "eddy":
        return EddyDevice(**kwargs)
    elif kwargs["data_tag"] == "eddy_icos":
        return EddyICOSDevice(**kwargs)
    elif kwargs["data_tag"] == "eddy_bc":
        return EddyBCDevice(**kwargs)
    elif kwargs["data_tag"] == "soil_respiration":
        return SoilrespirationDevice(**kwargs)
    else:
        return Device(**kwargs)


def _filterDevices(devices, func):
    seen = set()
    for device in devices:
        key = func(device)
        if key not in seen:
            seen.add(key)
            yield device


def _groupDevices(devices, keyfunc):
    out = OrderedDict()
    for device in devices:
        key = keyfunc(device)
        if key not in out:
            out[key] = []
        out[key].append(device)
    return out


def _filterPathFile(station=None, logger=None, tag=None):
    stations = _tupelize(station) if station else None
    tags = _tupelize(tag) if tag else None
    loggers = _tupelize(logger) if logger else None

    data = _readPathFile()

    if tags:
        data = data[data["data_tag"].isin(tags)]
    if stations:
        data = data[data["station_key"].isin(stations)]
    if loggers:
        data = data[data["logger_key"].isin(loggers)]

    return data


def getStations(station=None, logger=None, tag=None, start_date=None, end_date=None):
    from .station import Station

    data = _filterPathFile(station, logger, tag)
    for _, df in data.groupby(by="station_key"):
        yield Station(df, start_date=start_date, end_date=end_date)


def getDevices(
    station=None,
    logger=None,
    start_date=None,
    end_date=None,
    tag=None,
    filter=None,
    groupby=None,
):
    data = _filterPathFile(station, logger, tag)
    devices = (
        _createDevice(start_date=start_date, end_date=end_date, **dict(row))
        for i, row in data.iterrows()
    )

    if filter is not None:
        return _filterDevices(devices, filter)
    if groupby is not None:
        return _groupDevices(devices, groupby)

    return devices


def getDevice(*args, **kwargs):
    devices = tuple(getDevices(*args, **kwargs))
    if len(devices) > 1:
        raise RuntimeError("ambigous device defintion")
    if len(devices) == 0:
        raise RuntimeError("no such device")
    return devices[0]
