#! /usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
from datetime import datetime
from contextlib import contextmanager
from config.email import SENDER, RECIPIENTS, CREDENTIALS
from config.uri import LOGFILE
import logging
import logging.config


class MySMTPHandler(logging.handlers.SMTPHandler):
    def __init__(self, *args, **kwargs):
        super(MySMTPHandler, self).__init__(*args, **kwargs)

    def format(self, record):
        msg = super(MySMTPHandler, self).format(record)
        content = getattr(record, "content", None)
        if content:
            msg = msg + "\n" + content
        return msg

    def getSubject(self, record):
        return "[DATA-PROGS][{:}][{:}][{:}]".format(
            logging.getLevelName(record.levelno), record.name, record.msg
        )


log_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "standard": {"format": "[%(asctime)s][%(name)s][%(levelname)s][%(message)s]"}
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "standard",
        },
        "file": {
            "formatter": "standard",
            "class": "logging.handlers.TimedRotatingFileHandler",
            "when": "midnight",
            "backupCount": 14,
            "filename": LOGFILE,
            "level": "INFO",
        },
        "email": {
            "()": MySMTPHandler,
            "level": "ERROR",
            "mailhost": ("smtp.ufz.de", 587),
            "fromaddr": SENDER,
            "toaddrs": RECIPIENTS,
            "subject": "DATA-PROGS: Error",
            "credentials": CREDENTIALS,
            "secure": (),
        },
    },
    "loggers": {
        "": {
            # "handlers": ["console", "file", "email"],
            "handlers": ["console", "email"],
            "level": "INFO",
            "propagate": True,
        },
    },
}
logging.config.dictConfig(log_config)


def getHandler(logger, handler_class):
    handlers = logger.handlers + logger.parent.handlers
    for h in handlers:
        if isinstance(h, handler_class):
            return h


def removeHandler(logger, handler_class):
    handler = getHandler(logger, handler_class)
    logger.removeHandler(handler)
    logger.parent.removeHandler(handler)


def getLogger(name, debug=False, email=False):
    logger = logging.getLogger(name)
    if debug:
        removeHandler(logger, MySMTPHandler)
        removeHandler(logger, logging.handlers.TimedRotatingFileHandler)
        logger.setLevel(logging.DEBUG)
    if email:
        handler = getHandler(logger, MySMTPHandler)
        handler.setLevel(logging.INFO)

    return logger


@contextmanager
def exceptionLogged(logger, msg="Unexpected Error", fail=False):
    try:
        yield
    except (KeyboardInterrupt, SystemExit):
        raise
    except:
        logger.exception(msg)
        if fail:
            raise


@contextmanager
def initLogger(progname, *args, **kwargs):
    t0 = datetime.now()
    logger = getLogger(Path(progname).stem, *args, **kwargs)
    logger.info("starting...")
    yield logger
    logger.info("...finished (elapsed time: %s)", datetime.now() - t0)
