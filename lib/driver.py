#! /usr/bin/env python
# -*- coding: utf-8 -*-

from argparse import Namespace
from logging import Logger
from typing import Callable

from lib.devices import Device
from lib.logger import initLogger, exceptionLogged
from lib.faccess import getDevices


def mapDevices(
    name: str,
    clargs: Namespace,
    func: Callable[[Logger, Device], None],
    filter_kwargs=None,
    **kwargs,
) -> None:
    filter_kwargs = filter_kwargs or {}
    with initLogger(name, clargs.debug) as logger:
        args = {
            "station": clargs.station,
            "logger": clargs.device,
            "start_date": clargs.start_date,
            "end_date": clargs.end_date,
            "tag": clargs.tag,
            **filter_kwargs,
        }
        devices = getDevices(**args)
        for device in devices:
            msg = f"{device}: failed"
            with exceptionLogged(logger, msg, fail=clargs.fail):
                func(logger, device, **kwargs)
