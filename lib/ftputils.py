#! /usr/bin/env python
# -*- coding: utf-8 -*-

import stat
from datetime import datetime
from contextlib import contextmanager
from pathlib import Path

import paramiko

from .tools import _tupelize
from config.uri import KEYFILE


@contextmanager
def sftpConnection(root, login):
    transport = paramiko.Transport((root, 22))
    key = paramiko.rsakey.RSAKey(filename=KEYFILE)
    transport.connect(username=login, pkey=key)
    channel = transport.open_session()
    channel.invoke_subsystem("sftp")
    client = paramiko.sftp_client.SFTPClient(channel)

    yield client

    client.close()
    channel.close()
    transport.close()


@contextmanager
def remoteDirectorySFTP(sftp, path):
    pwd = sftp.getcwd()
    sftp.chdir(path)
    yield
    sftp.chdir(pwd)


def getFileListSFTP(sftp, fpattern="*", size=False, mdate=False, recurse=False):
    pwd = sftp.getcwd()
    out = []
    fpattern = _tupelize(fpattern)
    for fname in sftp.listdir():
        fileattr = sftp.stat(fname)

        if recurse and stat.S_ISDIR(fileattr.st_mode):
            with remoteDirectorySFTP(sftp, fname):
                out.extend(getFileListSFTP(sftp, fpattern, size, mdate, recurse))
            continue

        for pat in fpattern:
            if Path(fname).match(pat):
                # pattern does match, that's what we are looking for
                out.append([Path(pwd, fname).as_posix()])
                if size:
                    out[-1].append(fileattr.st_size)
                if mdate:
                    out[-1].append(datetime.fromtimestamp(fileattr.st_mtime))
                break

    return out
