#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re
from collections import OrderedDict
from typing import Sequence, Union
import numpy as np
import pandas as pd


def setIndexLevel(
    df: pd.DataFrame, level: Union[int, str] = 0, axis: int = 1
) -> pd.DataFrame:
    attr = "index" if axis == 0 else "columns"
    setattr(df, attr, getattr(df, attr).get_level_values(level))
    return df


def filterBy(
    df: pd.DataFrame,
    level: Union[int, str],
    regex: str,
    into_level: Union[int, str, None] = None,
) -> pd.DataFrame:
    cols = df.columns.get_level_values(level)
    idx = [i for i, col in enumerate(cols) if re.search(regex, col)]

    if into_level is not None:
        cols = df.columns.get_level_values(into_level)
        tmp = df.iloc[:, idx]
        selcols = tmp.columns.get_level_values(into_level)
        idx = [i for i, col in enumerate(cols) if col in selcols]

    return df.iloc[:, idx]


def pivotTable(df, columns, values):
    # NOTE: a poor man's pivot_table, only faster and less memory hungry...
    groups = df.groupby(by=columns)
    out = pd.DataFrame()
    for k, v in groups:
        col = v[values]
        out[k] = col[~col.index.duplicated()]
    return out


def getConsecutiveIndices(df: pd.DataFrame, consval: float) -> Sequence[pd.DataFrame]:
    idx = np.where((df.index[1:] - df.index[:-1]) > consval)[0]
    return np.split(df, idx + 1)


def concatOrdered(dfs: Sequence[pd.DataFrame]) -> pd.DataFrame:
    """
    concatenate dataframes while preserving the original
    column order
    """

    columns = []
    for df in dfs:
        columns.extend(df.columns.tolist())
    columns = list(OrderedDict.fromkeys(columns))
    data = pd.concat(dfs, sort=True)[columns]
    return data


def inferFrequency(df: pd.DataFrame):
    freq = df.index.freq

    if not freq:
        try:
            freq = pd.infer_freq(df.index)

        except ValueError:
            # if less then 3 items in df

            pass

    if not freq:
        freq = df.index.to_series().diff().min()

    return freq
