#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Helper functions
"""

import os
import shutil
import numbers
from calendar import monthrange
from datetime import datetime, timedelta, date
from pathlib import Path
from io import StringIO
from contextlib import contextmanager

from typing import Union
import numpy as np
import pandas as pd
import joblib

from config.uri import CACHEPATH


def getCache():
    p = Path(CACHEPATH)
    t0 = datetime.combine(date.today(), datetime.min.time())
    if p.exists():
        cdate = datetime.fromtimestamp(p.stat().st_ctime)
        if cdate < t0:
            shutil.rmtree(CACHEPATH)
    return joblib.Memory(CACHEPATH, verbose=0).cache


@contextmanager
def timedExecution():
    t0 = datetime.now()
    yield
    print("timed elapsed: {:}".format(datetime.now() - t0))


def _numpyfy(
    arg: Union[pd.DataFrame, pd.Series, np.array, numbers.Number]
) -> np.ndarray:
    try:
        # pandas dataframe
        return arg.values
    except AttributeError:
        try:
            # numpy array
            return arg.copy()
        except AttributeError:
            # scalar
            return np.atleast_1d(arg)


@contextmanager
def changeDirectory(path):
    """Temporarily change working directory to 'path'"""
    pwd = os.getcwd()
    os.chdir(str(path))
    yield
    os.chdir(pwd)


def _stringFname(fname):
    try:
        return fname.name
    except AttributeError:
        return str(Path(fname).name)


def _stringPname(fname):
    try:
        return fname.path
    except AttributeError:
        return str(Path(fname))


def _tupelize(arg):
    out = arg
    if isinstance(out, (str, StringIO)):
        out = (out,)
    if not isinstance(out, tuple):
        try:
            out = tuple(out)
        except TypeError:
            out = (out,)

    return out


def firstOfDay(date):
    return pd.Timestamp(toDate(date)).replace(minute=10)


def lastOfDay(date):
    return pd.Timestamp(toDate(date)).replace(minute=0) + timedelta(days=1)


def firstOfMonth(date):
    return pd.Timestamp(toDate(date)).replace(day=1, minute=10)


def lastOfMonth(date):
    ndays = monthrange(date.year, date.month)[-1]
    delta = timedelta(days=ndays)
    return pd.Timestamp(toDate(date)).replace(day=1, minute=0) + delta


def firstOfYear(date):
    return pd.Timestamp(toDate(date)).replace(month=1, day=1, minute=10)


def lastOfYear(date):
    return pd.Timestamp(toDate(date)).replace(
        year=date.year + 1, month=1, day=1, minute=0
    )


def toDate(date):
    # type (Union[pd.Timestamp, datetime.datetime, datetime.date, str]) -> datetime.date
    try:
        return date.date()
    except AttributeError:
        try:
            return pd.Timestamp(date).date()
        except AttributeError:
            return date


def createPath(fname):
    """make the path to fname, if it not already exists"""
    fname = Path(fname)
    if fname.suffix:
        # we got a file name, assume that we need to build its parent directory
        fname = fname.parent
    fname.mkdir(parents=True, exist_ok=True)
