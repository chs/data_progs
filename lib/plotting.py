#! /usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib import gridspec
from .flagging import getFlags, getMaxflags
from .pdutils import getConsecutiveIndices


SOILNETPLOTDATA = [
    ("Spade 1", "red"),
    ("Spade 2", "blue"),
    ("Spade 3", "green"),
    ("Spade 4", "purple"),
    ("Spade 5", "orange"),
    ("Spade 6", "gold"),
    ("Spade 7", "grey"),
]


# NOTE: check change - should be done
# def plotLine(ax, data,
#              color="blue", linestyle="-",
#              marker=None, interval=TIMESTEP):
def plotLine(ax, data, interval=None, color="blue", linestyle="-", marker=None):
    interval = (
        interval
        or data.index.freq
        or data.index.to_series().diff().value_counts().index[0]
    )
    parts = tuple(getConsecutiveIndices(data.dropna(), interval))

    lines = []
    markers = []
    for part in parts:
        if len(part) <= 5:
            markers.append(part)
        else:
            lines.append(part)

    if lines:
        lines = pd.concat(lines).reindex(data.index)
        ax.plot(lines, color=color, linestyle=linestyle)

    if markers:
        markers = pd.concat(markers).reindex(data.index)
        ax.plot(markers, color=color, marker=marker)


def plotData(ax, data, flags, interval, color="black"):
    # Purpose:
    #    plot the given data and flags
    # NOTE:
    # The column might names differ ('_f' vs unit) and idexing with
    # incompatable names yields an all-NaN Frame. So use the values instead
    idx = flags.notnull().values
    data = data[idx]
    flags = flags[idx]
    flag_plot_data = iterFlagPlots(data, flags)
    legend = []

    for flgs, clr, line, marker, label, handle in flag_plot_data:
        plotLine(ax, flgs, color=clr, linestyle=line, marker=marker, interval=interval)
        legend.append([handle, label])
    plotLine(ax, removeFlaggedData(data, flags), color=color, interval=interval)
    return zip(*legend)


def appendSubplot(fig):
    nplots = len(fig.axes)
    gs = gridspec.GridSpec(nplots + 1, 1)

    # reposition exiting subplots
    for i, ax in enumerate(fig.axes):
        ax.set_position(gs[i].get_position(fig))
        ax.set_subplotspec(gs[i])

    # add new suplots
    ax = fig.add_subplot(gs[-1])
    return ax


def configAxes(ax, label, start_date, end_date):
    nplots = len(ax.get_figure().axes)
    position = "left" if nplots % 2 == 1 else "right"
    ax.set_ylabel(label)
    ax.xaxis_date()
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%d.%m"))
    ax.yaxis.set_label_position(position)
    ax.yaxis.set_ticks_position(position)
    ax.set_xlim(left=start_date, right=end_date)


def removeFlaggedData(data, flags, nodata=np.nan, inplace=True):
    # Purpose:
    #     Replace all flagged data points with nodata argument

    flags = flags.values

    flag_options = (
        getMaxflags(flags, exclude=(0, 1, 2)),
        getFlags(flags, 2),
        getFlags(flags, 1),
    )

    flag_values = (1, 2)

    # build up an index mask
    mask = np.zeros_like(data.values, dtype=bool)
    for flag_option in flag_options:
        for flag_value in flag_values:
            mask |= flag_option == flag_value

    out = data.copy(deep=(not inplace))
    out.iloc[mask] = nodata
    return out


def iterFlagPlots(data, flags):
    # Purpose:
    #    plot all needed flags uniformely
    #    (i.e. values, color, legend, line-style, marjer)
    tmp1 = (
        (getMaxflags(flags.values, exclude=(0, 1, 2)), "green", "auto flag"),
        (getFlags(flags.values, 2), "red", "man flag"),
        (getFlags(flags.values, 1), "gold", "maintenance flag"),
    )

    tmp2 = [
        (1, "--", "o"),
        (2, ":", "^"),
    ]

    for flagvals, color, label in tmp1:
        for flagval, linestyle, marker in tmp2:
            flgs = data.copy()
            flgs[flagvals != flagval] = np.nan

            handle = getLegendHandle(color=color, linestyle=linestyle, marker=marker)

            llabel = "{:} {:}".format(label, flagval)
            yield flgs, color, linestyle, marker, llabel, handle


def getLegendHandle(color=None, linestyle=None, marker=None):
    return plt.Line2D((0, 1), (0, 0), color=color, linestyle=linestyle, marker=marker)


def htmlWrapper(plot_fname):
    return "\n".join(
        [
            '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">',
            "<html>",
            "<head>",
            "<title>Current Level1 Data</title>",
            "</head>",
            "<body>",
            '<p><img src="{:}"/></p>'.format(plot_fname),
            "</body>",
            "</html>",
        ]
    )
