#!/usr/bin/env python

from datetime import datetime
from collections import OrderedDict
from functools import reduce
from pathlib import Path

import numpy as np
import pandas as pd

from config.data import NODATA

from lib.faccess import getStations
from lib.daccess import iterDataFreq
from lib.daccess import ConfigFields as CF
from lib.flagging import getMaxflags
from lib.logger import initLogger, exceptionLogged
from lib.tools import firstOfYear, lastOfYear


OUTPATH = "Meteo_Files"

UNITS_MAP = {"W/m**2": "W+1m-2", "umol/m**2 s": "umol+1m-2s-1", "decC": "C"}


def readConfig(station):
    config = station.readExcel()

    if CF.EDDYPRO not in config.columns:
        return

    config = config.dropna(axis=0, subset=[CF.EDDYPRO])
    config[CF.UNIT] = config[CF.UNIT].replace(UNITS_MAP)
    config[CF.ENDDATE] = config[CF.ENDDATE].fillna(datetime.today())

    config = config[
        [CF.VARNAME, CF.EDDYPRO, CF.UNIT, CF.DEVICE, CF.STARTDATE, CF.ENDDATE]
    ]
    return config


def getData(logger, station):
    config = readConfig(station).sort_values(by=CF.STARTDATE)

    dfs = OrderedDict()

    for devname, devconfig in config.groupby(by=CF.DEVICE):
        logger.debug(f"reading: {devname}")
        data = station.getDevice(devname).getL2Data(
            fill=True,
            reindex=True,
            nbits=32,
            start_date=firstOfYear(station.start_date),
            end_date=lastOfYear(station.end_date),
        )

        for _, (headname, outname, unit, _, sdate, edate) in devconfig.iterrows():
            tmp = data.loc[sdate:edate, headname]
            idx = (getMaxflags(tmp["flag"].values) == 2).squeeze()
            tmpdata = tmp["data"].copy()
            tmpdata.loc[idx] = np.nan

            tmpdata.columns = pd.MultiIndex.from_tuples(
                [(outname, unit)], names=("varname", "units")
            )

            try:
                dfs[outname] = pd.concat([dfs[outname], tmpdata], axis=0)
            except KeyError:
                dfs[outname] = tmpdata

    merged = reduce(lambda x, y: x.join(y, how="outer"), dfs.values())
    cleaned = merged[~merged.index.duplicated(keep="last")]
    return cleaned


def writeData(logger, station, df):
    for date, data in iterDataFreq(df, freq="yearly", drop_empty=False):
        out = (
            data.fillna(NODATA)
            .reset_index()
            .rename(columns={"index": "Timestamp_1", "": "yyyy-mm-dd HHMM"})
        )

        outpath = Path(
            station.path, OUTPATH, "{:}_meteo_{:}.csv".format(station.name, date.year)
        )
        logger.debug("writing: %s", outpath)
        out.to_csv(str(outpath), sep=",", date_format="%Y-%m-%d %H%M", index=False)


def main(station=None, device=None, start_date=None, end_date=None, debug=False):
    with initLogger(__file__, debug) as log:
        stations = getStations(station=station, logger=device, tag="meteo")

        for station in stations:
            log.info("processing: %s", station)
            msg = "failed to write meteo file for: {:}".format(station)
            with exceptionLogged(log, msg, fail=debug):
                log.debug("reading")
                data = getData(log, station)
                log.debug("processing")
                shf = data.filter(regex=r"SHF.*")
                out = data.drop(shf.columns, axis=1)
                out[("SHF_1_1_1", "W+1m-2")] = shf.mean(axis=1, skipna=True)
                log.debug("reading")
                writeData(log, station, out)


if __name__ == "__main__":
    from lib.argparser import parseArguments

    args = parseArguments("Write meteo files.", {"ndays": 1})
    main(
        station=args.station,
        device=args.device,
        start_date=args.start_date,
        end_date=args.end_date,
        debug=args.debug,
    )
