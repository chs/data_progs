import numpy as np
import pandas as pd
import re
import os
import glob
import math
from pathlib import Path

from config.uri import ROOT, CACHEPATH
from config.data import NODATA, MAXDATE, MINDATE
from lib.faccess import getDevices
from lib.daccess import splitTable, iterDataFreq, ConfigFields as F, IndexFields as I
from lib.argparser import parseArguments
from lib.logger import initLogger, exceptionLogged
from lib.tools import firstOfYear, lastOfYear

pd.options.mode.chained_assignment = None

# constants
RGAS = 8.31451  # Universal gas constant [J K-1 mol-1 or m3 Pa K-1 mol-1]
MW = 0.0180153  # Molar mass of H2O [kg mol-1] (18.01528 g mol-1 )
RD = 287.0586  # Gas constant for dry air (Rgas/Md) [J kg-1 K-1]
MD = 0.0289645  # Molar mass of dry air [kg mol-1] (28.9647 g mol-1)
RV = 461.5  # Gas constant for water vapour [J kg-1 K-1]
DT = 1800  # second in 30 min

STOR_PROC_PATH = "HohesHolz/derived/storage_fluxes/processed_data"
STOR_CAL_PATH = "HohesHolz/derived/storage_fluxes/stor_cal"


def filter_flag(data, flags):
    data = data.replace(NODATA, np.nan)
    mask = (flags == 9) | (flags == 91)
    mask.columns = data.columns
    return data[mask]


def partion_data(data: pd.DataFrame):
    ta = data.filter(regex=".*TA", axis=1)
    rh = data.filter(regex=".*RH", axis=1)
    pa = data.filter(regex=".*PA", axis=1)
    co2 = data.filter(regex=".*CO2", axis=1)
    h2o = data.filter(regex=".*H2O", axis=1)
    return ta, rh, pa, co2, h2o


def process_weighted_high(df, dz):
    df_used_col = list(set(df.columns).intersection(dz.index))
    dz_df = (
        dz.loc[df_used_col]
        .sort_values(by="height (m)")
        .rolling(2)
        .sum()
        .shift(periods=-1)
        / 2
    )
    dz_df.iloc[-1] = dz.loc[df_used_col].max()
    height_df = pd.DataFrame({"height (m)": 0}, index=[0])
    dz_df = pd.concat([height_df, dz_df])
    dz_df = dz_df.diff().iloc[1:]
    # replace is used, because .dot would cancel the whole line
    df_avg = df[df_used_col].replace(np.nan, 0).dot(dz_df) / dz_df.sum()
    # replace is again used for the case, when the line above produces a 0
    return df_avg.replace(0, np.nan)


def process_barometric_pressure(ta, rh, pa, dz):
    ta_avg = process_weighted_high(ta, dz)

    rh_avg = process_weighted_high(rh, dz)

    pa /= 10  # convert from hPa to kPa
    pa_avg = process_weighted_high(pa, dz)

    # H2O molar fraction - resulting from hygrometers data
    h2o_mf_avg = (
        6088.484
        * math.e ** (ta_avg / ((ta_avg + 273.15) * 17.31303))
        / 18.0153
        * rh_avg
    ) / (pa_avg / (RGAS * (ta_avg + 273.15)))
    h2o_mf_avg /= 100000  # manual factor added to scale
    bm_data_avg = pd.concat([ta_avg, rh_avg, pa_avg, h2o_mf_avg], axis=1)
    bm_data_avg.columns = ["TA", "RH", "PA", "H2O_MF_meteo"]
    return bm_data_avg


def process_rho(bbm_data_avg):
    # Air molar volume [m3 mol-1] (should be around 0.025 m3 mol-1 at 25 °C)
    amv = RGAS * (bbm_data_avg["TA"] + 273.15) / (bbm_data_avg["PA"] * 10**3)

    # Water vapor mass density [kg m-3] (e.g. range: 0.005 kgm-3 at 0 °C  - 0.051 kg m-3 at 40 °C)
    rho_h2o = (bbm_data_avg["H2O_MF_meteo"] * 10**-3) * MW / amv

    # Water vapor partial pressure [kPa] (should be around 2.338 kPa at 20 °C)
    e_avg = rho_h2o * RV * 10**-3 * (bbm_data_avg["TA"] + 273.15)
    e_avg /= 10  # manual factor added to scale

    # Dry air partial pressure [kPa]
    pa_avg_d = bbm_data_avg["PA"] - e_avg

    # Average dry air molar density[mol m-3]
    rho = (pa_avg_d * 10**3) / (RD * (bbm_data_avg["TA"] + 273.15) * MD)
    return rho


def process_gas_kinematic_flux(df, dz):
    # Kinematic SC flux
    df_used_col = list(set(df.columns).intersection(dz.index))
    dz_df = (
        dz.loc[df_used_col]
        .sort_values(by="height (m)")
        .rolling(2)
        .sum()
        .shift(periods=-1)
        / 2
    )
    dz_df.iloc[-1] = dz.loc[df_used_col].max()
    height_df = pd.DataFrame({"height (m)": 0}, index=[0])
    dz_df = pd.concat([height_df, dz_df])
    dz_df = dz_df.diff().iloc[1:]

    df.dropna(how="all", axis=1)
    df = df.loc[:, ~df.columns.duplicated()]

    # it is not known, why divide by Dt (1800sec = 30min)
    return df[df_used_col].diff().replace(np.nan, 0).dot(dz_df) / DT


def process_seq(co2, h2o, bm_data_avg, dz, rho):
    # Specific evaporation heat (lambda) [J kg-1]
    seh = 10**3 * (3147.5 - 2.37 * (bm_data_avg["TA"] + 273.15))
    sc_gas_k = process_gas_kinematic_flux(h2o, dz)

    sc_gas_h2o = (rho * seh).to_frame(name="height (m)") * MW * 10**-3 * sc_gas_k
    sc_gas_h2o.columns = ["stor_H2O [umol/m2 s]"]

    sc_gas_co2 = rho.to_frame(name="height (m)") * sc_gas_k
    sc_gas_co2.columns = ["stor_CO2 [umol/m2 s]"]
    return sc_gas_h2o, sc_gas_co2


def process_h(ta, bm_data_avg, dz, rho):
    # convert TA to K
    ta += 273.15

    # Dry air heat capacity at constant pressure (cp) [J kg-1 K-1]
    cp = 1005 + ((bm_data_avg["TA"] + 23.12) ** 2) / 3364

    # KINEMATIC STORAGE TERM
    ta_used_col = list(set(ta.columns).intersection(dz.index))
    dz_ta = (
        dz.loc[ta_used_col]
        .sort_values(by="height (m)")
        .rolling(2)
        .sum()
        .shift(periods=-1)
        / 2
    )
    dz_ta.iloc[-1] = dz.loc[ta_used_col].max()
    height_df = pd.DataFrame({"height (m)": 0}, index=[0])
    dz_ta = pd.concat([height_df, dz_ta])
    dz_ta = dz_ta.diff().iloc[1:]
    sch_k = ta[ta_used_col].diff().replace(np.nan, 0).dot(dz_ta) / DT

    # STORAGE TERM (mass-based quantity)
    sch = sch_k * (rho * cp).to_frame(name="height (m)")
    sch /= 42  # manual factor added to scale
    sch.columns = ["stor_heat [W/m**2]"]
    return sch


def write(logger, sc_gas_h2o, sc_gas_co2, sc_heat):
    dfs = []
    for df in [sc_gas_h2o, sc_gas_co2, sc_heat]:
        df[df.columns[0].split(" ")[0] + "_f"] = np.where(df.notnull(), 9, 92)
        dfs.append(df.replace(np.nan, NODATA))
    final = pd.concat(dfs, axis=1)
    final.columns = final.columns.set_names([I.HEADER])

    path = os.path.join(ROOT, STOR_PROC_PATH)
    for date, data in iterDataFreq(final, freq="AS"):
        fname = f"{path}/DE-HoH_SC_{date.year}.csv"
        logger.debug(f"writing to: {fname}")
        data.to_csv(fname)


def duplicate_nans(df):
    # in the calculation process, the first value after NaN values is not useable, here it will be overwritten
    df[df.isna().shift().fillna(False)] = np.nan
    return df


def read_data(device):
    config = device.readExcel()
    config = config[[F.HEADNAME, F.STORAGE]]
    config = config.set_index(F.HEADNAME)

    storage_names_dict = config.dropna()[F.STORAGE].to_dict()
    df = device.readDerived(tag="aggregate-30min")

    df.columns = df.columns.set_names([I.HEADER])
    data, flags = splitTable(df, index_level=I.HEADER)
    data = filter_flag(data, flags)
    df = data[list(set(storage_names_dict.keys()).intersection(data.columns))].rename(
        columns=storage_names_dict
    )
    return df


def read_storage_calibration(start_date, end_date):
    path = os.path.join(ROOT, STOR_CAL_PATH)
    csv_files = glob.glob(os.path.join(path, "*.csv"))
    meta_collector_list = []
    for current_file in csv_files:
        if not "storvars" in current_file:
            continue
        date = pd.to_datetime(re.findall("[0-9]+", current_file)[0])
        meta_collector_list.append([current_file, date])
    meta_df = pd.DataFrame(meta_collector_list, columns=["fname", "start"])
    meta_df = meta_df.sort_values(by="start")
    meta_df["end"] = meta_df["start"].shift(-1) - pd.offsets.Minute()
    meta_df["end"].iloc[-1] = pd.Timestamp.now()
    return meta_df.loc[meta_df["start"].between(start_date, end_date)]


def main(station, device, start_date, end_date, debug):
    if start_date is None:
        start_date = MINDATE
    if end_date is None:
        end_date = MAXDATE
    start_date, end_date = pd.to_datetime(
        [firstOfYear(start_date), lastOfYear(end_date)]
    )
    meta_df = read_storage_calibration(start_date, end_date)

    with initLogger(__file__, debug) as logger:
        devices = getDevices(
            station=station,
            logger=device,
            tag="meteo",
            start_date=start_date,
            end_date=end_date,
        )

        with exceptionLogged(logger, fail=debug):
            data = pd.DataFrame()
            for device in devices:
                logger.debug(f"collect data: {device}")
                data = pd.concat([data, read_data(device)], axis=1)

            logger.debug(f"processing")
            data.index = pd.to_datetime(data.index)
            partions = partion_data(data)
            # If more than 1/3 of data in one level is missing, then do not compute a storage term
            ta, rh, pa, co2, h2o = [
                df.dropna(thresh=2 * df.shape[1] / 3) for df in partions
            ]

            sc_gas_h2o = pd.DataFrame()
            sc_gas_co2 = pd.DataFrame()
            sc_heat = pd.DataFrame()
            for index, (start, end, fname) in meta_df[
                ["start", "end", "fname"]
            ].iterrows():
                ta_chunk = ta.loc[start:end]
                rh_chunk = rh.loc[start:end]
                pa_chunk = pa.loc[start:end]
                co2_chunk = co2.loc[start:end]
                h2o_chunk = h2o.loc[start:end]
                if any(
                    [
                        ta_chunk.empty,
                        rh_chunk.empty,
                        pa_chunk.empty,
                        co2_chunk.empty,
                        h2o_chunk.empty,
                    ]
                ):
                    continue

                fname = Path(fname)
                dz = pd.read_csv(fname, index_col=0)
                bm_data_avg = process_barometric_pressure(
                    ta_chunk, rh_chunk, pa_chunk, dz
                )
                rho = process_rho(bm_data_avg)
                sc_gas_h2o_temp, sc_gas_co2_temp = process_seq(
                    co2_chunk, h2o_chunk, bm_data_avg, dz, rho
                )
                sc_gas_h2o = pd.concat([sc_gas_h2o, sc_gas_h2o_temp])
                sc_gas_co2 = pd.concat([sc_gas_co2, sc_gas_co2_temp])
                sc_heat_temp = process_h(ta_chunk, bm_data_avg, dz, rho)
                sc_heat = pd.concat([sc_heat, sc_heat_temp])
            sc_gas_h2o = duplicate_nans(sc_gas_h2o)
            sc_gas_co2 = duplicate_nans(sc_gas_co2)
            sc_heat = duplicate_nans(sc_heat)

            write(logger, sc_gas_h2o, sc_gas_co2, sc_heat)


if __name__ == "__main__":
    args = parseArguments("Calculate storage Flux", {"ndays": 1})

    main(
        station=args.station,
        device=args.device,
        start_date=args.start_date,
        end_date=args.end_date,
        debug=args.debug,
    )
#  python -m level1.do_calc_storage_terms --station HH --device T1 T2 T3 T4 T7 --doall
