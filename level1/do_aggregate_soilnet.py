#! /usr/bin/env python
# -*- coding: utf-8 -*-

import warnings
from pathlib import Path
from datetime import datetime

import numpy as np
import pandas as pd

from config.data import NODATA
from lib.flagging import getMaxflags, setFlags
from lib.daccess import reindexTable, writeTable, iterDataFreq, splitTable
from lib.faccess import getDevices
from lib.logger import initLogger, exceptionLogged
from lib.tools import firstOfYear, lastOfYear


def prepData(df):
    # drop unflagged columns
    _, flags = splitTable(df, index_level="varnames")
    is_flagged = ~(flags % 9 == 0).all()
    cols = is_flagged[is_flagged].index
    df = reindexTable(df)[cols].droplevel(axis="columns", level=[0, 1])
    return df


def aggregatePeriod(df):
    # Aggregation rules:
    # - remove rows with missing data
    # - remove rows with maxflag == 2
    # - more than one row remains -> max flag = aggregation flag
    # - only one row remains -> aggregation flags = 1

    if df.empty:
        return pd.Series([np.nan] * len(df.columns))

    data, flags = splitTable(df)
    data = data.values
    flags = getMaxflags(flags.values)

    missing = data == NODATA
    data[missing] = np.nan
    flags[missing] = np.nan
    data[flags == 2] = np.nan

    with warnings.catch_warnings():
        # we expect nan values and therfore warnings here
        warnings.simplefilter("ignore", category=RuntimeWarning)
        data_out = np.nanmean(data, axis=0)
        flags_max = np.nanmax(flags, axis=0)

        # only one valid value -> set flag to 1
        flags_max[np.sum(~np.isnan(data), axis=0) == 1] = 1

        flags_out = setFlags(9, 1, np.clip(flags_max, a_min=0, a_max=None))

        # all data missing -> flag is missing
        flags_out[np.all(missing, axis=0)] = NODATA
        # strip 0 flags
        flags_out[flags_out == 90] = 9

    # interweave arrays
    out = np.column_stack([data_out, flags_out]).ravel()
    return pd.Series(out)


def buildFname(device, date, freq):
    return Path(
        device.derivedpath,
        f"SoilNet_Data_{device.station_key}_{freq}_{device.logger_key}_{datetime.strftime(date, '%Y')}.level2.csv",
    )


def writeData(data, device, write_freq, period, logger):
    start_date = firstOfYear(device.start_date)
    end_date = lastOfYear(device.end_date)
    data = data.loc[(data.index >= start_date) & (data.index < end_date)]
    for date, out in iterDataFreq(data, write_freq):
        fname = buildFname(device, date, period)
        logger.debug("writing: %s", str(fname))
        writeTable(fname, out, make_path=True)


def aggregateData(df, freq="30T"):
    iii = df.copy(deep=True)
    df = df.sort_index(axis=1)
    df = prepData(df)

    groups = (
        reindexTable(df)
        .astype(float)
        .groupby(pd.Grouper(freq=freq, closed="right", label="right"))
    )

    out = groups.apply(aggregatePeriod)
    out.columns = df.columns
    return out


def main(station, device, start_date, end_date, period, debug, fail):
    with initLogger(__file__, debug) as logger:
        devices = getDevices(
            station=station,
            logger=device,
            tag=("soilnet", "soilnet_icos", "soilnet_V3"),
            start_date=start_date,
            end_date=end_date,
        )

        for device in devices:
            msg = "soilnet aggregation failed for: {:}".format(device)
            with exceptionLogged(logger, msg, fail=fail):
                data = device.getL2Data()
                if data.empty:
                    continue
                logger.info("aggregating: %s", device)
                agg_data = aggregateData(data, period)
                writeData(
                    agg_data, device, write_freq="yearly", period=period, logger=logger
                )


if __name__ == "__main__":
    from lib.argparser import ChainParser

    parser = ChainParser("Aggregate soilnet data to 30 minutes", {"ndays": 1})
    parser.add_argument(
        "--freq", type=str, help="target time frequency as a pandas offset string"
    )
    args = parser.parseArgs()

    main(
        station=args.station,
        device=args.device,
        start_date=args.start_date,
        end_date=args.end_date,
        period=args.freq,
        debug=args.debug,
        fail=args.fail,
    )
