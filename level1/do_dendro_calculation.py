import numpy as np
import pandas as pd
import re
import math

from pathlib import Path
from itertools import zip_longest

from config.uri import ROOT
from config.data import NODATA
from lib.faccess import getDevices
from lib.daccess import splitTable
from lib.logger import initLogger, exceptionLogged
from lib.flagging import getMaxflags


def filter(data, flags):
    # the filter function mask the data, leaving only useable data (not flagged)
    # and choose only the dendro columns
    max_flags = getMaxflags(flags)
    mask = max_flags == 0
    mask = pd.DataFrame(mask, columns=data.columns, index=data.index)
    data = data[mask]
    return data[data.columns[data.columns.str.match(".*dendro(?!.*r)")]]


def grouper(df, n=3, fillvalue=None):
    """
    This grouper function splits a pd.DataFrame into pd.DataFrames with `n` columns.

    Background: Every dendrometer (i.e one tree) is represented by three columns:
    1. calculated value
    2. manually read value
    3. quality flag
    """
    args = [iter(df)] * n
    return zip_longest(fillvalue=fillvalue, *args)


def fillNa(df):
    for bhd_value, bhd_manu, flag in grouper(df):
        # setting 91 flag on all values, where calculated value is nan
        df.loc[df[bhd_value].isna(), flag] = 91
        df[bhd_value] = df[bhd_value].fillna(df[bhd_manu])
        # setting 92 flag on all values, where interpolated vales is nan
        df.loc[df[bhd_manu].isna(), flag] = 92
        df[bhd_value] = df[bhd_value].fillna(NODATA)
        df[bhd_manu] = df[bhd_manu].fillna(NODATA)
    return df


def dendroCalculation(df, dendro, first_value):
    try:
        first_usable_dendro_value = df[dendro][df[dendro].first_valid_index()]
    except KeyError:
        first_usable_dendro_value = first_value
    df[dendro][0] = first_usable_dendro_value

    # df has the columns: measured values | d_ini | bhd
    # do the dropna here, cause the dataframe is now separated in the different dendros and mixed with the manual values
    df = df.dropna(subset=[dendro])

    # from um to mm
    df[dendro] = df[dendro] / 1000

    df.insert(0, "rad", df["dbh"][0] * 10 / 2)
    dini = df["d_ini"][0] * 10
    r0 = df["rad"][0]
    c0 = math.pi * 2 * r0
    v0 = df[dendro][0]
    cwire = (
        c0
        + 2 * ((dini + r0 - v0) ** 2 - r0**2) ** 0.5
        - 2 * r0 * math.acos(r0 / (dini + r0 - v0))
    )
    df["rad_2"] = df["rad"].shift(1)
    df["rad"] = (
        cwire
        - 2 * ((dini + df["rad_2"] - df[dendro]) ** 2 - df["rad_2"] ** 2) ** 0.5
        + 2 * df["rad_2"] * np.arccos(df["rad_2"] / (dini + df["rad_2"] - df[dendro]))
    ) / math.pi
    return df["rad"]


def dendroInterpolation(df, dendro):
    df_reindexed = df.reindex(
        pd.date_range(start=df.index.min(), end=df.index.max(), freq="10min")
    )
    df_reindexed = df_reindexed.interpolate(method="linear")
    df_reindexed_regex = r"[0-9]+"
    df_reindexed.name = (
        f"bhd_manu_{int(re.findall(df_reindexed_regex, dendro)[0]):03d} [mm]"
    )
    return df_reindexed


def dendroProcess(data, manflags):
    start_date, end_date = data.index.min(), data.index.max()

    final = []
    for key, values in data.items():
        values = values.dropna()
        if values.empty:
            continue

        if key not in manflags.index:
            continue

        dropped_manflags = manflags.loc[key].dropna(how="all")
        dropped_manflags.index = dropped_manflags.index.ceil("10min")

        concat_df = pd.concat([values, dropped_manflags], axis=1)
        concat_df = concat_df.loc[start_date:end_date]

        first_value = values.iloc[0]
        start_idx = concat_df["d_ini"].dropna().index
        stop_idx = start_idx[1:].tolist() + [pd.Timestamp.today()]
        # some old data doesn't have useable manflags, this should be filtered here
        if start_idx.empty:
            continue

        temporary = []
        for start, stop in zip(start_idx, stop_idx):
            chunk = concat_df.loc[start:stop][:-1]
            if chunk.empty:
                continue
            calc = dendroCalculation(chunk, key, first_value)
            temporary.append(calc)
        # some old data has values, but manflags starts, when tree is fallen, all chunks are empty and the temporary
        # list is also empty
        if not temporary:
            continue

        name = f"{int(key.split('_')[1])}"

        # data column
        temporary_series = pd.concat(temporary, axis=0).rename(
            f"bhd_auto_{int(name):03d} [mm]"
        )
        final.append(temporary_series)

        # interpolation column
        df = manflags["dbh"].loc[key]
        df_reindexed = dendroInterpolation(df, key)
        final.append(df_reindexed * 10)

        # flags column
        flags = pd.Series(
            9, index=temporary_series.index, name=f"bhd_{int(name):03d}_f"
        )
        final.append(flags)

    final_df = pd.concat(final, axis=1)
    final_df.index.name = "Date Time"
    return final_df


def writeData(data, device):
    fname = Path(
        f"{ROOT}/HohesHolz/derived/dendrometer/",
        f"{device.station_key}_bhd_trees_10min.level2.csv",
    )

    data.to_csv(fname)
    for time, values in data.groupby(by=pd.Grouper(freq="AS", closed="right")):
        values.to_csv(
            f"{ROOT}/{device.station_path}/derived/dendrometer/{device.station_key}_bhd_trees_10min.level2_{time.year}.csv"
        )


def procDevice(logger, device, **kwargs):
    logger.info(f"processing: {device}")

    logger.debug("reading data")
    df = device.getL2Data(
        start_date=device.start_date, end_date=device.end_date, reindex=True, fill=True
    )
    manflags = device.getManualFlags()
    if df.empty or manflags.empty:
        logger.info(f"{device}: no data found for period - skipping")

    logger.debug("processing")
    data, flags = splitTable(df, index_level="varnames")
    data = filter(data, flags)
    # in case, an error in the pipeline has produced a level2 dataframe only with -9999, the device will be skipped here
    if data.isna().values.all():
        return pd.DataFrame([])

    manflags.index = manflags.index.str.split(" ").str[0]
    manflags = manflags.set_index("end", append=True)
    manflags = manflags.drop(["start", "flag", "comment"], axis=1)
    manflags = manflags.dropna(subset=["dbh", "d_ini"], how="all")
    out = dendroProcess(data, manflags)
    out = fillNa(out)
    return out


def main(station, device, start_date, end_date, debug):
    with initLogger(__file__, debug) as logger:
        devices = getDevices(
            station=station,
            logger=device,
            tag="meteo",
            start_date=start_date,
            end_date=end_date,
        )

        final_out_list = []
        for device in devices:
            with exceptionLogged(logger, f"{device}: failed", fail=debug):
                final_out_list.append(procDevice(logger, device))

        logger.debug("writing")
        final = pd.concat(final_out_list, axis=1)
        final.index.name = "Date Time"
        if final.empty:
            raise RuntimeError("no data produced")

        # corrections in index Order and not setted flags
        final = final.sort_index()
        for a, b, c in grouper(final):
            final = final.fillna({a: NODATA, b: NODATA, c: 92})

        writeData(final, device)


if __name__ == "__main__":
    from lib.argparser import parseArguments

    pd.options.mode.chained_assignment = None
    args = parseArguments("Calculate dendro data", {"ndays": 1})
    main(args.station, args.device, args.start_date, args.end_date, args.debug)
