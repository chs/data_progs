import warnings
from pathlib import Path

import pandas as pd
import numpy as np

from config.data import NODATA, MINDATE, MAXDATE
from config.uri import CACHEPATH
from lib.faccess import getDevices
from lib.daccess import splitTable, reindexTable, writeTable, iterDataFreq
from lib.tools import firstOfYear, lastOfYear
from lib.logger import initLogger, exceptionLogged
from lib.flagging import getMaxflags

from lib.daccess import ConfigFields as F

pd.options.mode.chained_assignment = None
warnings.simplefilter(action="ignore", category=FutureWarning)

AGGREGATIONS = {
    "SUM": np.nansum,
    "MEAN": np.nanmean,
    "MEDIAN": np.nanmedian,
    "MAX": np.nanmax,
}


def wind(deg, speed):
    x = NODATA
    if len(deg) == len(speed):
        u = np.sum(-np.array(speed) * np.sin(np.radians(deg)))
        v = np.sum(-np.array(speed) * np.cos(np.radians(deg)))
        if u == 0 and v == 0:
            x = 0
        elif u == 0 and v < 0:
            x = 360
        elif u == 0 and v > 0:
            x = 180
        elif u > 0:
            x = 270 - np.degrees(np.arctan(v / u))
        elif u < 0:
            x = 90 - np.degrees(np.arctan(v / u))
    return x


def aggregatePeriod(df, pflags1, pflags2, aggregations):
    # just in case there are some missing data, the Grouper produces an Error,
    # this if clause add a NODATA line in output
    if df.empty:
        out = pd.Series(NODATA, index=range(df.shape[1]))
        out[1::2] = 92
        return out

    # NOTE:
    # Not the most expressive possible version, but this.
    # is called a lot so performance matters .
    # Maybe there is a way to numba boost this?

    data, flag = splitTable(df, index_level="varnames")

    # let's work on numpy arrays.
    # make all (not 9) flags uniformly with 91 or 92
    columns = data.columns
    df_flag = getMaxflags(flag)
    df_flag_rule_1 = df_flag.copy()
    df_flag_rule_2 = df_flag.copy()
    df_input = data.to_numpy()
    mask = df_flag == 0

    # mask all flags != 9
    df_flag = np.where(df_flag == 0, df_flag, np.nan)

    # mask all flags != 91
    df_flag_rule_1 = np.where(df_flag_rule_1 == 1, df_flag_rule_1, np.nan)

    # mask all flags != 92
    df_flag_rule_2 = np.where(df_flag_rule_2 == 2, df_flag_rule_2, np.nan)

    # mask all NODATA values
    mask &= df_input != NODATA
    df_input[~mask] = np.nan

    # aggregate flags
    ps = np.isfinite(df_flag).sum(axis=0) * 100 / df_flag.shape[0]
    flags = np.full(df_flag.shape[1], 92)
    flags[ps > (100 - pflags1)] = 9
    flags[(ps <= (100 - pflags1)) & (ps > (100 - pflags2))] = 91

    # Rules to make a 91 flag in the aggregation
    # Rule 1: if all flags are 91, then the aggregated flags should also be 91
    flags[(df_flag_rule_1 == 1).all(axis=0)] = 91

    # Rule 2: if the number of 92 flags are between at least one to 1/3 of all, then the aggregated flag should be 91
    flags[
        ((df_flag_rule_2 == 2).sum(axis=0) / df_flag_rule_2.shape[0] > 0)
        & ((df_flag_rule_2 == 2).sum(axis=0) / df_flag_rule_2.shape[0] < 0.3)
    ] = 91

    # aggregate data
    agg_data = np.full(df_input.shape[1], NODATA, dtype="float64")
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        for agg in np.unique(aggregations):
            idx = aggregations == agg
            if agg == "CIRCULAR_MAX_DIST":
                for i, col in zip(np.where(idx)[0], columns[idx]):
                    wsi = columns.get_loc("WS" + col[2:])
                    agg_data[i] = wind(df_input[:, i], df_input[:, wsi])
            else:
                agg_data[idx] = AGGREGATIONS[agg](df_input[:, idx], axis=0)
    # merge data and flags back together - again, performance matters
    out = np.zeros((len(flags) + len(agg_data),))
    out[::2] = agg_data
    out[1::2] = flags

    return pd.Series(out).replace(np.nan, NODATA)


def aggregateData(df, freq, config):
    groups = (
        reindexTable(df)
        .astype(float)
        .groupby(pd.Grouper(freq=freq, closed="right", label="right"))
    )

    cols = df.columns.get_level_values("varnames").drop_duplicates()
    # insert potentially missing column labels into config
    config = config.reindex(cols)
    # prep everything we can befor the groupby apply to safe runtime
    pflags1 = config.loc[cols, F.FLAG1]
    pflags2 = config.loc[cols, F.FLAG2]
    aggregations = config.loc[cols, F.AGGREGATION_TYPE].fillna("MEAN").str.upper()
    # that's still a long runinng call
    out = groups.apply(
        aggregatePeriod, pflags1.to_numpy(), pflags2.to_numpy(), aggregations.to_numpy()
    )
    out.columns = df.columns
    return out


def writeData(logger, data, device, freq):
    cache_name = Path(
        CACHEPATH,
        f"{device.station_key}-{device.logger_key}-aggregate-{freq}-level2.parquet",
    )
    writeTable(cache_name, data, make_path=True, format="parquet")

    for date, out in iterDataFreq(data, "yearly"):
        fname = Path(
            device.derivedpath,
            freq,
            f"{device.station_key}_{device.logger_key}_{freq}_{date.year}.level2.csv",
        )
        logger.debug(f"writing to: {fname}")
        if freq == "1D":
            out.index = out.index - pd.Timedelta(days=1)
        writeTable(fname, out, make_path=True)


def procDevice(logger, device, freq, start_date, end_date):
    logger.debug("reading data")

    df = device.getL2Data(
        reindex=True, fill=True, start_date=start_date, end_date=end_date
    )
    if df.empty:
        logger.info(f"{device}: no data found for period - skipping")
        return

    config = device.readExcel()
    config = config[[F.VARNAME, F.FLAG1, F.FLAG2, F.AGGREGATION_TYPE]]
    config.set_index("headerout", inplace=True)

    logger.debug("processing")
    new = aggregateData(df, freq=freq, config=config)

    logger.debug("writing")
    writeData(logger, new, device, freq)


def main(station, device, start_date, end_date, freq, debug, fail):
    with initLogger(__file__, debug) as logger:
        devices = getDevices(
            station=station,
            logger=device,
            tag="meteo",
            start_date=start_date,
            end_date=end_date,
        )
        for device in devices:
            with exceptionLogged(logger, f"{device}: failed", fail=fail):
                start = firstOfYear(
                    pd.to_datetime([start_date, device.start_date, MINDATE]).dropna()[0]
                )
                end = lastOfYear(
                    pd.to_datetime([end_date, device.end_date, MAXDATE]).dropna()[0]
                )

                logger.info(f"processing: {device}")
                procDevice(logger, device, freq, start, end)


if __name__ == "__main__":
    from lib.argparser import ChainParser

    parser = ChainParser(
        "Aggregate logger data to a frequency of 30 minutes",
        {"ndays": 1, "freq": "30min"},
    )
    parser.add_argument(
        "--freq", type=str, help="target time frequency as a pandas offset string"
    )
    args = parser.parseArgs()

    main(
        station=args.station,
        device=args.device,
        start_date=args.start_date,
        end_date=args.end_date,
        freq=args.freq,
        debug=args.debug,
        fail=args.fail,
    )
