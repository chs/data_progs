from lib.faccess import getDevices, getStations

path = "snakemake-out"
pythonflags = "-Wignore"
toolflags = "--doall --fail"

# CLI usage: '--config stations=HH[,GB,...] devices=Box38[,Box39,...]'

def splitOption(option):
    if option is None:
        return option
    return [o.strip() for o in option.split(",")]


stations = splitOption(config.get("stations", "HH, GB, HD"))
devices = splitOption(config.get("devices", None))
tags = splitOption(config.get("tags", "meteo, soilnet, soilnet_icos, soilnet_V3"))


def getTargetFiles(suffix, stations=None, devices=None, tags=None):
    out = []
    for device in getDevices(station=stations, logger=devices, tag=tags):
        out.append(f"{path}/{device.station_key}-{device.logger_key}-{suffix}")
    return out


def getAllTargets(stations, devices):
    out = getTargetFiles("level0", stations, devices)
    out.extend(getTargetFiles("level2-local", stations, devices, tags))
    out.extend(getTargetFiles("upload", stations, devices, ("soilnet", "soilnet_icos")))
    out.extend(getTargetFiles("aggregate-30min", stations, devices, tags))
    out.extend(getTargetFiles("aggregate-1D", stations, devices, tags))
    out.extend(getTargetFiles("plot", stations, devices, tags=tags))

    if "meteo" in tags:
        for station in getStations(station=stations, tag="meteo"):
            out.append(f"{path}/{station.station_key}-meteo_files")  # aggregates files from different devices of a station

    for t in {"soilnet", "soilnet_icos", "soilnet_V3"}.intersection(tags):
        for station in getStations(station=stations, tag=t):
            out.append(f"{path}/{station.station_key}-quicklook")

    # dendro calculation
    if "HH" in stations:
        out.append(f"{path}/HH-derived-dendro")
        out.append(f"{path}/HH-derived-storage")

    if set(stations) == set(["HH", "GB", "HD"]) and "meteo" in tags:
        out.append(f"{path}/plot-intersite-7")
        out.append(f"{path}/plot-intersite-30")

    for device in getDevices(station=stations, tag=("pheno", "eddy", "eddy_icos", "eddy_bc")):
        out.append(f"{path}/{device.station_key}-{device.logger_key}-archive")

    # disk usage checks
    out.append(f"{path}/disk-usage")

    return sorted(out)


ruleorder: transformSoilnetHFREQ > transformSoilnetV3 > transformSoilnet
# ruleorder: transformSoilnetV3 > transformSoilnet
ruleorder: qualityControlHFREQ > qualityControl

wildcard_constraints:
    soilnet_key_v1="Box[0-9]+",
    soilnet_key_hfreq="Box41|Box42|Box43|Box44",
    soilnet_key_v3="Box01_V3|Box11_V3|Box50|Box51",
    soilnet_key_all="Box[0-9]+.*",
    meteo_key="|".join([d.logger_key for d in getDevices(tag="meteo", station=stations)])

onsuccess:
    shell("python {pythonflags} pipetools/sendMail.py --sender david.schaefer@ufz.de --recipient david.schaefer@ufz.de --subject '[CHS Pipeline]: FINISHED'")

onerror:
    shell("cat {log} | python {pythonflags} pipetools/sendMail.py --sender david.schaefer@ufz.de --recipient david.schaefer@ufz.de --subject '[CHS Pipeline]: FAILED' --stdin")


rule all:
    input: getAllTargets(stations=stations, devices=devices)

rule diskUsage:
    output: touch("{path}/disk-usage")
    shell: "python {pythonflags} -m tools.do_check_usage --station HH HD GB"

# Snakemake provides FTP downloads, see https://snakemake.readthedocs.io/en/stable/snakefiles/remote_files.html#file-transfer-over-ssh-sftp
rule download:
    params: station="{station_key}", device="{device_key}"
    output: touch("{path}/{station_key}-{device_key}-download")
    shell: "python {pythonflags} -m transfer_level0_level1.do_data_download --station {wildcards.station_key} --device {wildcards.device_key} {toolflags}"

rule checkDownload:
    input: "{path}/{station_key}-{device_key}-download"
    output: touch("{path}/{station_key}-{device_key}-level0")
    shell: "python {pythonflags} -m transfer_level0_level1.do_check_download --station {wildcards.station_key} --device {wildcards.device_key} {toolflags}"

rule monthlyArchives:
    input: getTargetFiles(suffix="level0", stations="{station_key}", devices="{device_key}")
    output: touch("{path}/{station_key}-{device_key}-archive")
    shell: "python {pythonflags} -m tools.do_monthly_archives --station {wildcards.station_key} --device {wildcards.device_key}"

rule transformMeteo:
    input: "{path}/{station_key}-{meteo_key}-level0"
    output: touch("{path}/{station_key}-{meteo_key}-level1")
    shell: "python {pythonflags} -m transfer_level0_level1.do_transfer_level0_level1_meteo --station {wildcards.station_key} --device {wildcards.meteo_key} {toolflags}"

rule transformSoilnet:
    input: "{path}/{station_key}-{soilnet_key_v1}-level0"
    output: touch("{path}/{station_key}-{soilnet_key_v1}-level1")
    shell: "python {pythonflags} -m transfer_level0_level1.do_transfer_level0_level1_soilnet --station {wildcards.station_key} --device {wildcards.soilnet_key_v1} {toolflags}"

rule transformSoilnetHFREQ:
    input: "{path}/HH-{soilnet_key_hfreq}-level0"
    output: touch("{path}/HH-{soilnet_key_hfreq}-level1")
    shell: "python {pythonflags} -m transfer_level0_level1.do_transfer_level0_level1_soilnet_ICOS --station HH --device {wildcards.soilnet_key_hfreq} {toolflags}"

rule transformSoilnetV3:
    input: "{path}/{station_key}-{soilnet_key_v3}-level0"
    output: touch("{path}/{station_key}-{soilnet_key_v3}-level1")
    shell: "python {pythonflags} -m transfer_level0_level1.do_transfer_level0_level1_soilnet_V3 --fail --station {wildcards.station_key} --device {wildcards.soilnet_key_v3} {toolflags}"

rule qualityControl:
    input: "{path}/{station_key}-{device_key}-level1"
    output: touch("{path}/{station_key}-{device_key}-level2")
    shell: "python {pythonflags} -m transfer_level0_level1.do_level1_flagging --station {wildcards.station_key} --device {wildcards.device_key} {toolflags}"

rule qualityControlHFREQ:
    input: "{path}/{station_key}-{soilnet_key_hfreq}-level1"
    output: touch("{path}/{station_key}-{soilnet_key_hfreq}-level2")
    # we only want one of these jobs at a time, as they are rather memomry hungry
    # add the CLI flag `--ressources icos=1`
    # we can't have more nodes running, while the HFREQ boxes are qcontroled
    threads: workflow.cores
    shell: "python {pythonflags} -m transfer_level0_level1.do_level1_flagging --station {wildcards.station_key} --device {wildcards.soilnet_key_hfreq} {toolflags}"

# both rules could be merged again
rule upload:
    input: "{path}/{station_key}-{device_key}-level2"
    output: touch("{path}/{station_key}-{device_key}-upload")
    shell: "python {pythonflags} -m tools.do_upload_dmp --station {wildcards.station_key} --device {wildcards.device_key} {toolflags} --ndays 100"

rule writeL2:
    input: "{path}/{station_key}-{device_key}-level2"
    output: touch("{path}/{station_key}-{device_key}-level2-local")
    shell: "python {pythonflags} -m tools.do_write_level2 --station {wildcards.station_key} --device {wildcards.device_key} {toolflags}"

rule aggregateSoilnet:
    input: "{path}/{station_key}-{soilnet_key_all}-level2"
    output: touch("{path}/{station_key}-{soilnet_key_all}-aggregate-{period}")
    shell: "python {pythonflags} -m level1.do_aggregate_soilnet --station {wildcards.station_key} --device {wildcards.soilnet_key_all} --freq {wildcards.period} {toolflags}"

rule aggregateMeteo:
    input: "{path}/{station_key}-{meteo_key}-level2"
    output: touch("{path}/{station_key}-{meteo_key}-aggregate-{period}")
    shell: "python {pythonflags} -m level1.do_aggregate_meteo --station {wildcards.station_key} --device {wildcards.meteo_key} --freq {wildcards.period} {toolflags}"

rule writeMeteoFiles:
    input: getTargetFiles("level2", "{station_key}")
    output: touch("{path}/{station_key}-meteo_files")
    shell: "python {pythonflags} -m level1.do_write_meteo --station {wildcards.station_key} --doall"

rule quicklook:
    input: getTargetFiles("level2", "{station_key}")
    output: touch("{path}/{station_key}-quicklook")
    shell: "python {pythonflags} -m plotting.do_plot_current_level1_soilnet --station {wildcards.station_key} --ndays 15"

rule calculateDendro:
    input:
        "{path}/HH-BC1-level2",
        "{path}/HH-BC1-level2",
        "{path}/HH-BC2-level2",
        "{path}/HH-BC3-level2",
        "{path}/HH-T5-level2",
    output: touch("{path}/{station_key}-derived-dendro")
    shell: "python {pythonflags} -m level1.do_dendro_calculation --station HH --device BC1 BC2 BC3 T5 --doall"

rule calculateStorage:
    input:
        "{path}/HH-T1-level2",
        "{path}/HH-T2-level2",
        "{path}/HH-T3-level2",
        "{path}/HH-T4-level2",
        "{path}/HH-T7-level2"
    output: touch("{path}/HH-derived-storage")
    shell: "python {pythonflags} -m level1.do_calc_storage_terms --station HH --device T1 T2 T3 T4 T7 --doall"

rule plotMeteo:
    input: "{path}/{station_key}-{meteo_key}-level2"
    output: touch("{path}/{station_key}-{meteo_key}-plot")
    shell: "python {pythonflags} -m plotting.do_plot_current_level1 --station {wildcards.station_key} --device {wildcards.meteo_key} --ndays 7"

rule plotSoilnet:
    input:
        "{path}/{station_key}-{soilnet_key_all}-level2",
        "{path}/HH-BC3-level2",
        "{path}/HH-T5-level2",
        "{path}/GB-W2-level2",
    output: touch("{path}/{station_key}-{soilnet_key_all}-plot")
    shell: "python {pythonflags} -m plotting.do_plot_level1_soilnet --station {wildcards.station_key} --device {wildcards.soilnet_key_all} {toolflags}"

rule plotIntersite:
    input: getTargetFiles("level2", tags="meteo")
    output: touch("{path}/plot-intersite-{ndays}")
    shell: "python {pythonflags} -m plotting.do_plot_current_level1_intersite --ndays {wildcards.ndays}"

