FROM python:3.8

# get and checkout saqc
RUN git clone https://git.ufz.de/rdm-software/saqc saqc && cd saqc && git reset --hard fa1612ff5530129a95eb9edce1b0ed72896bd50d && cd ..

# get and checkout the data_progs
RUN git clone https://git.ufz.de/chs/data_progs data_progs && cd data_progs && git checkout soilnet && cd ..

ENV PYTHONPATH=/saqc/:$PYTHONPATH

WORKDIR data_progs

# get and checkout the pipetools
RUN git clone https://git.ufz.de/rdm/pipelines/pipetools pipetools && cd pipetools && git reset --hard 465b5d4882fd56790d497bc3df64e4c5dabbd841 && cd ..

RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir -r /saqc/requirements.txt
RUN pip install --no-cache-dir -r /data_progs/pip-requirements.txt

# CMD snakemake -j4 --forceall --printshellcmds --keep-going
