#! /usr/bin/env python
# -*- coding: utf-8 -*-

from lib.faccess import getDevices


def main(
    station=None,
    device=None,
    start_date=None,
    end_date=None,
    tag=None,
    debug=False,
    fail=False,
):
    devices = getDevices(
        station=station,
        logger=device,
        tag=tag,
        start_date=start_date,
        end_date=end_date,
    )

    for device in devices:
        print(device.station_key, device.logger_key)


if __name__ == "__main__":
    from lib.argparser import parseArguments

    args = parseArguments(
        "print the device keys o stdout",
        {"doall": True, "tag": ("soilnet", "soilnet_icos")},
    )

    main(
        station=args.station,
        device=args.device,
        start_date=args.start_date,
        end_date=args.end_date,
        debug=args.debug,
        tag=args.tag,
    )
