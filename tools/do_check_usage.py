#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re
from io import StringIO
from datetime import date

from pathlib import Path

from lib.faccess import getStations
from lib.ftputils import sftpConnection
from lib.logger import initLogger, exceptionLogged
from config.uri import FTPROOT, USAGEPATH, ROOT


WARNLIMIT = 99


def parseLocalUsageFile():
    keymap = {"limit": "size", "usage": "used"}
    fname = Path(Path(ROOT).parent, "chs-data.txt")

    with open(str(fname), "r") as f:
        lines = f.readlines()

    out = {"unit": "GB"}
    lines = [re.split(r":\s*", l.strip()) for l in lines]
    for k, v in lines:
        m = re.match(r"[0-9\.]+", v)
        if m:
            out[keymap[k.lower()]] = float(m.group())
    out["free"] = out["size"] - out["used"]
    out["used [%]"] = round((out["used"] / out["size"]) * 100, 1)

    return out


def parseRemoteUsageFile(station):
    keymap = {"size": "size", "used": "used", "use%": "used [%]", "available": "free"}
    fname = "quota_usage"

    with sftpConnection(FTPROOT, station.sftp_user) as sftp:
        f = StringIO(sftp.open(fname).read().decode("utf-8"))

    keyline, valueline = [l.strip() for l in f.readlines()[1:]]
    keys = re.split("\s{2,}", keyline)
    values = re.split("\s{1,}", valueline)

    out = {}
    for k, v in zip(keys, values):
        k = k.strip().lower()
        m = re.match(r"[0-9\.]+", v.strip())
        if m:
            out[keymap.get(k.lower(), k)] = float(m.group())
        if v.endswith("iB"):
            out["unit"] = v[-3] + "B"
    return out


def writeLog(content, location):
    fields = ["date", "unit", "size", "free", "used", "used [%]"]
    path = Path(USAGEPATH, "usage_log_{:}.csv".format(location))
    if not path.exists():
        with open(str(path), "w") as f:
            f.write(",".join(fields) + "\n")
    content["date"] = date.today()
    with open(str(path), "a") as f:
        f.write(",".join([str(content[fld]) for fld in fields]) + "\n")


def checkUsage(content, path):
    if content["used [%]"] >= WARNLIMIT:
        raise RuntimeError(
            "Disk quota is at {:}% for {:}".format(content["used [%]"], path)
        )


def main(station=None, device=None, debug=False):
    with initLogger(__file__, debug=debug) as logger:
        for station in getStations(station=station, logger=device):
            with exceptionLogged(logger, "Disk Quota: {:}".format(station)):
                logger.info("checking: %s", station)
                content = parseRemoteUsageFile(station)
                writeLog(content, station.sftp_user)
                checkUsage(content, station.sftp_user)

        with exceptionLogged(logger, "Disk Quota: Y:/Gruppen/chs-data"):
            logger.info("checking: chs-data")
            content = parseLocalUsageFile()
            writeLog(content, "chs-data")
            checkUsage(content, "chs-data")


if __name__ == "__main__":
    from lib.argparser import parseArguments

    args = parseArguments("check SFTP disk usage")

    main(station=args.station, device=args.device, debug=args.debug)
