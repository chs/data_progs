#! /usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

import pandas as pd

from config.data import NODATA
from lib.daccess import iterDataFreq, writeTable
from lib.pdutils import inferFrequency


def procDevice(logger, device, **kwargs):
    df = device.getL2Data(fillna=True, drop_record=False)
    if df.empty:
        return

    for date, data in iterDataFreq(df, freq="yearly"):
        fname = f"{device.station_key}_{device.logger_key}_{date.year}.csv"

        if device.data_tag in {"soilnet", "soilnet_icos"}:
            fname = f"Soilnet_{fname}"

        if device.data_tag == "meteo":
            index = pd.date_range(
                data.index.min(), data.index.max(), freq=inferFrequency(data)
            )
            data = data.reindex(index, fill_value=NODATA)

        outpath = Path(device.l1path, fname)
        logger.debug("writing: %s", outpath)
        writeTable(outpath, data)


if __name__ == "__main__":
    from lib.argparser import parseArguments
    from lib.driver import mapDevices

    args = parseArguments("write data into yearly csv files for easy consumption")
    mapDevices(__file__, args, procDevice)
