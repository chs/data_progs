#! /usr/bin/env python
# -*- coding: utf-8 -*-


"""
History
-------
Written Robin Leucht Mar/Apr 2017

Changelog
---------
David Schäfer Jul 2018 - rewrite
"""


from zipfile import ZipFile
from datetime import timedelta
from itertools import groupby

from pathlib import Path

from lib.faccess import getDevices
from lib.logger import initLogger, exceptionLogged
from lib.tools import firstOfMonth


def writeArchive(fname, fnames, logger):
    logger.debug("archiving: %s files", len(fnames))
    with ZipFile(fname, "w", compression=8, allowZip64=True) as zipf:
        for f in fnames:
            logger.debug("archiving: %s", f)
            zipf.write(f, Path(f).name)


def groupMonthly(device, fnames):
    # type: (Entry, Sequence[str]) -> Iterator[Sequence[str]]
    dates = tuple(device.getFnameDate(f) for f in fnames)
    fnames = sorted(zip(dates, fnames), key=lambda x: x[0])
    for k, g in groupby(fnames, key=lambda x: (x[0].year, x[0].month)):
        dates, fnames = zip(*tuple(g))
        yield k, fnames


def main(station, device, start_date, end_date, debug):
    # process entire months until the previous one
    end_date = firstOfMonth(end_date) - timedelta(days=1)
    start_date = min([firstOfMonth(start_date), firstOfMonth(end_date)])

    devices = getDevices(
        station=station,
        logger=device,
        tag=("eddy", "eddy_bc", "eddy_icos", "pheno"),
        start_date=start_date,
        end_date=end_date,
    )

    with initLogger(__file__, debug) as logger:
        for device in devices:
            logger.info("processing: %s", device)
            msg = "Failed to write archive for: {:}".format(device)
            with exceptionLogged(logger, msg):
                fnames = tuple(device.getL0Files())
                for date, fnamegroup in groupMonthly(device, fnames):
                    logger.debug("processing: %s-%s", date[0], date[1])
                    for zipfname, zipcontent in device.iterArchiveGroups(fnamegroup):
                        zipfname = str(Path(device.archivepath, zipfname))
                        writeArchive(zipfname, zipcontent, logger)


if __name__ == "__main__":
    from lib.argparser import parseArguments

    args = parseArguments("Update the monthly archives", {"ndays": 1})
    main(
        station=args.station,
        device=args.device,
        start_date=args.start_date,
        end_date=args.end_date,
        debug=args.debug,
    )
