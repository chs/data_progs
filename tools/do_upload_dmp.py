#! /usr/bin/env python
# -*- coding: utf-8 -*-

# import concurrent.futures

import json
from typing import Iterator, Tuple

import numpy as np
import pandas as pd

from lib.daccess import splitTable
from lib.daccess import ConfigFields as CF
from lib.daccess import IndexFields as IF
from config.data import NODATA

from pipetools.dmpapi import DmpAPIPandas, stripDaylightSavingTimes


def toDmpFlags(flags):
    def _translateFlags(flags: pd.Series) -> pd.Series:
        index, flags = flags.index, flags.to_numpy()
        ndigits = np.floor(np.log10(flags[0])).astype(int)

        max_flags = np.zeros_like(flags)
        for pos in range(1, ndigits):
            digits = flags // 10 ** (ndigits - pos) % 10
            max_flags = np.maximum(max_flags, digits)

        out = pd.Series(max_flags, index=index)
        out = out.replace({0: "OK", 1: "DOUBTFUL", 2: "BAD", 9: "OK"})
        return out

    out = pd.DataFrame(
        index=flags.index,
        columns=pd.MultiIndex.from_product(
            [flags.columns, ("quality_flag", "quality_cause", "quality_comment")]
        ),
    )
    if out.empty:
        return out

    for key, values in flags.items():
        out.loc[:, (key, "quality_flag")] = _translateFlags(values)
        out.loc[:, (key, "quality_cause")] = "OTHER"
        out.loc[:, (key, "quality_comment")] = values.apply(
            lambda v: json.dumps({"flag": v})
        )

    return out


def getData(device, start_date, end_date):
    config = device.readExcel()[[CF.VARNAME, CF.DBNAME]]
    for col in config.columns:
        config[col] = config[col].str.strip()
    data = device.getL2Data(start_date=start_date, end_date=end_date, reindex=True)
    data = data.rename(columns=dict(config.to_numpy()))

    data, flags = splitTable(data, index_level=IF.VARNAME)
    if data.columns.duplicated().any():
        dups = data.columns[data.columns.duplicated()]
        raise ValueError(
            f"The variables {dups} are duplicated within the dataset. "
            "Maybe something went wrong in CHS-Measurements?"
        )
    return data, toDmpFlags(flags)


def iterMonths(
    start_date: pd.Timestamp, end_date: pd.Timestamp
) -> Iterator[Tuple[pd.Timestamp, pd.Timestamp]]:
    for _, chunk in (
        pd.date_range(start_date, end_date, freq="D")
        .to_series()
        .groupby(pd.Grouper(freq="M"))
    ):
        yield chunk.iloc[0], chunk.iloc[-1]


def uploadData(logger, api, device):
    config = device.readExcel()
    config = config.set_index(CF.DBNAME)[[CF.START_DATE, CF.END_DATE]]

    sensors = api.getSensors(device.dmp_logger_id, filter=True)["sensorName"]

    for start_date, end_date in iterMonths(device.start_date, device.end_date):
        logger.info(f"{device}: {start_date.date()} -- {end_date.date()}")
        data_new, flags_new = getData(device, start_date, end_date)
        if data_new.empty and flags_new.empty:
            continue

        chunk_sensors = config[config[CF.START_DATE] <= data_new.index.max()].index
        diff = chunk_sensors.difference(sensors)
        if not diff.empty:
            raise ValueError(f"missing dmp sensors for {diff.tolist()}")
        chunk_sensors = chunk_sensors.intersection(data_new.columns)

        data_new = (
            data_new[chunk_sensors]
            .pipe(stripDaylightSavingTimes)
            .stack()
            .swaplevel()
            .to_frame(name="data")
        )
        # we don't want -9999 in the DB
        data_new = data_new.replace(NODATA, np.nan)

        flags_new = (
            flags_new[chunk_sensors]
            .pipe(stripDaylightSavingTimes)
            .stack(level=0)
            .swaplevel()
        )

        api.postLevel1Data(
            device.dmp_logger_id, data_new, label=False, verbose=logger.debug
        )
        api.postLevel2Data(
            device.dmp_logger_id,
            flags_new,
            label=False,
            verbose=logger.debug,
            check=True,
        )


def bailOut(logger, device):
    if not device.dmp_logger_id:
        logger.info(f"skipping upload: no dmp_logger_id for {device}")
        return True

    df = device.getL2Data(columns=[]).reset_index()
    if df.empty:
        logger.info(f"skipping upload: no data for {device}")
        return True

    return False


def procDevice(logger, device):
    logger.info(f"processing device: {device}")
    if bailOut(logger, device):
        return

    api = DmpAPIPandas(
        data_project=device.dmp_id,
        username=device.dmp_username,
        password=device.dmp_password,
    )

    uploadData(logger=logger, api=api, device=device)


if __name__ == "__main__":
    from lib.argparser import ChainParser
    from lib.driver import mapDevices

    parser = ChainParser("upload processed data to the DMP")
    args = parser.parseArgs()
    mapDevices(__file__, args, procDevice)
