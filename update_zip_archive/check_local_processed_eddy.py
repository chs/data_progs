# -*- coding: utf-8 -*-
"""
Created on Mon May 08 18:04:10 2017

@author: leucht
"""


def check_local_processed_eddy(infile):
    log = open(infile, "r")
    processed_raw = log.readlines()[1:]  # skip header
    log.close()

    processed_raw = [i.strip() for i in processed_raw]  # remove format characters

    return processed_raw


def compare_local_to_ftp_eddy(list_local, list_ftp):
    # check if files on the ftp server were already downloaded and processed
    # (they do not net to be downloaded again)
    new_list_ftp = [
        line_ftp
        for line_ftp in list_ftp
        if not any([(line_ftp == line_local) for line_local in list_local])
    ]

    return new_list_ftp
