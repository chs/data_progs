# #############################################################################
# #############################################################################
#
#   update zip archives
#
# #############################################################################
# #############################################################################


"""
This script reads files stored in zip archives in a given directory and checks
if the files in the zip archives are incomplete in reference to given data files in
a comparative repository. The scripts links repository files to their associated
zip archives by the date information given in their name and updates the
zip archives, if the repository file has data stored, that is not contained in
the zip archive. The script is able to move repository files after
updating / checking, if wanted so. BadZipfiles will be recorded and printed in a separate file.

To achieve the same level of compression of the archives, when they were produced at
the measurement site, the same external zippping tool will be applied after updating the
zip archives. depending on their filesize the members of the zip archives or the repository files
are extracted or copied to the process directory (processdir), where you can find the zipping tool ZIP.exe.
The tool zips the members into a new archive, restores the old file access and manipulation time and moves
them back to the Raw-Zip directory.


    Note:   This version checks only if the size of the files gives a hint, if
            the repository file has more information, as it is expected, that
            files in the zip archives and the repository start at an entry with
            the same time of capture and only differ in the ammount of entries stored

    parameters:
      indir_zip                 Directory with input zip archives.
      indir_rep                 Directory with input repository files.

    optional keyword:
      move                  Move the marked files in the repository to a outsource directory
                            (default is to not move)

    output:
       copy of original zip archives, updated with files from the repository
       file containing list of all repository files, that either now can be deleted or have been moved to a tmp directory
       file containing list of corrupt BadZipFiles, if any where found


Execution example (working Directory is Eddydata):
------------------------------------------------------

    indir_zip = '/Y:/Gruppen/chs-data/HohesHolz/Eddydata/'
    update_zip_archives_eddy(indir, move = 0)


History
-------
Written Robin Leucht Mar 2017
"""

import datetime
import time

import zipfile
from zipfile import BadZipfile

import os
import sys
import shutil

import numpy as np

# ufz process logging
from lib.logger import getLogger

LOGGER = getLogger("update_zip_archives_eddy_external_zipper")


def update_zip_archives_eddy(indir, move=0):
    # get directory by date
    date = datetime.datetime.today()

    # "Eddydata" as working directory
    indir_zip = indir + "Raw-Zip_" + str(date.year) + "/"
    indir_rep = indir + "Rawdata" + str(date.year) + "/"

    outdir = os.path.join(indir_zip, "tmp/")
    if not os.path.isdir(outdir):
        os.mkdir(outdir)

    processdir = os.path.join(indir_zip, "process/")
    if not os.path.isdir(processdir):
        os.mkdir(processdir)

    # 1 - Get all files
    # ------------------------------

    zip_archives = os.listdir(indir_zip)  # could have used glob(), but for rep_files
    rep_files = os.listdir(
        indir_rep
    )  # we want all files instead of a specific wildcard match

    # as listdir produces a list of all files in the directory, nonzip / dia, slt files may be included
    # need to exclude them
    zip_archives = [z for z in zip_archives if z.endswith("zip")]
    rep_files = [z for z in rep_files if z.endswith("dia") or z.endswith("slt")]
    doy_rep_list = [z[5:8] for z in rep_files]

    bad_files = []

    move = int(move)
    move_request = []
    if move == 1:
        if not os.path.isfile(indir_rep + "processed_rawdata" + ".txt"):
            move_report_f = open(indir_rep + "processed_rawdata" + ".txt", "w")
            move_report_f.write(
                " These files were move to already moved to the local tmp directory \n"
            )
            move_report_f.close()

        move_report_f = open(indir_rep + "processed_rawdata" + ".txt", "a")

    # 2 - Linking single repository files to entries in zip archives
    #     Comparing filesize of archive members to single repository afterwards
    # ----------------------------------------------------------------------------

    for z in range(len(zip_archives)):
        doy = zip_archives[z][6:9]
        doy_today = (date - datetime.datetime(date.year, 1, 1)).days + 1

        if int(doy) == int(doy_today):
            continue  # ignore archive from today as it may not be complete yet

        print(" Archive DOY: ", int(doy), "  ", zip_archives[z])

        # check if the process directory is clear, clear directory otherise
        process_files = os.listdir(processdir)
        process_files = [
            f for f in process_files if f.endswith(".slt") or f.endswith(".dia")
        ]

        if len(process_files) > 0:
            for i in process_files:
                os.remove(processdir + i)

        # check only when there are files in the repository, that belong to the zip archive
        if any([bool(f == doy) for f in doy_rep_list]):
            try:
                zip_file = zipfile.ZipFile(
                    indir_zip + zip_archives[z], "r"
                )  # open zip file for reading

            except BadZipfile:
                LOGGER.debug(
                    "Badzipfile detected (even regular access is impeded): %s",
                    zip_archives[z],
                )
                LOGGER.debug("Zipfile skipped.")
                bad_files.append(zip_archives[z])
                continue

            zip_members = (
                zip_file.namelist()
            )  # find content / members of the zip archive
            zip_info = zip_file.infolist()  # zip member informations

            # gather filesize of zip archive members and random files
            zip_member_size = []
            rep_file_size = []

            # get single repository files, missing in the raw zip archive, but belonging there
            # -----------------------------------------------------------------------------------------------

            # get files, that belong to this zip archive
            query_string_miss = zip_archives[z][0:5] + zip_archives[z][6:9]
            query_list_miss = [bool(r[0:8] == query_string_miss) for r in rep_files]
            query_list_miss = np.nonzero(query_list_miss)
            query_files_miss = [rep_files[q] for q in query_list_miss[0]]

            # separate files, that are not included in the zip_archive
            dummy1 = []
            dummy2 = []
            for q in range(len(query_files_miss)):
                if any([bool(f == query_files_miss[q]) for f in zip_members]):
                    pass
                else:
                    dummy1.append(query_files_miss[q])
                    dummy2.append(query_list_miss[0][q])

            query_files_miss = dummy1
            query_list_miss = dummy2

            # get filesize of all files
            # ---------------------------

            for f in range(len(zip_members)):
                # 1. Method, filesize by compressed zip archive information, return of incorrect filesize due to compression possible
                # zip_member_size.append(zip_info[f].file_size) # zip file size

                # 2. Method, filesize by extracted uncompressed files
                try:
                    dummy = zip_file.extract(zip_members[f])
                    zip_member_size.append(
                        int(os.path.getsize(dummy))
                    )  # extracted zip content size
                    os.remove(dummy)

                # sometimes there are bad zipfiles, skip this specific one, but record it for later
                except BadZipfile:
                    LOGGER.debug(
                        "Badzipfile-member detected: %s",
                        zip_archives[z] + ": " + zip_members[f],
                    )
                    LOGGER.debug(
                        "Zipfile-member not extracted, will be replaced if rawdata is available."
                    )

                    zip_member_size.append(-999)
                    bad_files.append(zip_archives[z] + "/" + zip_members[f])

                    # BadZipfiles are still extracted, but try aborts before removing them again
                    if os.path.isfile(zip_members[f]):
                        os.remove(zip_members[f])

                # find matching file entry in the repository file directory
                query_name = zip_info[f].filename

                query_list = [
                    bool(r == query_name) for r in rep_files
                ]  # true/false list, whichs true element
                #  indicates the location of query_name in rep_files
                if any(query_list):
                    location = np.nonzero(query_list)[0][
                        0
                    ]  # nonzero() finds this location (if it is there)
                    query_file = rep_files[location]

                    rep_file_size.append(
                        int(os.path.getsize(indir_rep + query_file))
                    )  # repository file size

                else:
                    rep_file_size.append(
                        -1
                    )  # when the file was not found in the repository

                # add single repository files, missing the raw zip archive, but belonging there
                # -------------------------------------------------------------------------

                # get time from name -> insert missing file entry in chronological order
                if (len(query_files_miss) > 0) and (
                    int(zip_members[f][5:12]) > int(query_files_miss[0][5:12])
                ):
                    # insert entry of missing file in list and put a mark with -1
                    zip_members.insert(f, query_files_miss[0])
                    zip_member_size.insert(f, -1)
                    zip_info.insert(
                        f, zip_info[0]
                    )  # insert a fake zip info object into info list

                    # convert old file time stamp to proper time tuple
                    time_float = os.path.getmtime(indir_rep + query_files_miss[0])
                    time_tuple = time.localtime(time_float)
                    time_tuple = (
                        time_tuple.tm_year,
                        time_tuple.tm_mon,
                        time_tuple.tm_mday,
                        time_tuple.tm_hour,
                        time_tuple.tm_min,
                        time_tuple.tm_sec,
                    )
                    zip_info[f].date_time = time_tuple

                    rep_file_size.insert(
                        f, int(os.path.getsize(indir_rep + query_files_miss[0]))
                    )

                    # therefor discard entry in the list of missing files (until noone is left)
                    del query_files_miss[0]
                    del query_list_miss[0]

            # add any missing files, that are left over for this zip archive
            if len(query_files_miss) > 0:
                for miss_file in query_files_miss:
                    # add entry of missing file to list and put a mark with -1
                    zip_members.append(miss_file)
                    zip_member_size.append(-1)
                    zip_info.append(
                        zip_info[0]
                    )  # insert a fake zip info object into info list

                    time_float = os.path.getmtime(indir_rep + miss_file)
                    time_tuple = time.localtime(time_float)
                    time_tuple = (
                        time_tuple.tm_year,
                        time_tuple.tm_mon,
                        time_tuple.tm_mday,
                        time_tuple.tm_hour,
                        time_tuple.tm_min,
                        time_tuple.tm_sec,
                    )
                    zip_info[-1].date_time = time_tuple

                    rep_file_size.append(int(os.path.getsize(indir_rep + miss_file)))

            # 3 - Updating a zip archive by creating an updated version
            # ----------------------------------------------------------------

            # but only, if at least one member needs to be updated
            if any(
                [
                    bool(rep_file_size[f] > zip_member_size[f])
                    for f in range(len(zip_members))
                ]
            ):
                print(
                    "Comparing filesize of old zip archive members to updated members"
                )
                print("                                 old                   updated")

                for f in range(len(zip_members)):
                    # keep the old file, when there was no entry in the random file directory
                    if rep_file_size[f] == -1 or rep_file_size[f] <= zip_member_size[f]:
                        if rep_file_size[f] != -1 and rep_file_size[f] != -999:
                            print(
                                zip_members[f],
                                "    zip: ",
                                zip_member_size[f],
                                "     rep: ",
                                rep_file_size[f],
                            )

                        try:
                            zip_file.extract(zip_members[f], processdir)

                            atime = time.time()
                            mtime = zip_info[f].date_time
                            mtime = time.mktime(mtime + (0, 0, 0))

                            os.utime(processdir + zip_members[f], (atime, mtime))

                            if os.path.isfile(indir_rep + zip_members[f]):
                                move_request.append(zip_members[f])

                        # sometimes there are bad files, which can not be opened for reading
                        # workaround: replace with matching repository file, if possible
                        except BadZipfile:
                            LOGGER.debug(
                                "Badzipfile-member detected while updating zipfiles: %s",
                                zip_archives[z] + ": " + zip_members[f],
                            )

                            if (zip_member_size[f] == -999) and (
                                os.path.isfile(indir_rep + zip_members[f])
                            ):
                                print(
                                    zip_members[f],
                                    "    zip: ",
                                    "-BadZip-",
                                    " rep: ",
                                    rep_file_size[f],
                                    "   [BadZipFile replaced]",
                                )
                                LOGGER.debug("Badzipfile-member sucessfully replaced.")

                                shutil.copy2(
                                    indir_rep + zip_members[f],
                                    processdir + zip_members[f],
                                )

                                move_request.append(zip_members[f])

                                # get last modification- time of zip member
                                atime = os.path.getatime(indir_rep + zip_members[f])
                                mtime = os.path.getmtime(indir_rep + zip_members[f])
                                os.utime(processdir + zip_members[f], (atime, mtime))

                            elif os.path.isfile(indir_rep + zip_members[f]) == False:
                                LOGGER.debug(
                                    "Badzipfile-member not replaced. No replacement available"
                                )
                                print(
                                    zip_members[f],
                                    "    zip: ",
                                    "-BadZip-",
                                    "             [BadZipFile but not replacement available]",
                                )

                    elif rep_file_size[f] > zip_member_size[f]:
                        # update the file entry by the file from the random file directory
                        if zip_member_size[f] == -1:
                            print(
                                zip_members[f],
                                "    zip: ",
                                "-N/A-",
                                "    rep: ",
                                int(os.path.getsize(indir_rep + zip_members[f])),
                                "   [added to archive]",
                            )
                        else:
                            print(
                                zip_members[f],
                                "    zip: ",
                                zip_member_size[f],
                                "     rep: ",
                                rep_file_size[f],
                                "   [updated]",
                            )

                        shutil.copy2(
                            indir_rep + zip_members[f], processdir + zip_members[f]
                        )

                        move_request.append(zip_members[f])

                        # get last modification- time of zip member
                        atime = os.path.getatime(indir_rep + zip_members[f])
                        mtime = os.path.getmtime(indir_rep + zip_members[f])
                        os.utime(processdir + zip_members[f], (atime, mtime))

                print("")  # newline in console
                print("")

                zip_file.close()

                # Deploy external zipping tool, used at the measurement site computers, to recreate the same level of compression
                # ----------------------------------------------------------------------------------------------------------------
                old_cwd = os.getcwd()  # save old working directory
                os.chdir(
                    processdir
                )  # move to processing directory (location of the ZIP.exe)

                # Deploy zipping tool
                for member in zip_members:
                    os.system(
                        "ZIP.EXE -m -j " + "../tmp/" + zip_archives[z] + " ./" + member
                    )

                os.chdir(old_cwd)  # restore old working directory

            else:  # when no repository file is bigger than the original zip member,
                # for a specific zip archive, move all reposiotry files to tmp,
                # while skipping
                for f in range(len(rep_file_size)):
                    if rep_file_size[f] > -1:
                        move_request.append(zip_members[f])

    # 4 - clear repository and move them if requested
    # ----------------------------------------------------------------

    if move == 1:
        print(" Moving processed repository files to tmp directory...")

        movdir = os.path.join(indir_rep, "tmp/")
        if not os.path.isdir(movdir):
            os.mkdir(movdir)

        for line in move_request:
            shutil.move(indir_rep + line, movdir + line)

            move_report_f.write(
                line + "\n"
            )  # log that the file was moved (necessary to check if the file should be downloaded from the ftp server or not)

        move_report_f.close()

    # ----------------------------------------------------------------------

    # 5 - report badfiles
    # ---------------------
    if len(bad_files) > 0:
        filepath = indir_zip + "BadZipFiles_report_" + str(date.year) + ".txt"

        if os.path.isfile(filepath):
            report_f = open(filepath, "r+")
            lines = report_f.readlines()

        else:
            report_f = open(filepath, "w")
            report_f.write(
                "BadZipFile members found in the archives for year "
                + str(date.year)
                + "\n \n"
            )
            lines = []

        lines = lines[2:]

        for badfile in bad_files:
            query_list = [bool(line.strip() == badfile) for line in lines]
            if any(query_list):
                pass
            else:
                report_f.write(badfile + "\n")

        report_f.close()

    # optional: move updated files to input directory and replace the old ones (optional to batch script move command)
    print(" Moving updated zip files...")

    updated_zip = os.listdir(outdir)

    for src in updated_zip:
        try:
            os.remove(indir_zip + src)
        except FileNotFoundError:
            pass
        shutil.move(os.path.abspath(os.path.join(outdir, src)), indir_zip)

    # Done
    print(" ... Done")
    LOGGER.debug("returned function update_zip_archives_eddy()")

    return 1


if __name__ == "__main__":
    if len(sys.argv) > 3:
        LOGGER.debug(
            "Too many arguments for update_zip_archives_eddy_external_zipper.py for %s",
            sys.argv[1][20 : 20 + sys.argv[1][20:].find("/")],
        )
        raise RuntimeError("Too many arguments!")

    indir = sys.argv[1]
    move = sys.argv[2]

    LOGGER.info(
        "Do update eddy data zip_archives for %s", indir[20 : 20 + indir[20:].find("/")]
    )
    LOGGER.info("starting...")

    LOGGER.debug("calling function update_zip_archives_eddy()")

    #    indir       = 'Y:/Gruppen/chs-data/HohesHolz/Eddydata/'
    #    indir       = 'Y:/Gruppen/chs-data/Hordorf/Eddydata/'
    #    indir       = 'Y:/Gruppen/chs-data/Wulferstedt/Eddydata/'
    update_zip_archives_eddy(indir, move=1)

    LOGGER.info("...finished")
