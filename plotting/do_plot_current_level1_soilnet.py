#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re
from pathlib import Path
from typing import Dict

import pandas as pd
import matplotlib.pyplot as plt

from lib.faccess import getStations
from lib.daccess import splitTable
from lib.logger import initLogger, exceptionLogged
from lib.plotting import (
    getLegendHandle,
    iterFlagPlots,
    removeFlaggedData,
    htmlWrapper,
    appendSubplot,
    configAxes,
)


COLORS = ["red", "blue", "green", "purple", "orange", "gold", "grey", "black"]


def prepData(data, flags):
    # drop unflagged columns
    is_flagged = ~(flags % 9 == 0).all()
    cols = is_flagged[is_flagged].index
    data, flags = data[cols], flags[cols]
    return data.astype("float32"), flags.astype("int64")


def plotSoilnetVariables(fig, data, flags, label):
    col = data.columns.get_level_values(level="headers")[0]
    box = col.split("_")[0]
    unit = col.split(" ")[-1]

    data, flags = prepData(
        data.droplevel(level="headers", axis="columns"),
        flags.droplevel(level="headers", axis="columns"),
    )

    ax = appendSubplot(fig)
    configAxes(
        ax, "{:} {:} {:}".format(box, label, unit), data.index.min(), data.index.max()
    )

    legend = []
    for name, color in zip(data.columns, COLORS):
        llabel = re.sub("(Temp|Moist)", "Spade ", re.sub("Box\d+_", "", name))
        legend.append([getLegendHandle(color), llabel])
        idx = flags[name].notnull()
        if not idx.any(axis=None):
            continue
        dvalues = data.loc[idx, name]
        fvalues = flags.loc[idx, name]
        for flgs, _, line, _, _, _ in iterFlagPlots(dvalues, fvalues):
            ax.plot(flgs, color=color, linestyle=line)
        ax.plot(removeFlaggedData(dvalues, fvalues), color=color)
    ax.legend(*zip(*legend), loc="best", ncol=6, frameon=False, fontsize="medium")
    return ax


def plotBox(fig, df, height=8):
    dfs = [
        [df.filter(regex="_Moist[0-9](\s\[.*\]|_f)"), "Moisture", [0, 60]],
        [df.filter(regex="_Temp[0-9](\s\[.*\]|_f)"), "Temperature", [None, None]],
    ]

    for i, (df, plabel, ylims) in enumerate(dfs):
        data, flags = splitTable(df)
        data = data.mask((data < ylims[0]) | (data >= ylims[1]))
        ax = plotSoilnetVariables(fig, data, flags, plabel)
        ax.set_ylim(ylims)

    w, h = fig.get_size_inches()
    fig.set_size_inches(w, h + height)


def plotSHF(fig, dfs: Dict[str, pd.DataFrame]):
    # NOTE:
    # I know, I know, this is rather hacky and badly integrated,
    # but I really don't want to spend, more time as necessary on
    # the plotting scripts

    ax = appendSubplot(fig)

    start_date = min([df.index.min() for df in dfs.values()])
    end_date = max([df.index.max() for df in dfs.values()])

    configAxes(ax, "SHF [W/m**2]", start_date, end_date)

    legend = []
    for (box, df), color in zip(dfs.items(), COLORS):
        data, flags = splitTable(df, 0)
        data, flags = data.squeeze(), flags.squeeze()
        label = data.name

        legend.append([getLegendHandle(color), label])
        data = data.mask((data < -50) | (data >= 50))

        idx = flags.notnull()
        if not idx.any(axis=None):
            continue

        dvalues = data.loc[idx]
        fvalues = flags.loc[idx]

        for flgs, _, line, _, _, _ in iterFlagPlots(dvalues, fvalues):
            ax.plot(flgs, color=color, linestyle=line)
        ax.plot(removeFlaggedData(dvalues, fvalues), color=color)

    ax.legend(*zip(*legend), loc="best", ncol=6, frameon=False, fontsize="medium")

    return ax


def main(
    station=None, device=None, start_date=None, end_date=None, debug=False, fail=False
):
    from pandas.plotting import register_matplotlib_converters

    register_matplotlib_converters()

    with initLogger(__file__, debug) as logger:
        stations = getStations(
            station=station,
            logger=device,
            tag=("soilnet", "soilnet_icos"),
            start_date=start_date,
            end_date=end_date,
        )

        for station in stations:
            fig = plt.figure(figsize=(12, 0))
            logger.info("processing: %s", station)

            shf_data = {}
            for device in station.getDevices():
                msg = "failed to plot: {:}".format(device)
                with exceptionLogged(logger, msg, fail=debug):
                    df = device.getL2Data(fillna=False)
                    if df.empty:
                        continue
                    shf = df.filter(regex="SHF\d{2}(\s\[.*\]|_f)")
                    if not shf.empty:
                        shf_data[device.logger_key] = shf
                    logger.debug("processing: %s", device)
                    plotBox(fig, df)

            with exceptionLogged(logger, msg, fail=fail):
                if shf_data:
                    plotSHF(fig, shf_data)

                fname = Path(station.soilnet_path, "current_level1_soilnet.png")
                logger.debug("writing to: %s", fname)
                if all(fig.get_size_inches()):
                    plt.savefig(str(fname), bbox_inches="tight")
                    with open(str(fname.with_suffix(".html")), "w") as f:
                        f.write(htmlWrapper(fname.name))
                else:
                    logger.warn("No data to plot")


if __name__ == "__main__":
    from lib.argparser import parseArguments

    args = parseArguments("Plot flagged soilnet data", {"ndays": 1})
    main(
        station=args.station,
        device=args.device,
        start_date=args.start_date,
        end_date=args.end_date,
        debug=args.debug,
        fail=args.fail,
    )
