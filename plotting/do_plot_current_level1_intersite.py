#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re
import pandas as pd
from pathlib import Path
import matplotlib.pyplot as plt
from lib.faccess import getDevices
from config.uri import PLOTPATH
from lib.daccess import splitTable, filterVariables
from lib.pdutils import setIndexLevel
from lib.logger import initLogger, exceptionLogged
from lib.plotting import (
    appendSubplot,
    configAxes,
    plotLine,
    iterFlagPlots,
    removeFlaggedData,
)

# NOTE:
# This program tends to throw MemoryErrors. To mitigate this, the subsetting
# of variables could be done directly within the reading functions and not
# within the main loop in function plotIntersite.
# BTW, the subsetting call chain is rather confusing and should be simplified.

OUTPATH = Path(PLOTPATH, "Intersite_comparison")

# [(output_name, df_header_pattern, [plot_ylmit_values_or_function_on_data])]
# type: Sequence[
# type:   Sequence[str, str, Sequence[
# type:                        Union[Callable[[pd.DataFrame], float], float],
# type:                        Union[Callable[[pd.DataFrame], float], float]]]]
# NOTE: would be good to have a other data structure (dict or namedtuple)
VARIABLES = [
    (r"airpres", r"^airpres", [920, 1050]),
    (r"LWDR", r"^LWDR", [180, 520]),
    (r"LWUR", r"^LWUR", [220, 600]),
    (r"precip", r"^precip", [0, 35]),
    (r"precip_min_scale", r"^precip", [-0.1, lambda df: df.max().min()]),
    (r"RH", r"^RH", [0, 105]),
    (r"Tair", r"^Tair", [-35, 50]),
    (r"Trad", r"^Trad", [-30, 105]),
    (r"WS", r"^WS.*\[m/s\]", [0, 50]),
]


def subsetVariable(dfs, var_pattern):
    # type: (List[pd.DataFrame, str]) -> pd.DataFrame
    data = []
    for df in dfs:
        dff = filterVariables(df, regex=var_pattern)
        if not dff.empty:
            data.append(setIndexLevel(dff, 2))
    return pd.concat(data, axis=1)


def renameColumns(columns, device_key):
    out = []
    for col in columns:
        parts = re.split(r"(_(?=f)|\s+)", col)
        if device_key not in col:
            parts[-3] = "{:}_{:}".format(parts[0], device_key)
        out.append("".join(parts))
    return out


def readData(devices):
    dfs = []
    for device in devices:
        data = device.getL2Data(fillna=False, fill=True)
        if data.empty:
            continue
        data.columns = pd.Index(
            renameColumns(data.columns, device.logger_key), name=data.columns.name
        )
        dfs.append(data)
    return dfs


def plotSensors(fig, data, flags, ylims):
    interval = data.index.freq

    for i, (name, label) in enumerate(data.columns):
        ax = appendSubplot(fig)
        configAxes(ax, label, data.index.min(), data.index.max())

        flag_plot_data = iterFlagPlots(data.iloc[:, i], flags.iloc[:, i])
        for flgs, color, line, marker, _, _ in flag_plot_data:
            plotLine(ax, flgs, interval, color, line, marker)
        ax.plot(removeFlaggedData(data.iloc[:, i], flags.iloc[:, i]), color="blue")
        ax.set_ylim(ylims)


def calcYLimits(data, ymin, ymax):
    if callable(ymin):
        ymin = ymin(data)

    if callable(ymax):
        ymax = ymax(data)

    ymin = max(data.min().min() * 0.99, ymin)
    ymax = min(data.max().max() * 1.01, ymax)
    return ymin, ymax


def plotIntersite(devices, outpath, logger):
    logger.debug("reading data")
    dfs = readData(devices)
    logger.debug("read data")

    for var, var_pattern, (ymin, ymax) in VARIABLES:
        logger.info("plotting: %s", var)
        df = subsetVariable(dfs, var_pattern)
        data, flags = splitTable(df)

        flags.fillna(9, inplace=True)

        ylims = calcYLimits(data, ymin, ymax)
        fig = plt.figure(figsize=(12, 1.8 * len(df.columns)))
        plotSensors(fig, data, flags, ylims)

        outfile = Path(outpath, "{:}_current_level1_intersite.png".format(var))
        logger.debug(f"writing: {outfile}")
        fig.savefig(str(outfile), bbox_inches="tight")


def main(
    outpath, station=None, device=None, start_date=None, end_date=None, debug=False
):
    with initLogger(__file__, debug) as log:
        with exceptionLogged(log, fail=debug):
            devices = getDevices(
                station=station,
                logger=device,
                tag="meteo",
                start_date=start_date,
                end_date=end_date,
            )
            plotIntersite(devices, outpath, log)


if __name__ == "__main__":
    from lib.argparser import parseArguments

    args = parseArguments("Plot site comparisons", {"ndays": 1})

    outpath = str(Path(OUTPATH, "{:}_days".format(args.ndays)))
    main(
        outpath,
        station=args.station,
        device=args.device,
        debug=args.debug,
        start_date=args.start_date,
        end_date=args.end_date,
    )
