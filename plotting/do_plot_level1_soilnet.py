#! /usr/bin/env python
# -*- coding: utf-8 -*-

import warnings
from pathlib import Path

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from config.data import NODATA

from lib.tools import firstOfMonth, lastOfMonth, firstOfYear, lastOfYear
from lib.daccess import Fields, splitTable, iterDataFreq, limitData, splitSoilnet
from lib.flagging import getMaxflags
from lib.faccess import getDevice, getStations
from lib.logger import initLogger, exceptionLogged
from lib.plotting import (
    getLegendHandle,
    SOILNETPLOTDATA,
    plotLine,
    appendSubplot,
    configAxes,
    plotData,
)

from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()


warnings.simplefilter(action="ignore", category=pd.errors.PerformanceWarning)


PLOTPATH = "Plots"


TEMPCOMPVARS = {"HH": {"T5": ["STa010"]}, "GB": {"W2": ["ST010"]}}


MOISTCOMPVARS = {
    "HH": {"BC3": ["precip02WXT"], "T3": ["precip08WXT"], "T1": ["precip50"]},
    "GB": {"W2": ["precip_W"]},
}


INTERVAL = "30min"


class Monthly(object):
    def __init__(self, device):
        self._device = device
        self.freq = "monthly"

    def buildFname(self, varname, date):
        fname = "{:}_{:}_{:}-{:02d}.pdf".format(
            self._device.logger_key, varname, date.year, date.month
        )
        return Path(self._device.l1path, PLOTPATH, fname)

    def __str__(self):
        return "Monthly"

    @staticmethod
    def startDate(date):
        return firstOfMonth(date)

    @staticmethod
    def endDate(date):
        return lastOfMonth(date)

    __repr__ = __str__


class Yearly(object):
    def __init__(self, device):
        self._device = device
        self.freq = "yearly"

    def buildFname(self, varname, date):
        fname = "{:}_{:}_{:}.pdf".format(self._device.logger_key, varname, date.year)
        return Path(self._device.l1path, PLOTPATH, fname)

    def __str__(self):
        return "Yearly"

    @staticmethod
    def startDate(date):
        return firstOfYear(date)

    @staticmethod
    def endDate(date):
        return lastOfYear(date)

    __repr__ = __str__


def readCompareData(compdict, start_date, end_date):
    dfs = []
    for logger, cols in compdict.items():
        device = getDevice(logger=logger, start_date=start_date, end_date=end_date)
        data = device.getL2Data(fillna=True, reindex=True, nbits=32)
        try:
            dfs.append(data.loc[:, cols])
        except KeyError:
            pass
    if dfs:
        return pd.concat(dfs, axis=1)
    return pd.DataFrame()


def getSpadeDepths(df, config):
    vals = df.columns.get_level_values(0)
    selected = config[config[Fields.VARNAME].isin(vals)]
    out = selected.set_index(Fields.VARNAME, drop=True).sort_values(Fields.DEPTH)[
        Fields.DEPTH
    ]
    return out


def prepData(data, flags):
    is_flagged = ~(flags % 9 == 0).all()
    cols = is_flagged[is_flagged].index
    data, flags = data[cols], flags[cols]
    data = data.replace(NODATA, np.nan)
    return data.astype("float32"), flags.astype("int64")


def plotVariable(fig, df, config, start_date, end_date):
    data, flags = splitTable(df, index_level=0)
    data, flags = prepData(data, flags)
    if data.empty and flags.empty:
        warnings.warn("dataset is unflagged: skipping")
        return

    box = df.columns.get_level_values("headers")[0].split("_")[0]
    unit = df.columns.get_level_values("headers")[0].split(" ")[-1]
    depths = getSpadeDepths(df, config)

    ax = appendSubplot(fig)
    configAxes(ax, "{:} {:}".format(box, unit), start_date, end_date)

    data_legend = []
    for name, (llabel, color) in zip(data.columns, SOILNETPLOTDATA):
        idx = flags[name].notnull()
        dvalues = data.loc[idx, name].copy()
        if not dvalues.empty:
            dvalues = dvalues.mask(getMaxflags(flags.loc[idx, name].values) > 0)
        plotLine(ax, dvalues, color=color, interval=INTERVAL)
        data_legend.append([getLegendHandle(color), llabel])

        depth = depths[name]
        depth_ax = appendSubplot(fig)
        configAxes(
            depth_ax,
            "{:} D{:} {:}".format(name, round(depth, 2), unit),
            start_date,
            end_date,
        )

        flag_legend = plotData(
            depth_ax, data[name], flags[name], color=color, interval=INTERVAL
        )

    ax.legend(*zip(*data_legend), loc="best", ncol=6, frameon=False, fontsize="medium")
    fig.axes[0].legend(
        *flag_legend, loc="upper center", bbox_to_anchor=(0.5, 1.5), ncol=3
    )


def plotCompare(fig, df, start_date, end_date):
    cols = df.columns.get_level_values("varnames").drop_duplicates()
    for col in cols:
        ax = appendSubplot(fig)
        data = df[col]["data"]
        flags = df[(col, "flag")]
        configAxes(ax, data.columns[0], start_date, end_date)
        plotData(ax, data, flags, color="black", interval=INTERVAL)


def plotDeviceVariable(config, compdata, data):
    figsize = (12, 1.8 * (len(compdata.columns) + 7))
    fig = plt.figure(figsize=figsize)
    start_date = data.index.min()
    end_date = data.index.max()
    if not compdata.empty:
        plotCompare(fig, compdata, start_date, end_date)
    plotVariable(fig, data, config, start_date, end_date)
    return fig


def saveFigure(fig, fname):
    Path(fname).parent.mkdir(parents=True, exist_ok=True)
    fig.savefig(str(fname))


def plotDevice(config, data, moistcompdata, tempcompdata, freq_handler, logger):
    # NOTE: no warnings, please
    pd.set_option("mode.chained_assignment", None)

    moist, temp = [limitData(df, config) for df in splitSoilnet(data)]

    shf = data.filter(regex=".*_SHF\d+(\s+\[W/m\*\*2\]|_f)")
    if not shf.empty:
        tempcompdata = tempcompdata.join(shf)

    tmp = {
        "Temp": (temp, tempcompdata),
        "Moist": (moist, moistcompdata),
    }

    for varname, (df, compdata) in tmp.items():
        iterator = iterDataFreq(df, freq_handler.freq, drop_empty=False)
        for date, dfpart in iterator:
            start_date = freq_handler.startDate(dfpart.index.min())
            end_date = freq_handler.endDate(dfpart.index.max())

            logger.debug(
                "processing soil %s period: %s - %s",
                varname.lower(),
                start_date,
                end_date,
            )
            compart = compdata.loc[start_date:end_date]

            fig = plotDeviceVariable(config, compart, dfpart)
            fname = freq_handler.buildFname(varname, date)
            logger.debug("writing to: %s", fname)
            saveFigure(fig, fname)
            plt.close()


def main(
    station=None, device=None, start_date=None, end_date=None, debug=False, fail=False
):
    with initLogger(__file__, debug) as logger:
        stations = getStations(
            station=station,
            logger=device,
            tag=("soilnet", "soilnet_icos"),
            start_date=start_date,
            end_date=end_date,
        )

        for station in stations:
            logger.debug("reading compare data")

            start_date = Yearly.startDate(station.start_date)
            end_date = Yearly.endDate(station.end_date)

            tempcompdata = readCompareData(
                TEMPCOMPVARS[station.station_key], start_date, end_date
            )
            moistcompdata = readCompareData(
                MOISTCOMPVARS[station.station_key], start_date, end_date
            )

            for device in station.getDevices(devices=device):
                logger.info("processing: %s", device)

                msg = "soilnet plotting failed for {:}".format(device)
                with exceptionLogged(logger, msg, fail=fail):
                    logger.debug("reading data")
                    config = device.readExcel()
                    data = device.getL2Data(nbits=32, reindex=True, fillna=False)
                    if data.empty:
                        logger.warning("No data found for given period")
                        continue
                    for freq in [Yearly, Monthly]:
                        logger.debug("frequency: %s", freq)
                        plotDevice(
                            config,
                            data,
                            moistcompdata,
                            tempcompdata,
                            freq_handler=freq(device),
                            logger=logger,
                        )


if __name__ == "__main__":
    from lib.argparser import ChainParser

    parser = ChainParser("Plot flagged soilnet data", {"ndays": 1})
    args = parser.parseArgs()

    main(
        station=args.station,
        device=args.device,
        start_date=args.start_date,
        end_date=args.end_date,
        debug=args.debug,
        fail=args.fail,
    )
