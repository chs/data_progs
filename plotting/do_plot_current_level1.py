#! /usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
from lib.faccess import getDevices
from lib.flagging import getMaxflags
from lib.daccess import splitTable, dropEmptyVariables
from lib.logger import initLogger, exceptionLogged
from lib.plotting import (
    appendSubplot,
    configAxes,
    plotLine,
    removeFlaggedData,
    iterFlagPlots,
    htmlWrapper,
)

from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()

"""
Group plots

KS1:
Radiation[W/m**2]  {Rn, SWDR, SWUR}
LW[W/m**2]         {LWDR, LWUR}
ST[degC]           {ST[0-9]{3}}
SM[%]              {SM[0-9]{3}}
SHF [W/m**2]       {SHF[0-9]{2}, SHFm}

KS2:
Radiation[W/m**2]  {Rn, SWDR, SWUR}
LW[W/m**2]         {LWDR, LWUR}
ST[degC]           {ST[0-9]{3}}
SM[%]              {SM[0-9]{3}}
SHF [W/m**2]       {SHF[0-9]{2}, SHFm}

BC1:
??

BC2:
STF [mm]                 {STF_BC2_[0-9]{3}}
stem_T_[0-9]{2} [degC]   {stemT_[0-9]{3}_[0-9]*}

BC3:
STF [mm]                 {STF_BC3_[0-9]{3}}
stem_T_[0-9]{2} [degC]   {stemT_[0-9]{3}_[0-9]*}

T1:
Radiation[W/m**2]  {DDR, Rn, SWDR, SWDRspn1, SWUR}
LW[W/m**2]         {LWDR, LWUR}
WS[m/s]            {WS49_2D, WS49_doag_2D}
PAR[umol/m**2]     {PARBF[0-9]{1}, PARD[0-9]{2}, PARU[0-9]{2}, PARdiff}

T2:
WS[m/s]            {WS[0-9]{2}}
PAR[umol/m**2]     {PARD[0-9]{2}}
Tair[degC]         {Tair[0-9]{2}(PT|HMP)}

T3:
WS[m/s]            {WS[0-9]{2}(_WXT)?}
PAR[umol/m**2]     {PARD[0-9]{2}, PARU[0-9]{2}}
Tair[degC]         {Tair[0-9]{2}(PT|WXT)}

T4:
CO2[umol/mol]      {CO2_[0-9]{2}m}
H20[mmol/mol]      {H2O_[0-9]{2}m}
Flow[sl/m]         {Flow_[0-9]{2}m}

T5:
ST [degC]          {STHF[0-9]{2}(a|b)?, ST(a|b)[0-9]{3}}
STa [degC]         {STa[0-9]{3}}
STb [degC]         {STb[0-9]{3}}
STHF[degC]         {STHF[0-9]{2}(a|b)?}
SM[%]              {SM(a|b){3}}
SMa[%]             {SMa[0-9]{3}}
SMb[%]             {SMb[0-9]{3}}
SHF[W/m**2]        {SHF[0-9]{2}(a|b)?}
SHFsc[W/m**2]      {SHFsc[0-9]cal}

T6:
None

T7:
Trad[degC]         {Trad[0-9]{2}(n|s)}

T8:
Radiation[W/m**2]  {Rn_back, SWDR_back, SWUR_back}
LW[W/m**2]         {LWDR_back, LWUR_back}

W1:
Radiation[W/m**2]  {DDR, Rn, SWDR, SWDRspn1, SWUR}
LW[W/m**2]         {LWDR, LWUR}
SM[%]              {SM[0-9]{3}}
PAR[umol/m**2]     {PARD, PARU}

W2:
ST [degC]          {ST[0-9]{3}}
SHF[W/m**2]        {SHF([0-9]{2}|m)}
Tsoil [degC]       {Tsoil[0-9]{1,2}(_CS650)?}
"""

# [({argument to df.filter}, subplot label),]
VARGROUPS = [
    # solar radiation
    (
        {"regex": r"(DDR|Rn(_back)?|SWDR(_back|_spn1)?|SWUR(_back)?)"},
        "Radiation [W/m**2]",
    ),
    ({"regex": r"(LWDR(_back)?|LWUR(_back)?)"}, "LW [W/m**2]"),
    # soil temperature
    ({"regex": r"^ST(a|b|HF)?[0-9]+"}, "ST [degC]"),
    ({"regex": r"^STa[0-9]+"}, "STa [degC]"),
    ({"regex": r"^STb[0-9]+"}, "STb [degC]"),
    ({"regex": r"^STHF[0-9]+"}, "STHF [degC]"),
    # soil moisture
    ({"regex": r"^SM(a|b)?[0-9]+"}, "SM [%]"),
    ({"regex": r"^SMa[0-9]+"}, "SMa [%]"),
    ({"regex": r"^SMb[0-9]+"}, "SMb [%]"),
    # wind speed
    ({"regex": r"^WS[0-9]+"}, "WS [m/s]"),
    # not sure
    ({"regex": r"^PAR(B|D|U|d)"}, "PAR [umol/m**2]"),
    # not sure
    # ({"regex": r"^SHF.*"}, "SHF [W/m**2]"),
    ({"regex": r"^SHF([0-9]{2}|m)"}, "SHF [W/m**2]"),
    ({"regex": r"^SHFsc[0-9]+"}, "SHFsc [W/m**2]"),
    # stem flow
    ({"regex": r"^STF.*"}, "STF [mm]"),
    # misc
    ({"regex": r"^CO2_[0-9]+m"}, "CO2 [umol/mol]"),
    ({"regex": r"^H2O_[0-9]+m"}, "H20 [mmol/mol]"),
    ({"regex": r"^Flow_[0-9]+m"}, "Flow [sl/m]"),
    ({"regex": r"^Tair[0-9]+(PT|HMP|WXT)"}, "Tair [degC]"),
    ({"regex": r"^Trad[0-9]+(n|s)"}, "Trad [degC]"),
    ({"regex": r"^Tsoil[0-9]+(_.*)?"}, "Tsoil [degC]"),
]


def plotCombined(fig, df):
    def _plot(fig, df, label):
        if len(df.columns) > 1:
            ax = appendSubplot(fig)
            configAxes(ax, label, df.index.min(), df.index.max())
            for name, col in df.items():
                ax.plot(col, label=name)
            ax.legend(loc="upper left", fontsize="small", frameon=False)

    def plotVargroups(fig, data):
        for i, (arg, label) in enumerate(VARGROUPS):
            _plot(fig, data.filter(**arg), label)

    def plotStems(fig, data):
        groups = data.columns.str.extract(r"(stemT_[0-9]{2})", expand=False)
        for label, dff in data.groupby(by=groups, axis=1):
            _plot(fig, dff, "{:} [degC]".format(label))

    data, flags = splitTable(df, 0)
    # remove flagged data from the group plots
    data[getMaxflags(flags.values) > 0] = np.nan

    plotVargroups(fig, data)
    plotStems(fig, data)


def plotSensors(fig, df, ylims=(None, None)):
    interval = df.index.freq
    data, flags = splitTable(df)

    for name, label in data.columns:
        ax = appendSubplot(fig)
        configAxes(ax, label, df.index.min(), df.index.max())

        flag_plot_data = iterFlagPlots(data[name], flags[name])
        legend = []
        for flgs, color, line, marker, label, handle in flag_plot_data:
            plotLine(ax, flgs, interval, color, line, marker)
            legend.append([handle, label])

        ax.plot(removeFlaggedData(data[name], flags[name]), color="blue")
        ax.set_ylim(ylims)

    fig.axes[0].legend(
        *zip(*legend), loc="upper center", bbox_to_anchor=(0.5, 1.5), ncol=3
    )


def procDevice(device, logger):
    logger.info("plotting: %s - %s", device.station_key, device.logger_key)
    df = device.getL2Data(fillna=False, fill=True, drop_record=True)

    if df.empty:
        logger.warning("No data available for: %s", device)
        return
    df = dropEmptyVariables(df)

    fig = plt.figure(figsize=(12, 1.8 * len(df.columns)))
    plotSensors(fig, df)
    plotCombined(fig, df)
    fname = Path(device.path, "current_level1.png")
    logger.debug("writing to: %s", fname)
    plt.savefig(str(fname), bbox_inches="tight")
    with open(str(fname.with_suffix(".html")), "w") as f:
        f.write(htmlWrapper(fname.name))
    plt.close()


def main(station=None, device=None, start_date=None, end_date=None, debug=False):
    devices = getDevices(
        station=station,
        logger=device,
        tag="meteo",
        start_date=start_date,
        end_date=end_date,
    )

    with initLogger(__file__, debug) as logger:
        for device in devices:
            with exceptionLogged(logger, fail=debug):
                procDevice(device, logger)


if __name__ == "__main__":
    from lib.argparser import parseArguments

    args = parseArguments("Plot flagged data", {"ndays": 1})
    main(
        station=args.station,
        device=args.device,
        start_date=args.start_date,
        end_date=args.end_date,
        debug=args.debug,
    )
