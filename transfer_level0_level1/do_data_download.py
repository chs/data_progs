#!/usr/bin/env python
"""
HISTORY
-------
Written Matthias Cuntz Jun 2014

CHANGELOG
---------
David Schafer, Jul 2017, removed this nasty if-statement

"""

from pathlib import Path
from lib.tools import createPath
from lib.faccess import getStations
from lib.logger import initLogger, exceptionLogged
from lib.ftputils import sftpConnection
from config.uri import FTPROOT


def downloadFiles(device, logger):
    with sftpConnection(FTPROOT, device.sftp_user) as sftp:
        for remote_fname, fsize in device.getRemoteFiles():
            local_fname = Path(device.translateRemoteToL0(remote_fname))

            createPath(local_fname)

            if local_fname.is_file() and (fsize <= local_fname.stat().st_size):
                logger.debug(f"{local_fname} already exists: skipping")
                continue

            logger.info("downloading: %s -> %s", remote_fname, local_fname)
            sftp.get(remote_fname, local_fname)


def main(station=None, device=None, tag=None, debug=False, fail=False):
    with initLogger(__file__, debug) as logger:
        for station in getStations(station, device):
            for device in station.getDevices(tag=tag):
                logger.info("processing: %s", device)
                msg = "download failed for: {:}".format(device)
                with exceptionLogged(logger, msg, fail=fail):
                    downloadFiles(device, logger=logger)


if __name__ == "__main__":
    from lib.argparser import parseArguments

    args = parseArguments("Downlowd data from the ftp servers", {"ndays": 1})

    main(
        station=args.station,
        device=args.device,
        tag=args.tag,
        debug=args.debug,
        fail=args.fail,
    )
