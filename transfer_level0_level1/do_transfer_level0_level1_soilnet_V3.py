#! /usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime

from config.data import MINDATE
from lib.faccess import getDevices
from lib.logger import initLogger, exceptionLogged
from lib.argparser import parseArguments
from lib.daccess import IndexFields as I


def procDevice(device, logger):
    raw = device.getL0Data()
    if raw.empty:
        return

    out = raw.drop_duplicates()
    out = out.drop(columns=["ID", "counter"])
    out.columns = out.columns.set_names([I.HEADER])
    device.setL1Data(out)


def main(
    station=None, device=None, start_date=None, end_date=None, debug=None, fail=None
):
    with initLogger(__file__, debug) as logger:
        device = getDevices(
            station=station,
            logger=device,
            tag="soilnet_V3",
            start_date=start_date or MINDATE,
            end_date=end_date or datetime.today(),
        )

        for device in device:
            logger.info("transferring: %s", device)

            msg = "soilnet transfer level0 to level1 failed for: {:}".format(device)
            with exceptionLogged(logger, msg, fail=fail):
                procDevice(device, logger)


if __name__ == "__main__":
    args = parseArguments("Transfer soilnet V3 level0 data to level1", {"ndays": 1})
    main(
        station=args.station,
        device=args.device,
        start_date=args.start_date,
        end_date=args.end_date,
        debug=args.debug,
        fail=args.fail,
    )
