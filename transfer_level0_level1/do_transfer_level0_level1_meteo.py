#! /usr/bin/env python
# -*- coding: utf-8 -*-
import re
from functools import partial

import pandas as pd
import numpy as np
import saqc

from saqc import fromConfig, DictOfSeries

from lib.daccess import prepareTable, squeezeColumns

SIGMA = 5.67e-08  # Stefan-Boltzmann constant [W m^-2 K^-4]
T0 = 273.15  # Celcius <-> Kelvin [K]


class CalFlds(object):
    VARIABLE = "Variable"
    OFFSET = "Offset"
    LINEAR = "Linear"
    SQUARE = "Square"
    HEADEROUT = "Headerout"
    UNITS = "Units"


def rotateWinddirection(wd: pd.Series, a: float) -> pd.Series:
    """
    Rotation of wind direction

    Parameters
    ----------
    wd : wind direction in degree
    a  : rotation angle in degree (positive is clockwise)

    Returns
    -------
    rotated wind direction
    """
    rot = wd + a
    rot = np.where(rot < 0, rot + 360, rot)
    rot = np.where(rot >= 360, rot - 360, rot)
    return pd.Series(rot, index=wd.index)


def rotateWinddirectionSlice(wd, a, start_date=None, end_date=None):
    rot = rotateWinddirection(wd.loc[start_date:end_date], a)
    out = wd.copy(deep=True)
    out.loc[start_date:end_date] = rot
    return out


def windSpeed(u, v):
    """
    Calculation of wind velocity from u- and v-component of wind vector

    Parameters
    ----------
    u : u-component of the wind vector
    v : v-component of the wind vector

    Returns
    -------
    horizontal wind velocity
    """
    return np.sqrt(u * u + v * v)


def windDirection(u: pd.Series, v: pd.Series) -> pd.Series:
    """
    Calculation of wind direction from u- and v-component of wind vector


    Parameters
    ----------
    u : u-component of the wind vector
    v : v-component of the wind vector

    Returns
    -------
    horizontal wind direction
    """
    wd = u.copy(deep=True)
    wd.loc[(u == 0) & (v == 0)] = 0
    wd.loc[(u == 0) & (v < 0)] = 360
    wd.loc[(u == 0) & (v > 0)] = 180

    wd.loc[u > 0] = 270.0 - np.rad2deg(np.arctan(v / u))
    wd.loc[u < 0] = 90.0 - np.rad2deg(np.arctan(v / u))

    return wd


def radiationTemperature(data, eps=1):
    """
    Calculation of radiation temperature from long wave radiation

    Parameters
    ----------
    data : long wave radiation in W m-2
    eps  : long wave emissivity of the surface (between 0 and 1)

    Returns
    -------
    radiation temperature in degrees C
    """
    const = 1.0 / (eps * SIGMA)
    out = np.sqrt(np.sqrt(const * data)) - 273.15
    # TODO: remove the masking, when SaQC integration is completed
    return out.mask((out < -50) | (out > 70))


def albedo(swd, swu, swdmin, swumin):
    """
    Calculation of albedo from short wave downward and upward radiation with limits

    Parameters
    ----------
    swd    : short wave downward radiation in Wm-2,
    swu    : short wave upward radiation in Wm-2,
    swdmin : short wave downward radiation limit in Wm-2,
    swumin : short wave upward radiation limit in Wm-2,

    Returns
    -------
    albedo in %
    """
    out = swu * 100 / swd
    return out.mask((out < 0) | (out > 100) | (swd < swdmin) | (swu < swumin))


def longwaveRadiation(data, tpyr):
    """
    Calculation of long wave radiation from net radiometer

    Parameters
    ----------
    data : output voltage of the net radiometer in mV
    tpyr : temperature of the net radiometer body in degree C

    Returns
    -------
    total radiation in W m-2
    """
    return data + SIGMA * (tpyr + T0) ** 4


def waterVapourConcentration(temp, rh, p):
    """
    Calculation of water vapour concentration

    Parameters
    ----------
    temp : air temperature in degrees C
    rh   : relative humidity in %
    p    : air pressure in mbar (hPa)

    Returns
    -------
    water vapour concentration in mmol mol-1
    """
    es = esat(temp + 273.15) * 0.01
    ea = es * rh * 0.01
    c = (1000.0 * ea) / p
    return c


def potentialTemperature(temp, p):
    """
    Calculation of potential temperature

    Parameters
    ----------
    temp : air temperature in degrees C
    p    : the air pressure in mbar (hPa)

    Returns
    -------
    potential temperature in K
    """
    return (temp + 273.15) * (1000.0 / p) ** 0.286


def airDensity(temp, rh, p):
    """
    Calculation of air density

    Parameters
    ----------
    temp : air temperature in oC,
    rh   : relative humidity in %,
    p    : air pressure in mbar (hPa).

    Returns
    -------
    air density in kg m-3
    """
    es = esat(temp + T0) * 0.01
    ea = es * rh * 0.01
    sh = (622.0 * ea) / (p - 0.378 * ea)
    Tv = ((temp + T0) * (1 + 0.000608 * sh)) - T0
    rho = (p * 100) / (287.05 * (Tv + T0))
    return rho


def specificHumidity(temp, rh, p):
    """
    Calculation of specific humidity

    Parameters
    ----------
    temp : air temperature in oC
    rh   : relative humidity in %
    p    : air pressure in mbar (hPa)

    Returns
    -------
    specific humidity in g kg-1
    """
    es = esat(temp + T0) * 0.01
    ea = es * rh * 0.01
    sh = (622.0 * ea) / (p - 0.378 * ea)
    return sh


def dewpointTemperature(temp, rh):
    """
    Calculation of dew point temperature

    Parameters
    ----------
    temp : air temperature in degrees C
    rh   : relative humidity in %

    Returns
    -------
    dew point temperature in degrees C
    """
    es = esat(temp + 273.15) * 0.01
    ea = es * rh * 0.01
    dpt = 234.175 * np.log(ea / 6.1078) / (17.08085 - np.log(ea / 6.1078))
    return dpt


def waterVapourPressureDeficit(temp, rh):
    """
    Calculation of water vapour pressure deficit


    Parameters
    ----------
    temp : air temperature in degrees C
    rh   : relative humidity in %

    Returns
    -------
    water vapour pressure deficit in mbar (hPa)
    """
    es = esat(temp + 273.15) * 0.01
    ea = es * rh * 0.01
    vpd = es - ea
    return vpd


def waterVapourPressure(temp, rh):
    """
    Calculation of actual water vapour pressure

    Parameters
    ----------
    temp : air temperature in degrees C
    rh   : relative humidity in %

    Returns
    -------
    actual water vapour pressure in mbar (hPa)
    """
    es = esat(temp + T0) * 0.01
    ea = es * rh * 0.01
    return ea


def saturationWaterVapourPressure(temp):
    """
    Calculation of saturation water vapour pressure

    Parameters
    ----------
    temp : air temperature in degrees C

    Returns
    -------
    saturation water vapour pressure in mbar (hPa)
    """
    es = esat(temp + T0) * 0.01
    return es


def soilmoistureMean(*variables: pd.Series) -> pd.Series:
    df = pd.DataFrame({v.name: v for v in variables})
    out = df.mean(axis=1).mask(df.isna().any(axis=1))
    return out


def esat(temp, liquid=False):
    """
    Calculates the saturation vapour pressure of water/ice.

    Parameters
    ----------
    temp   : air temperature in K
    liquid : whether to calculate  vapour pressure over liquid water over the entire temperature range


    Returns
    -------
    saturation water pressure at temperature T in Pascal [Pa]

    Note
    ----
    Calculation based on Smithsonian Tables, 1984; after Goff and Gratch, 1946
    For temperatures above (and equal) 0 deg C (273.15 K), the vapour pressure over liquid water is calculated.
    For temperatures below 0 deg C, the vapour pressure over ice is calculated.
    """

    # Split input into masked arrays
    if liquid == True:
        tlim = 1e-3
    else:
        tlim = T0

    ii = temp >= tlim
    jj = temp < tlim
    t_liq = temp[ii]
    t_ice = temp[jj]

    esat = temp.copy()  # to conserve mask

    # Liquid
    if t_liq.size > 0:
        ts = 373.16  # steam point temperature in K
        ews = 1013.246  # saturation pressure at steam point temperature, normal atmosphere
        esat_liq = 10.0 ** (
            -7.90298 * (ts / t_liq - 1.0)
            + 5.02808 * np.log10(ts / t_liq)
            - 1.3816e-7 * (10.0 ** (11.344 * (1.0 - t_liq / ts)) - 1.0)
            + 8.1328e-3 * (10.0 ** (-3.49149 * (ts / t_liq - 1)) - 1.0)
            + np.log10(ews)
        )
        esat[ii] = esat_liq * 100.0

    # Ice
    if t_ice.size > 0:
        ei0 = 6.1071  # mbar
        ts = 273.16  # freezing point in K
        esat_ice = np.exp(
            np.log(10.0)
            * (
                -9.09718 * (ts / t_ice - 1.0)
                - 3.56654 * np.log10(ts / t_ice)
                + 0.876793 * (1.0 - t_ice / ts)
                + np.log10(ei0)
            )
        )
        esat[jj] = esat_ice * 100.0

    return esat


def cleanData(df, device):
    "replace manual flag -20 with 0 values"

    # manflags don't carry units, so we need to strip them first
    columns = df.columns
    df.columns = df.columns.str.split(" ").str[0]
    manflags = device.getManualFlags()
    cleanups = manflags.loc[
        (manflags["start"] >= df.index.min()) & (manflags["flag"] == -20)
    ]
    for key, (start, end, *_) in cleanups.iterrows():
        # we need to work index based here
        col = df.columns.get_loc(key)
        # find the next valid indexes
        sidx = df.index.get_indexer([start], method="bfill")[0]
        eidx = df.index.get_indexer([end], method="bfill")[0] + 1
        df.iloc[sidx:eidx, col] = 0
    # bring back the units
    df.columns = columns
    return df


def transferData(df, device):
    def rename(caldata, name):
        """
        column name lookup
        """
        calrow = caldata.loc[name.split(" ")[0]]
        return calrow[CalFlds.HEADEROUT] + " [" + calrow[CalFlds.UNITS] + "]"

    def calibrate(caldata, column):
        """apply calibration function"""
        calrow = caldata.loc[column.name]
        return (
            calrow[CalFlds.OFFSET]
            + calrow[CalFlds.LINEAR] * column
            + calrow[CalFlds.SQUARE] * column * column
        )

    calfiles = device.getConfigFiles()  # filter=True)

    df = df.copy()
    df.columns = df.columns.str.split(" ").str[0]
    df.columns = [re.sub("(_avg|_max)$", "", col, flags=re.I) for col in df.columns]

    dfs = []
    for i, (f, start, end) in calfiles.iterrows():
        data = df.loc[(df.index >= start) & (df.index < end)]
        if data.empty:
            continue

        caldata = (
            pd.read_csv(f, encoding="latin")
            .dropna(axis=1, how="all")
            .fillna("")
            .set_index(CalFlds.VARIABLE)
        )

        # we currently don't enforce minimal calfile, i.e. there
        # may be variables listed not available in `data`
        data = data.loc[:, caldata.index.intersection(data.columns)]

        # apply calibration functions
        data = data.apply(partial(calibrate, caldata))

        # rename columns to values given in the calfile
        data = data.rename(partial(rename, caldata), axis="columns")

        dfs.append(squeezeColumns(data))

    out = pd.concat(dfs).sort_index()
    out = (
        out.pipe(squeezeColumns)
        .pipe(prepareTable, fillna=False)
        .dropna(how="all", axis="columns")
    )
    return out


def columnMapper(name):
    # TODO:
    # - fix SaQC.generic.{process, flag}
    # - move the units to the config files
    if re.search(".*\[.*\]$", name):
        # don't overwrite present units
        return name
    if re.search("^(SHF|LWUR|LWDR|TDR|TUR|Rn)", name):
        return f"{name} [W/m**2]"
    elif re.search("^(Albedo|PAlbedo)", name):
        return f"{name} [%]"
    elif re.search("^(Trad|Tdew)", name):
        return f"{name} [degC]"
    elif re.search("^(VPact|VPdef|VPmax)", name):
        return f"{name} [hPa]"
    elif re.search("^(sh)", name):
        return f"{name} [g/kg]"
    elif re.search("^(rho)", name):
        return f"{name} [kg/m3]"
    elif re.search("^(Tpot)", name):
        return f"{name} [K]"
    elif re.search("^(H2Oc)", name):
        return f"{name} [mmol/mol]"
    elif re.search("^(WS_)", name):
        return f"{name} [m/s]"
    elif re.search("^(WD_)", name):
        return f"{name} [deg]"
    else:
        return name


saqc.parsing.environ.ENVIRONMENT.update(
    {
        "soilmoistureMean": soilmoistureMean,
        "rotateWinddirection": rotateWinddirection,
        "rotateWinddirectionSlice": rotateWinddirectionSlice,
        "windSpeed": windSpeed,
        "windDirection": windDirection,
        "radiationTemperature": radiationTemperature,
        "albedo": albedo,
        "longwaveRadiation": longwaveRadiation,
        "waterVapourConcentration": waterVapourConcentration,
        "potentialTemperature": potentialTemperature,
        "airDensity": airDensity,
        "specificHumidity": specificHumidity,
        "dewpointTemperature": dewpointTemperature,
        "waterVapourPressureDeficit": waterVapourPressureDeficit,
        "waterVapourPressure": waterVapourPressure,
        "saturationWaterVapourPressure": saturationWaterVapourPressure,
    }
)


def procDevice(logger, device, **kwargs):
    logger.info(f"processing: {device}")

    logger.info("reading ...")
    l0 = device.getL0Data(fillna=False, fail=False)

    l1 = l0.pipe(transferData, device).pipe(cleanData, device).astype(np.float32)

    # NOTE: strip the units from column names for a simpler SaQC setup
    colnames = {c: c.split(" ")[0] for c in l1.columns}
    l1 = l1.rename(columns=colnames)

    logger.info("run SaQC ...")
    saqc = fromConfig(
        device.getTransferConfig(),
        data=DictOfSeries({k: v for k, v in l1.items()}),
        scheme="positional",
    )

    # NOTE: remove columns we should have overwritten, but didn't because of stripped columns names
    data = saqc.data.to_df()
    data = data.drop(
        columns=data.columns[data.columns.str.match(".*\[.*\]")].str.split(" ").str[0],
        errors="ignore",
    )

    # NOTE: bring back the stripped units
    data = data.rename(columns={v: k for k, v in colnames.items()})

    # NOTE: add units to the newly creates columns, this is fixable as soon as
    #       generic.process accpets aribtrary strings as function arguments
    data = data.rename(columns=columnMapper)

    # NOTE: some functions might generate np.inf -> remove that
    data = data.replace([np.inf, -np.inf], np.nan)

    # NOTE: we need to restore the origin
    data.columns.name = l0.columns.name

    logger.info("writing output ...")
    device.setL1Data(data)


if __name__ == "__main__":
    from lib.argparser import parseArguments
    from lib.driver import mapDevices

    args = parseArguments("transfer meteo data from level 0 to level 1")
    mapDevices(__file__, args, procDevice, filter_kwargs={"tag": ("meteo")})
