#! /usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import date, datetime

import numpy as np
import pandas as pd

from lib.faccess import getDevices
from lib.logger import initLogger, exceptionLogged
from lib.daccess import readCaldataICOS
from lib.pdutils import pivotTable

from transfer_level0_level1.do_transfer_level0_level1_soilnet import Flds


def flattenData(df):
    df[Flds.BOX] = ("Box" + df[Flds.BOX].astype("int32").astype("string")).astype(
        "category"
    )
    out = (
        df.set_index([df.index, Flds.BOX])
        .stack(dropna=False)
        .rename_axis([Flds.DATE, Flds.BOX, Flds.HEADERS])
        .rename(Flds.VALUE)
        .reset_index(level=[1, 2])
    )

    return out


def getUnits(device):
    cal = readCaldataICOS(device.calpath)
    dfs = []
    for fname in cal["fname"]:
        df = pd.read_csv(fname, usecols=[Flds.HEADEROUT, Flds.UNITS])
        dfs.append(df.dropna())
    return pd.concat(dfs).drop_duplicates()


def prepHeaders(data, units):
    df = pd.merge(
        data[Flds.HEADERS].drop_duplicates().to_frame(),
        units.set_index(Flds.HEADEROUT),
        left_on=Flds.HEADERS,
        right_index=True,
        how="left",
    )
    # the units are not necessarly unique
    df = df[~df[Flds.HEADERS].duplicated()]
    df = df.set_index(Flds.HEADERS)
    out = df.index + " [" + df[Flds.UNITS].fillna("") + "]"
    return out


def procDevice(device, logger):
    logger.debug("reading data")
    raw = device.getL0Data().astype(np.float32)
    dfs = []

    logger.debug("transferring data")
    for chunk in np.array_split(raw, 4):
        data = flattenData(chunk)
        headers = prepHeaders(data, getUnits(device))

        # remove all variables without complete definition in the config
        data = data[data[Flds.HEADERS].isin(headers.index)]
        data[Flds.HEADERS] = headers[data[Flds.HEADERS]].values

        out = pivotTable(data, columns=Flds.HEADERS, values=Flds.VALUE)
        out.columns.name = Flds.HEADERS
        # NOTE: don't know what to do with duplicated timestamps, so remove them all
        dfs.append(out)

    out = pd.concat(dfs, axis="index")
    out = out[~out.index.duplicated(keep=False)]

    logger.debug("writing data")
    device.setL1Data(out)


def main(
    station=None, device=None, start_date=None, end_date=None, debug=False, fail=False
):
    with initLogger(__file__, debug) as logger:
        devices = getDevices(
            station=station,
            logger=device,
            tag="soilnet_icos",
            start_date=start_date or date(2012, 1, 1),
            end_date=end_date or datetime.today(),
        )

        for device in devices:
            logger.info("transferring: %s", device)

            msg = "soilnet transfer level0 to level1 failed for: {:}".format(device)
            with exceptionLogged(logger, msg, fail=fail):
                # soilnet devices share the same rawdata,
                # do not read it multiple times
                procDevice(device, logger)


if __name__ == "__main__":
    from lib.argparser import parseArguments

    args = parseArguments("Transfer soilnet ICOS level0 data to level1", {"ndays": 1})
    main(
        station=args.station,
        device=args.device,
        start_date=args.start_date,
        end_date=args.end_date,
        debug=args.debug,
        fail=args.fail,
    )
