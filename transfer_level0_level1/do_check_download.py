#!/usr/bin/env python

"""

HISTORY
-------
Written Matthias Cuntz Jun 2014

CHANGELOG
---------
David Schaefer, Jul 2017, removed this terrible if statement...

"""

from lib.faccess import getDevices
from lib.tools import changeDirectory
from lib.logger import initLogger, exceptionLogged
from datetime import timedelta, date


def checkDevice(device):
    files = device.getL0Files()
    dates = [device.getFnameDate(f) for f in files]
    if not len(dates):
        msg = "{:}: no rawdata files found in {:}".format(device, device.l0path)
        raise RuntimeError(msg)

    today = date.today()
    last_date = max(dates).date()
    if last_date < (today - timedelta(1)):
        msg = "{:}: last rawdata file is from {:}".format(device, last_date)
        raise RuntimeError(msg)


def checkDownload(station=None, device=None, debug=False, tag=None):
    devices = getDevices(station=station, logger=device, tag=tag)

    with initLogger(__file__, debug) as log:
        for device in devices:
            if not device.check:
                continue

            # soilnet raw data from all boxes is delivered in a single file
            # no need to check and fail multiple times
            if (
                device.station_key in {"HH", "GB"}
                and device.data_tag == "soilnet"
                and device.logger_key != "Box01"
            ):
                continue

            msg = "download check failed for: {:}".format(device.l0path)
            with exceptionLogged(log, msg, fail=debug):
                log.info("checking: %s", device)

                with changeDirectory(device.l0path):
                    checkDevice(device)


if __name__ == "__main__":
    from lib.argparser import parseArguments

    args = parseArguments("Check if data download was successful", {"ndays": 1})

    checkDownload(
        station=args.station, device=args.device, debug=args.debug, tag=args.tag
    )
