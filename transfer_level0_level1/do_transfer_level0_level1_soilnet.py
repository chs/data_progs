#! /usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd

from lib.logger import initLogger, exceptionLogged
from lib.faccess import getDevices
from lib.argparser import parseArguments
from lib.pdutils import pivotTable


# this thing is mutable at runtime, it would be nice to change that
class Flds(object):
    HEADERS = "headers"
    BOX = "box"
    DATE = "date"
    VALUE = "value"
    OFFSET = "Offset"
    LINEAR = "Linear"
    SQUARE = "Square"
    HEADEROUT = "Headerout"
    UNITS = "Units"


FNAME = "SoilNet_Data_{station}_{box}_{date}.level1.csv"


def flattenData(df):
    df = df.copy()
    df[Flds.BOX] = df[Flds.BOX].apply(lambda x: "Box" + (x.split("_")[-1][1:]))
    out = (
        df.set_index([df.index, Flds.BOX])
        .stack()
        .rename_axis([Flds.DATE, Flds.BOX, Flds.HEADERS])
        .rename(Flds.VALUE)
        .reset_index(level=[1, 2])
    )

    out[Flds.HEADERS] = out[Flds.BOX] + "_" + out[Flds.HEADERS]
    return out


def calibrateTemperature(tempdf):
    return (
        tempdf[Flds.OFFSET]
        + tempdf[Flds.LINEAR] * tempdf[Flds.VALUE]
        + tempdf[Flds.SQUARE] * tempdf[Flds.VALUE] * tempdf[Flds.VALUE]
    )


def calibrateMoisture(moistdf, tempdf):
    # Crimm formula
    epss = 5.0  # eps of soil particules
    epsw_a = 78.35  # a  in exp-equation of eps of water
    epsw_b = -4.55e-3  # b  in exp-equation of eps of water
    T0 = 25.0  # T0 in exp-equation of eps of water
    porosity = 0.45  # of soil
    sepsa = 1.0  # sqrt(epsa)
    sepss = np.sqrt(epss)  # sqrt(epss)

    eps = moistdf[Flds.SQUARE] + (
        1.0
        / (moistdf[Flds.OFFSET] + moistdf[Flds.LINEAR] / (moistdf[Flds.VALUE] * 0.001))
    )  # Calibration of eps
    epsw = epsw_a * np.exp(epsw_b * (tempdf[Flds.VALUE] - T0))
    out = (
        (np.sqrt(eps.values) - (1 - porosity) * sepss - porosity * sepsa)
        / (np.sqrt(epsw.values) - sepsa)
        * 100
    )

    return out


def transferData(device, df):
    calfiles = device.getConfigFiles(filter=True)

    dfs = []
    for i, (f, start, end) in calfiles.iterrows():
        caldata = pd.read_csv(f).dropna(axis=1, how="all")
        data = (
            df.loc[start:end]
            .reset_index()
            .merge(caldata, left_on=Flds.HEADERS, right_on=Flds.HEADEROUT, how="left")
            .set_index(Flds.DATE)
        )
        data[Flds.HEADERS] = data[Flds.HEADERS] + " [" + data[Flds.UNITS] + "]"
        temp_idx = data[Flds.HEADEROUT].str.contains("Temp").fillna(False)
        moist_idx = data[Flds.HEADEROUT].str.contains("Moist").fillna(False)

        data.loc[temp_idx, Flds.VALUE] = calibrateTemperature(data[temp_idx])
        data.loc[moist_idx, Flds.VALUE] = calibrateMoisture(
            data[moist_idx], data[temp_idx]
        )

        dfs.append(data)

    return pd.concat(dfs)


def dropDuplicates(df):
    # drop duplicates from rawdata
    return (
        df.reset_index()
        .drop_duplicates(subset=[Flds.DATE, Flds.BOX])
        .set_index(Flds.DATE)
    )


def trimData(device, df):
    start_date = pd.to_datetime(device.start_date)
    end_date = pd.to_datetime(device.end_date)
    return df.loc[(df.index >= pd.to_datetime(start_date)) & (df.index < end_date)]


def prepOutput(df):
    data = pivotTable(df, columns=Flds.HEADERS, values=Flds.VALUE)
    data.columns.name = Flds.HEADERS
    # NOTE: don't know what to do with duplicated timestamps, so remove them all
    data = data[~data.index.duplicated(keep=False)]
    return data.round(4)


def main(
    station=None, device=None, start_date=None, end_date=None, debug=False, fail=False
):
    with initLogger(__file__, debug) as logger:
        devices = getDevices(
            station=station,
            logger=device,
            tag="soilnet",
            start_date=start_date,
            end_date=end_date,
        )

        for device in devices:
            logger.info("transferring: %s", device)

            msg = f"soilnet transfer level0 to level1 failed for: {device}"
            with exceptionLogged(logger, msg, fail=fail):
                logger.debug("reading data: %s", device)

                # soilnet devices share the same rawdata,
                # do not read it multiple times
                raw = device.getL0Data()
                if raw.empty:
                    continue

                device_data = raw.pipe(dropDuplicates).pipe(flattenData)
                logger.debug(f"values found: {len(device_data)}")
                if device_data.empty:
                    continue

                transfer = transferData(device, device_data)
                trim = trimData(device, transfer)
                out = prepOutput(trim)
                device.setL1Data(out)


if __name__ == "__main__":
    args = parseArguments("Transfer soilnet level0 data to level1", {"ndays": 1})
    main(
        station=args.station,
        device=args.device,
        start_date=args.start_date,
        end_date=args.end_date,
        debug=args.debug,
        fail=args.fail,
    )
