#! /usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import annotations

"""
NOTE
----
As the name suggests, this script should be placed in the subfolder level1
"""

import re
import csv
import logging
import uuid

from dataclasses import dataclass
from io import StringIO
from typing import Tuple, Sequence, List, Dict

import numpy as np
import pandas as pd

from saqc import PositionalScheme, SaQC, fromConfig
from saqc.core import flagging, register
from saqc.constants import BAD
from saqc.lib.tools import toSequence
from saqc.core.history import History

from config.data import NODATA
from lib.daccess import splitTable
from lib.daccess import ConfigFields as CF
from lib.daccess import IndexFields as IF
from lib.logger import initLogger, exceptionLogged
from lib.faccess import getDevices
from lib.pdutils import inferFrequency


def prepManFlags(data, manual_flags, variable):
    translator = PositionalScheme()
    mflags = (
        manual_flags[manual_flags.index == variable]
        # sort by flag to not acciedentially overwrite higher flags
        .sort_values(manual_flags.columns[2], ascending=True)
    )
    freq = inferFrequency(data)

    out = pd.Series(index=data.index, data=np.nan)
    for _, (start, end, flag, *_) in mflags.iterrows():
        if end < out.index[0] or start > out.index[-1]:
            # skip over invalid manual flag date date ranges (e.g. H3 maintenance)
            continue
        if pd.isna(end):
            end = data.index[-1]

        if flag != -20:
            end = end.ceil(freq)
            out.loc[start:end] = translator(flag)
    return out


def registerManualFuncs(manual_flags):
    # NOTE: close over manual_flags
    @flagging()
    def flagMaintenance(saqc, field, **kwargs):
        mflags = prepManFlags(saqc.data[field], manual_flags, "maintenance")
        saqc._flags[field] = mflags
        return saqc

    # NOTE: close over manual_flags
    @flagging()
    def flagManual(saqc, field, **kwargs):
        mflags = prepManFlags(saqc.data[field], manual_flags, field.split(" ")[0])
        saqc._flags[field] = mflags
        return saqc


@flagging()
def custom_flagConstants(
    saqc, field, thresh, window, lower=-np.inf, deriv=1, flag=BAD, **kwargs
):
    return saqc.andGroup(
        field=field,
        group=[
            saqc.flagConstants(field, thresh, window, **kwargs),
            saqc.flagGeneric(field, func=lambda x: x >= lower),
        ],
    )


def _broadcastFields(
    field: str | Sequence[str], target: str | Sequence[str]
) -> Tuple[Sequence[str], Sequence[str]]:
    fields, targets = toSequence(field), toSequence(target)
    if len(fields) != len(targets):
        if len(fields) != 1 and len(targets) != 1:
            raise ValueError(
                "'field' and 'target' need the same length or one of boths needs "
                f"to be scalar, got 'field={field}' and 'target={target}'"
            )
        if len(targets) == 1:
            targets = targets * len(fields)
        if len(fields) == 1:
            fields = fields * len(targets)
    return fields, targets


@register(
    mask=[], demask=[], squeeze=["target"], handles_target=True, multivariate=True
)
def transferFlags(
    saqc: SaQC,
    field: str | Sequence[str],
    target: str | Sequence[str],
    dfilter: float,
    flag: float | None = None,
    **kwargs,
) -> SaQC:
    target = field if target is None else target
    fields, targets = _broadcastFields(field, target)

    # we need to find the max flags of all fields, that's why we first
    # accumulate the flagcols of all fields and then `max` rowwise
    flags = {}

    for src, trg in zip(fields, targets):
        index = saqc._data[src].index
        if trg not in saqc._data:
            saqc._data[col] = pd.Series(np.nan, index=index)
        if trg not in saqc._flags:
            saqc._flags.history[trg] = History(index=index)

        flagcol = saqc._flags.history[src].hist.max(axis=1)
        if trg not in flags:
            flags[trg] = pd.DataFrame({src: flagcol})
        else:
            flags[trg][src] = flagcol

    for key, df in flags.items():
        saqc._flags[:, key] = df.max(axis=1) if flag is None else flag

    return saqc


@flagging()
def repeatFlags(saqc, field, *args, **kwargs):
    return saqc.propagateFlags(field, *args, **kwargs)


def toFile(config):
    fobj = StringIO()
    config.to_csv(fobj, sep=";", quoting=csv.QUOTE_NONE, index=True)
    fobj.seek(0)
    return fobj


def prepOutput(data, flags, names):
    data = pd.concat(
        [
            data.rename(names, axis="columns"),
            flags.rename(lambda c: c + "_f", axis="columns"),
        ],
        axis="columns",
    )
    # restore the initial column ordering
    data = data.sort_index(axis="columns")
    data.columns.name = IF.HEADER
    return data


def resolveDependencies(df: pd.Series) -> pd.Series:
    """
    returns the given keys ordered by dependecies
    """

    # algorithm adapted from:
    # https://www.electricmonk.nl/log/2008/08/07/dependency-resolving-algorithm/

    @dataclass
    class Node:
        name: str
        edges: List[str]

        def addEdge(self, dep):
            self.edges.append(dep)

        def __repr__(self):
            return f"Node({self.name}, {self.edges})"

    Tree = Dict[str, Node]

    def initTree(df: pd.Series) -> Tree:
        tree = {}
        for name in df.index:
            tree[name] = Node(name=name, edges=[])

        for name, dependencies in df.items():
            node = tree[name]
            for dep in re.split("\s+", dependencies):
                if not dep:
                    continue
                if dep not in tree:
                    raise RuntimeError(f"unknow dependency '{dep}' for node '{name}'")
                node.addEdge(tree[dep])
        return tree

    def resolve(tree: Tree) -> List[str]:
        def _resolve(node, resolved, unresolved):
            unresolved.append(node)
            for edge in node.edges:
                if edge not in resolved:
                    if edge in unresolved:
                        raise RuntimeError(
                            f"circular reference detected: {node.name} <-> {edge.name}"
                        )
                    _resolve(edge, resolved, unresolved)
            resolved.append(node)
            unresolved.remove(node)

        resolved, unresolved = [], []
        _resolve(tree, resolved, unresolved)
        return resolved

    df = df.copy(deep=True)
    # we need a dummy root node, as the dpendency tree
    # likely consists of several unconnected subtrees
    root = str(uuid.uuid4())
    df[root] = " ".join(df.index.tolist())
    tree = initTree(df)
    resolution = resolve(tree[root])
    return pd.Series([r.name for r in resolution if r.name != root])


def getQCConfig(device, logger: logging.Logger) -> pd.Series:
    meta = device.readExcel().set_index(CF.VARNAME)

    # Index(['airpres02WXT', 'Tair02WXT', 'WD02_WXT', 'WS02_WXT', 'RH02WXT',\n       'precip02WXT']
    flagfields = meta.columns[meta.columns.str.match("^Flag_\d+")]
    config = meta[flagfields].dropna(axis="columns", how="all")
    config.index = config.index.to_series().str.strip()

    if not config.empty:
        try:
            deps = meta[CF.DEPENDENCIES]
            var_order = deps.fillna("").pipe(resolveDependencies)
        except KeyError:
            if not device.data_tag.startswith("soilnet"):
                logger.warn(f"{device}: no variable dependency information found")
            var_order = config.index

        config = config.fillna("flagDummy()")

        config = (
            config.loc[var_order]
            .stack()
            .reset_index(level=1, drop=True)
            .replace("flagDummy()", np.nan)
            .dropna()
        )
    config.name = "test"
    config.index.name = "varname"
    return config


def procDevice(device, logger):
    if device.station_key == "HD":
        device.xls_sheets = [
            "Logger Hordorf SAQC",
        ]

    logger.debug("reading data")
    data = device.getL1Data(reindex=True)
    if data.empty:
        logger.debug(f"no data in period: skipping {device}")
        return

    config = getQCConfig(device, logger)
    if config.empty:
        logger.debug(f"no SaQC config: skipping {device}")
        return
    # allow cell based comments
    config = config[~config.str.startswith("#")]

    # remove all columns without DMP registration
    variables = config.index.drop_duplicates().intersection(
        data.columns.get_level_values(0).drop_duplicates()
    )
    config = config[variables]
    data = data[variables]

    # NOTE:
    # just to allow testing with the old data access structure
    # try-block should be removed after the move is completed
    if len(data.columns.levels) == 3:
        try:
            data, _ = splitTable(data)
        except KeyError:
            data = data.droplevel(level=1, axis="columns")
    col_names = dict(data.columns.tolist())

    logger.debug("QC")
    registerManualFuncs(device.getManualFlags())

    saqc = fromConfig(
        toFile(config),
        data=data.droplevel("headers", axis="columns").replace(NODATA, np.nan),
        scheme="positional",
    )
    # TODO:
    # fill all variables outside of their chs-measurements given date ranges
    # with -9999. Example T2: RH33HMP

    # prepare the output (at least as long as we still write to disk)
    out = prepOutput(saqc.data.to_df().fillna(NODATA), saqc.flags, col_names)

    logger.debug("writing data")

    device.setL2Data(out)


def main(
    station=None,
    device=None,
    start_date=None,
    end_date=None,
    tag=None,
    debug=False,
    fail=False,
):
    with initLogger(__file__, debug) as logger:
        devices = getDevices(
            station=station,
            logger=device,
            tag=tag,
            start_date=start_date,
            end_date=end_date,
        )

        for device in devices:
            msg = "flagging failed for: {:}".format(device)
            with exceptionLogged(logger, msg, fail=fail):
                logger.info("flagging: %s", device)
                procDevice(device, logger=logger)


if __name__ == "__main__":
    from lib.argparser import parseArguments

    args = parseArguments("quality control data")

    main(
        station=args.station,
        device=args.device,
        start_date=args.start_date,
        end_date=args.end_date,
        debug=args.debug,
        tag=args.tag,
        fail=args.fail,
    )
